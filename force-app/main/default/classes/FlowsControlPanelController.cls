public class FlowsControlPanelController {

    @AuraEnabled(Cacheable=true)
    public static String getFlows() {
        List<Id> listOfFlowIds = new List<Id>();
        String sessionId = getSessionId();
        Map<String, FlowData> flowsMap = new Map<String, FlowData>();
        String query = 'SELECT Id, MasterLabel, VersionNumber, processType, Status, DefinitionId FROM Flow ORDER BY DefinitionId, Status, VersionNumber';
        String endpoint = URL.getSalesforceBaseUrl().toExternalForm() + 
            '/services/data/v51.0/tooling/query/?q=' +
            EncodingUtil.urlEncode(query, 'UTF-8');
        Http NewReq = new Http();
        HttpRequest hreq = new HttpRequest();
        String baseURL = URL.getSalesforceBaseUrl().toExternalForm();
        hreq.setHeader('Authorization', 'Bearer ' + sessionId);
        hreq.setTimeout(60000);
        hreq.setEndpoint(endPoint);
        hreq.setMethod('GET');
        HttpResponse hresp = NewReq.send(hreq);
        JSONParser parser = JSON.createParser(hresp.getBody());
        FlowData fd;
        while (parser.nextToken() != null) {
            if (parser.getCurrentToken() == JSONToken.FIELD_NAME) {
                if (parser.getText() == 'attributes') {
                    if (fd != null) {
                        if(!flowsMap.containsKey(fd.Id)) flowsMap.put(fd.Id, fd);
                    }
                }
                if (parser.getText() == 'Id') {
                    fd = new FlowData();
                    parser.nextToken();
                    fd.Id = parser.getIdValue();
                    listOfFlowIds.add(parser.getIdValue());
                }
                if (parser.getText() == 'MasterLabel') {
                    parser.nextToken();
                    fd.MasterLabel = parser.getText();
                }
                if (parser.getText() == 'DefinitionId') {
                    parser.nextToken();
                    fd.DefinitionId = parser.getText();
                }
                if (parser.getText() == 'Status') {
                    parser.nextToken();
                    fd.Status = parser.getText();
                }
                if (parser.getText() == 'ProcessType') {
                    parser.nextToken();
                    fd.ProcessType = parser.getText();
                }
                if (parser.getText() == 'VersionNumber') {
                    parser.nextToken();
                    fd.VersionNumber = Integer.valueOf(parser.getText());
                }
            }
        }
        if (fd != null) {
            if(!flowsMap.containsKey(fd.Id)) flowsMap.put(fd.Id, fd);
        }
        for (Id flowId : listOfFlowIds) {
            if(flowsMap.get(flowId).ProcessType == 'Workflow') {
                endpoint = URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v51.0/tooling/sobjects/Flow/' + flowId;
                Http http = new Http();
                HttpRequest httpReq = new HttpRequest();
                HttpResponse httpRes = new HttpResponse();
                httpReq.setMethod('GET');
                httpReq.setHeader('Authorization', 'OAuth ' + sessionId);
                httpReq.setEndpoint(endpoint);
                httpRes = http.send(httpReq);
                Map<String, Object> results = (Map<String, Object>) JSON.deserializeUntyped(httpRes.getBody());
                Map<String, Object> metadata = (Map<String, Object>) results.get('Metadata');
                List<Object> processMetadataValues = (List<Object>) metadata.get('processMetadataValues');
                String objectName = processMetadataValues.size() > 0 ? (String)((Map<String, Object>)((Map<String, Object>) processMetadataValues.get(0)).get('value')).get('stringValue') : null;
                FlowData flow = flowsMap.get(flowId);
                if (flow != null) flow.objectName = objectName;
            }
        }
        system.debug(JSON.serialize(flowsMap));
        return JSON.serialize(flowsMap.values());
    }
    
    @AuraEnabled(Cacheable=true)
    public static String disableFlow(String definitionId, Integer activeVersion) {
        Http h = new Http();
        HttpRequest req = new HttpRequest();
        req.setEndpoint(URL.getSalesforceBaseUrl().toExternalForm() + '/services/data/v41.0/tooling/sobjects/FlowDefinition/' + definitionId + '/?_HttpMethod=PATCH');
        req.setBody('{ "Metadata": { "activeVersionNumber": ' + activeVersion + ' } }');
        req.setHeader('Authorization', 'OAuth ' + getSessionId());
        req.setHeader('Content-Type', 'application/json');
        req.setMethod('POST');
        HttpResponse res = h.send(req);
        return res.getBody();
    }
    
    private static String getSessionId() {
        return Page.SessionId.getContent().toString();
    }
    
    public class FlowData {
        public String Id;
        public String MasterLabel;
        public String DefinitionId;
        public String Status;
        public Integer VersionNumber;
        public String ProcessType;
        public String objectName;
    }

}