/**
 * @author ULIT
 // Database.executeBatch(new UpdateContractSignatoriesBatch('Dev Console', null), 10);
 * */
public class UpdateContractSignatoriesBatch implements Database.Batchable<SObject> {

    private ULit_Negoptim.NegoptimBatch nb;
    public final String query;
    
    public UpdateContractSignatoriesBatch(String startedFrom, Set<Id> contractIds) {
        this.nb = new ULit_Negoptim.NegoptimBatch(UpdateContractSignatoriesBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateless, startedFrom);

        String q = 'SELECT Id, ULiT_Negoptim__CompanySigned_By__c, SPITM_ITM_Signatory_role_Id__c FROM ULiT_Negoptim__Contract__c';
        q += ' WHERE RecordType.DeveloperName in (\'Period_Extension\', \'Signed\', \'To_Be_Signed\')';
        q += ' AND ULiT_Negoptim__Status__c != \'Document Printed\'';
        q += ' AND ULiT_Negoptim__Status__c != \'Ready to publish\'';
        q += ' AND ULiT_Negoptim__Status__c != \'Published\'';
        q += ' AND ULiT_Negoptim__Status__c != \'Signé Frs\'';
        q += ' AND ULiT_Negoptim__Status__c != \'Balanced\'';
        q += ' AND ULiT_Negoptim__Status__c != \'Closed\'';
        q += ' AND ULiT_Negoptim__Status__c != \'Deleted\'';
        q += ' AND ULiT_Negoptim__Contract_EDate__c >= ' + String.ValueOf(system.today());
        if (contractIds != null && !contractIds.isEmpty()) {
            q += ' AND Id IN (\'' + String.join(new List<Id>(contractIds), '\',\'') +'\')';
        }
        this.query = q;
        nb.logParameter('contractIds', contractIds);
        nb.logParameter('query', this.query);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    // Execute method implementation.
    public void execute(Database.BatchableContext BC, List<ULiT_Negoptim__Contract__c> scope) {
        list<ULiT_Negoptim__Contract__c> contractToUpdate = new list<ULiT_Negoptim__Contract__c>();
        for (ULiT_Negoptim__Contract__c item : scope) {
            list<Banner_Signatory__c> list_Banner_Signatories = [SELECT Id, Signatory__c, Signatory_role_Id__c FROM Banner_Signatory__c WHERE is_Active__c = true];
            for (Banner_Signatory__c BannerSignatory : list_Banner_Signatories){

                if (BannerSignatory.Signatory_role_Id__c == item.SPITM_ITM_Signatory_role_Id__c){
                    item.ULiT_Negoptim__CompanySigned_By__c = BannerSignatory.Signatory__c;
                    item.SPITM_Last_CompanySignedBy_Update__c = system.today();
                    contractToUpdate.add(item);
                }
            }
        }
        if (!contractToUpdate.isEmpty()) {
            List<Database.SaveResult> results = Database.update(contractToUpdate, false);
            for (Integer i = 0; i < results.size(); i++) {
                Database.SaveResult result = results.get(i);
                if (!result.isSuccess()) {
                    String errorMessage = 'Contract: ' + '\n';
                    Database.Error[] errors = result.getErrors();
                    for (Database.Error err : errors) {
                        errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                    }
                    nb.logError(errorMessage);
                }
            }
        }
        nb.saveLog(bc);
    }
    
    public void finish(Database.BatchableContext BC) {
        nb.sendEmail(bc, null, 'UpdateContractSignatoriesBatch');
    }
}