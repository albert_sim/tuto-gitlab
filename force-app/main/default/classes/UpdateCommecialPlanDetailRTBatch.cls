/**
 * @author ULIT
 * @description Manual batch to be executed manually one time to set up the Commercial_Plan_Detail__c RT to Higher_Level_Packaging
 * */
public class UpdateCommecialPlanDetailRTBatch  implements Database.Batchable<SObject>{
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        String query ='SELECT Id, Name, RecordType.DeveloperName, ULiT_Negoptim__Statut__c, ULiT_Negoptim__Product__r.RecordType.DeveloperName';
        query += ' FROM ULiT_Negoptim__Commercial_Plan_Detail__c';
        query += ' WHERE RecordType.DeveloperName = \'To_Be_Validated\'';
        query += ' OR RecordType.DeveloperName = \'Higher_Level_Packaging\'';
        ////query += ' LIMIT 1000';
        return Database.getQueryLocator(query);
    }
    public void execute(Database.BatchableContext bc, List<ULiT_Negoptim__Commercial_Plan_Detail__c> scope) {
        List<ULiT_Negoptim__Commercial_Plan_Detail__c> listToUpdate = new List<ULiT_Negoptim__Commercial_Plan_Detail__c>();
        for (ULiT_Negoptim__Commercial_Plan_Detail__c item : scope) {
            Id currentRTId = item.RecordTypeId;
            if (item.ULiT_Negoptim__Product__r.RecordType.DeveloperName == 'Higher_Level_Packaging') {
                item.RecordTypeId = '0129E000000ExPuQAK'; // Higher_Level_Packaging
            } else if (item.ULiT_Negoptim__Product__r.RecordType.DeveloperName == 'Product') {
                item.RecordTypeId = item.ULiT_Negoptim__Statut__c == 'Validated' ? '0129E000000hHECQA2' : '0129E000000hHEBQA2';
            }
            if (item.RecordTypeId != currentRTId) {
                listToUpdate.add(item);
            }
        }
        List<Database.SaveResult> results = Database.update(listToUpdate, false);
    }
    public void finish(Database.BatchableContext BC) {}
}