/**
 * @author ULiT
 * @date 07 October 2021
 * @description Test for ExecuteBatchInvocable
*/
@isTest
private class ExecuteBatchInvocableTest {

    static testMethod void case1_execute() {
        List<ExecuteBatchInvocable.BatchParametersWrapper> batchParamsList = new List<ExecuteBatchInvocable.BatchParametersWrapper>();
        ExecuteBatchInvocable.BatchParametersWrapper batchParams = new ExecuteBatchInvocable.BatchParametersWrapper();
        batchParams.batchName = 'ULiT_Negoptim.ConsolidateContractBatch';
        batchParams.startedFrom = 'Test';
        batchParams.supplierBU = null;
        batchParams.supplierId = null;
        batchParams.negoScopeId = null;
        batchParams.contractBU = null;
        batchParams.year = date.today().year();
        batchParamsList.add(batchParams);
        Test.startTest();
        ExecuteBatchInvocable.execute(batchParamsList);
        Test.stopTest();
        System.assertEquals(1, [SELECT COUNT() FROM AsyncApexJob]);
    }
}