/**
 * @author ULiT
 * @description unit test class for Trg_ULiT_Contract_Discount trigger
 * @date 21 september 2021
*/
@isTest
private class Trg_ULiT_Contract_Discount_Test {
    static ULiT_Negoptim__Pol_Com_Condition__c nip, rp0, en0;
    static ULiT_Negoptim__Contract_Discount__c cd1, cd2, cd3;
    static ULiT_Negoptim__Contract__c contract;
    
    static void init() {
        
        Integer year = Date.today().year();
        
        // Get field describe.
        Schema.DescribeFieldResult fieldResult = ULiT_Negoptim__Pol_Com_Condition__c.ULiT_Negoptim__Nego_Discount_Type__c.getDescribe();
        // Get the picklist value.
        String plv = fieldResult.getPicklistValues().get(0).getValue();
        // Create NIP Tarrif Condition.
        nip = new ULiT_Negoptim__Pol_Com_Condition__c(Name = 'NIP',
                                                      ULiT_Negoptim__Index__c = 'Index3',
                                                      ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1),
                                                      ULiT_Negoptim__Condition_Code__c = 'NIP',
                                                      ULiT_Negoptim__Abrev__c = 'NIP',
                                                      SPITM_Groupe_nego__c = '10_REMISE_FACTURE',
                                                      ULit_Negoptim__Nego_Discount_type__c = 'Price Discount',
                                                      ULiT_Negoptim__Gen_Name_com__c = 'NIP',
                                                      ULiT_Negoptim__Name_Com__c = 'NIP',
                                                      ULiT_Negoptim__Name_Fi__c = 'NIP',
                                                      ULiT_Negoptim__Name_Leg__c = 'NIP',
                                                      ULiT_Negoptim__Gen_Name_Fi__c = 'NIP',
                                                      ULiT_Negoptim__Gen_Name_Leg__c = 'NIP',
                                                      ULiT_Negoptim__VAT_Type__c = 'Rebate',
                                                      ULiT_Negoptim__Acc_Document_Type__c = 'Credit Note Request');
        // Create NIP Tarrif Condition.
        rp0 = new ULiT_Negoptim__Pol_Com_Condition__c(Name = 'RP0',
                                                      ULiT_Negoptim__Index__c = 'Index3',
                                                      ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1),
                                                      ULiT_Negoptim__Condition_Code__c = 'RP0',
                                                      ULiT_Negoptim__Abrev__c = 'RP0',
                                                      ULiT_Negoptim__Gen_Name_com__c = 'RP0',
                                                      ULiT_Negoptim__Name_Com__c = 'RP0',
                                                      ULiT_Negoptim__Name_Fi__c = 'RP0',
                                                      SPITM_Groupe_nego__c = '10_REMISE_FACTURE',
                                                      ULit_Negoptim__Nego_Discount_type__c = 'Price Discount',
                                                      ULiT_Negoptim__Name_Leg__c = 'RP0',
                                                      ULiT_Negoptim__Gen_Name_Fi__c = 'RP0',
                                                      ULiT_Negoptim__Gen_Name_Leg__c = 'RP0',
                                                      ULiT_Negoptim__VAT_Type__c = 'Rebate',
                                                      ULiT_Negoptim__Acc_Document_Type__c = 'Credit Note Request');
        
        // Create NIP Tarrif Condition.
        en0 = new ULiT_Negoptim__Pol_Com_Condition__c(Name = 'EN0',
                                                      ULiT_Negoptim__Index__c = 'Index3',
                                                      ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1),
                                                      ULiT_Negoptim__Condition_Code__c = 'EN0',
                                                      ULiT_Negoptim__Abrev__c = 'EN0',
                                                      ULiT_Negoptim__Gen_Name_com__c = 'EN0',
                                                      ULiT_Negoptim__Name_Com__c = 'EN0',
                                                      SPITM_Groupe_nego__c = '10_REMISE_FACTURE',
                                                      ULit_Negoptim__Nego_Discount_type__c = 'Price Discount',
                                                      ULiT_Negoptim__Name_Fi__c = 'EN0',
                                                      ULiT_Negoptim__Name_Leg__c = 'EN0',
                                                      ULiT_Negoptim__Gen_Name_Fi__c = 'EN0',
                                                      ULiT_Negoptim__Gen_Name_Leg__c = 'EN0',
                                                      ULiT_Negoptim__VAT_Type__c = 'Rebate',
                                                      ULiT_Negoptim__Acc_Document_Type__c = 'Credit Note Request');
        
        insert new List<ULiT_Negoptim__Pol_Com_Condition__c> {nip, rp0, en0};
            
        Map<String, Id> buRTIds = new Map<String, Id>();
        for(RecordType item : [SELECT Id, DeveloperName
                               FROM RecordType
                               WHERE SobjectType = 'ULiT_Negoptim__Orga_BU__c'
                               AND IsActive = true])
        {
            buRTIds.put(item.DeveloperName, item.Id);
        }
        
        Map<String, Id> conditionRTIds = new Map<String, Id>();
        for(RecordType item : [SELECT Id, DeveloperName
                               FROM RecordType
                               WHERE SobjectType = 'ULiT_Negoptim__Contract_Discount__c'
                               AND IsActive = true])
        {
            conditionRTIds.put(item.DeveloperName, item.Id);
        }
        
        ULiT_Negoptim__Country_List__c countryList = new ULiT_Negoptim__Country_List__c(Name = 'Country',
                                                                                        ULiT_Negoptim__Country_Code__c = 'CTR');
        insert countryList;
        
        ULiT_Negoptim__Orga_BU__c region = new ULiT_Negoptim__Orga_BU__c(Name = 'Region',
                                                                         ULiT_Negoptim__BU_Code__c = 'RGN',
                                                                         ULiT_Negoptim__Country__c = countryList.Name,
                                                                         ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                                                         ULiT_Negoptim__Status__c = 'Open', 
                                                                         RecordTypeId = buRTIds.get('Region'));
        insert region;
        
        ULiT_Negoptim__Orga_BU__c country = new ULiT_Negoptim__Orga_BU__c(Name = 'Country',
                                                                          ULiT_Negoptim__BU_Code__c = 'CTR1',
                                                                          ULiT_Negoptim__ISO_Country_Code__c = countryList.ULiT_Negoptim__Country_Code__c,
                                                                          ULiT_Negoptim__Country__c = countryList.Name,
                                                                          ULiT_Negoptim__Country_Zone_origin__c = region.Id,
                                                                          ULiT_Negoptim__Status__c = 'Open',
                                                                          ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                                                          ULiT_Negoptim__Set_Contract_BU_Target__c = true,
                                                                          RecordTypeId = buRTIds.get('Country'));
        insert country;
        
        ULiT_Negoptim__Sup_Supplier__c supplier = new  ULiT_Negoptim__Sup_Supplier__c(Name = 'supplier1',
                                                                                      ULiT_Negoptim__Code__c = 'code1',
                                                                                      ULiT_Negoptim__Country_origin__c = country.Id,
                                                                                      ULiT_Negoptim__Acc_Country__c = country.Name,
                                                                                      ULiT_Negoptim__Is_Default_NegoScope__c = false,
                                                                                      ULiT_Negoptim__Status__c = 'Active',
                                                                                      ULiT_Negoptim__Acc_Address_External_Synchro__c = false,
                                                                                      ULiT_Negoptim__Admin_Address_External_Synchro__c = false);
        insert supplier;
        
        ULiT_Negoptim__Sup_sup_NegoScope__c NS = new ULiT_Negoptim__Sup_sup_NegoScope__c(Name = 'NS',
                                                                                          ULiT_Negoptim__Supplier__c = supplier.Id,
                                                                                          ULiT_Negoptim__Status__c = 'Active',
                                                                                          ULiT_Negoptim__Acc_Address_External_Synchro__c = false,
                                                                                          ULiT_Negoptim__Admin_Address_External_Synchro__c = false, 
                                                                                          ULiT_Negoptim__Use_Electronic_Signature__c = false);
        
        insert NS;
        
        List<RecordType> contractRecordTypes =  [SELECT Id FROM RecordType WHERE SObjectType = 'ULiT_Negoptim__Contract__c' AND DeveloperName = 'To_Be_Signed'];
        Id toBeSignedRecordTypeId = !contractRecordTypes.isEmpty() ? contractRecordTypes[0].Id : null;
        contract = new ULiT_Negoptim__Contract__c(Name = supplier.Name + ' - Contract ' + year,
                                                  ULiT_Negoptim__Supplier__c = supplier.Id,
                                                  ULiT_Negoptim__Contract_BU__c = supplier.ULiT_Negoptim__Country_origin__c,
                                                  ULiT_Negoptim__Supplier_Nego_Scope__c = NS.Id,
                                                  ULiT_Negoptim__Contract_BDate__c = date.newInstance(year, 1, 1),
                                                  ULiT_Negoptim__Contract_EDate__c = date.newInstance(year, 12, 31),
                                                  ULiT_Negoptim__Duration__c = 12,
                                                  ULiT_Negoptim__Contract_Type__c = 'Contract',
                                                  ULiT_Negoptim__Ext_id__c = 'c123',
                                                  ULiT_Negoptim__Contract_Commercial_BDate__c =  Date.newInstance(year, 1, 1),
                                                  ULiT_Negoptim__Contract_Commercial_EDate__c = Date.newInstance(year, 12, 31),
                                                  RecordTypeId = toBeSignedRecordTypeId);
        
        insert contract;
        
        cd1 = new ULiT_Negoptim__Contract_Discount__c(ULiT_Negoptim__Nego_Discount_Type__c = nip.ULiT_Negoptim__Nego_Discount_Type__c,
                                                      ULiT_Negoptim__Condition_Type__c = nip.Id,
                                                      ULiT_Negoptim__Contract__c = contract.Id,
                                                      ULiT_Negoptim__Product_Scope__c = contract.ULiT_Negoptim__Supplier_Nego_Scope__c,
                                                      ULiT_Negoptim__BU_Scope__c = contract.ULiT_Negoptim__BU_Source__c,
                                                      ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1),
                                                      ULiT_Negoptim__Disc_BDate__c = contract.ULiT_Negoptim__Contract_BDate__c,
                                                      ULiT_Negoptim__Disc_EDate__c = contract.ULiT_Negoptim__Contract_EDate__c,
                                                      recordTypeId = conditionRTIds.get('Signed'));
        
        cd2 = new ULiT_Negoptim__Contract_Discount__c(ULiT_Negoptim__Nego_Discount_Type__c = rp0.ULiT_Negoptim__Nego_Discount_Type__c,
                                                      ULiT_Negoptim__Condition_Type__c = rp0.Id,
                                                      ULiT_Negoptim__Contract__c = contract.Id,
                                                      ULiT_Negoptim__Product_Scope__c = contract.ULiT_Negoptim__Supplier_Nego_Scope__c,
                                                      ULiT_Negoptim__BU_Scope__c = contract.ULiT_Negoptim__BU_Source__c,
                                                      ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1),
                                                      ULiT_Negoptim__Disc_BDate__c = contract.ULiT_Negoptim__Contract_BDate__c,
                                                      ULiT_Negoptim__Disc_EDate__c = contract.ULiT_Negoptim__Contract_EDate__c,
                                                      recordTypeId = conditionRTIds.get('To_Be_Signed'));
        
        cd3 = new ULiT_Negoptim__Contract_Discount__c(ULiT_Negoptim__Nego_Discount_Type__c = en0.ULiT_Negoptim__Nego_Discount_Type__c,
                                                      ULiT_Negoptim__Condition_Type__c = en0.Id,
                                                      ULiT_Negoptim__Contract__c = contract.Id,
                                                      ULiT_Negoptim__Product_Scope__c = contract.ULiT_Negoptim__Supplier_Nego_Scope__c,
                                                      ULiT_Negoptim__BU_Scope__c = contract.ULiT_Negoptim__BU_Source__c,
                                                      ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1),
                                                      ULiT_Negoptim__Disc_BDate__c = contract.ULiT_Negoptim__Contract_BDate__c,
                                                      ULiT_Negoptim__Disc_EDate__c = contract.ULiT_Negoptim__Contract_EDate__c,
                                                      recordTypeId = conditionRTIds.get('Signed'));
        
        insert new List<ULiT_Negoptim__Contract_Discount__c> {cd1, cd2, cd3};
    }
    
    testmethod static void case1_add_Conditions() {
        Test.startTest();
        init();
        Test.stopTest();
        
        System.assert([SELECT SPITM_Is_Include_NIP__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Include_NIP__c);
        System.assert([SELECT SPITM_Is_Include_Promo_Condition__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Include_Promo_Condition__c);
    }

     testmethod static void case2_delete_Conditions() {
         init();
         Test.startTest();
         contract.ULiT_Negoptim__Is_Managed_By_ExternalSource__c = true;
         update contract;
         delete new List<ULiT_Negoptim__Contract_Discount__c> {cd1, cd2};
         System.assert([SELECT SPITM_Is_Include_Promo_Condition__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Include_Promo_Condition__c);
         delete cd3;
         contract.ULiT_Negoptim__Is_Managed_By_ExternalSource__c = false;
         update contract;
         Test.stopTest();
         
         System.assert(![SELECT SPITM_Is_Include_NIP__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Include_NIP__c);
         System.assert(![SELECT SPITM_Is_Include_Promo_Condition__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Include_Promo_Condition__c);
    }
    
    /*testmethod static void case3_executeBatch() {
        Test.startTest();
        init();
        Database.executeBatch(new UpdateContractPromoConditionFieldsBatch('UNIT_TEST', new List<Id> {contract.Id}));
        Test.stopTest();
        
        System.assert([SELECT SPITM_Is_Include_NIP__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Include_NIP__c);
        System.assert([SELECT SPITM_Is_Include_Promo_Condition__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Include_Promo_Condition__c);
    }*/

    testmethod static void case4_update_SRP_Correction() {
        init();
        Test.startTest();
        contract.ULiT_Negoptim__Is_Managed_By_ExternalSource__c = false;
        contract.ULiT_Negoptim__TO1__c = 10000;
        update contract;
        nip.ULiT_Negoptim__Index__c = 'Index2';
        nip.ULiT_Negoptim__Is_Tariff_Condition__c = false;
        nip.ULiT_Negoptim__Is_FlatPrice_Condition__c = false;
        update nip;
        cd1.ULiT_Negoptim__Is_Tariff_Condition__c = false;
        cd1.ULiT_Negoptim__Index_perc__c  = 10;
        cd1.ULiT_Negoptim__Value_amt__c = 1000;
        update cd1;
        Test.stopTest();
        contract = [SELECT SPITM_SRP_Correction__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id];
        cd1 = [SELECT SPITM_SRP_Correction__c, ULiT_Negoptim__X2015_Nego_p__c FROM ULiT_Negoptim__Contract_Discount__c WHERE Id = :cd1.Id];
        System.assertEquals(cd1.SPITM_SRP_Correction__c, cd1.ULiT_Negoptim__X2015_Nego_p__c);
        System.assertEquals(cd1.SPITM_SRP_Correction__c, contract.SPITM_SRP_Correction__c);
    }
}