/**
 * @author ULiT
 * @date 02/10/2021
 * @description unit test class for Trg_Contract trigger
******************************************************************************************************************************************* */
@isTest
private class Trg_Contract_Test {

    private static Integer randomNumber;
    private static Integer year;
    private static ULit_Negoptim__Orga_BU__c region;
    private static ULit_Negoptim__Orga_BU__c country;
    private static ULit_Negoptim__Sup_Supplier__c supplier;
    private static ULit_Negoptim__Sup_sup_NegoScope__c NS;

    static ULiT_Negoptim__Contract__c negoBaseContract;
    static ULiT_Negoptim__Contract__c negoBaseInitContract;
    static ULiT_Negoptim__Contract__c toBeSignedContract;
    static ULiT_Negoptim__Contract__c peContract;
    static ULiT_Negoptim__Contract__c simulationContract1;
    static ULiT_Negoptim__Contract__c simulationContract2;

    public static final Map<String, Id> contractRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Contract__c.SObjectType);
    public static final Map<String, Id> buRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Orga_BU__c.SObjectType);

    static void init() {
        year = System.Today().year();
        // Add Country
        ULit_Negoptim__Country_List__c myCountry = createCountry(true, 'FRANCE', 'FR');
        // Create Region
        region = createBURegion(true, 'EU');
        // Create Country
        country = createBUCountry(true, myCountry, region.Id);
        // Create Supplier
        supplier = createSupplier(false, country, true);
        insert supplier;
        // Get the default NegoScope created on supplier creation
        NS = getNSs(supplier.Id)[0];
        // Create Contracts
        // Create Contract - Nego Base
        negoBaseContract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        negoBaseContract.Name = supplier.Name + ' - Contract ' + year;
        negoBaseContract.ULit_Negoptim__TO1__c = 1000;
        negoBaseContract.ULit_Negoptim__Contract_Numbder__c = 'C001';
        negoBaseContract.ULit_Negoptim__Contract_Type__c = 'Contract';
        negoBaseContract.ULit_Negoptim__Status__c = 'In preparation';
        negoBaseContract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Nego_Base'));
        // Create Contract - Nego Base Init
        negoBaseInitContract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        negoBaseInitContract.Name = supplier.Name + ' - Contract ' + year;
        negoBaseInitContract.ULit_Negoptim__TO1__c = 1000;
        negoBaseInitContract.ULit_Negoptim__Contract_Numbder__c = 'C002';
        negoBaseInitContract.ULit_Negoptim__Contract_Type__c = 'Contract';
        negoBaseInitContract.ULit_Negoptim__Status__c = 'In preparation';
        negoBaseInitContract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Nego_Base_Init'));
        // Create Contract - Period Extension
        peContract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 2, 28));
        peContract.Name = supplier.Name + ' - Contract ' + year;
        peContract.ULit_Negoptim__TO1__c = 1000;
        peContract.ULit_Negoptim__Contract_Numbder__c = 'C003';
        peContract.ULit_Negoptim__Contract_Type__c = 'Contract';
        peContract.ULit_Negoptim__Status__c = 'Validated';
        peContract.SPITM_Is_3Net_Contract__c = true;
        peContract.SPITM_Is_Excluded_To_Nego_Summary__c = true;
        peContract.SPITM_Accord_Non_Ristournble__c = true;
        peContract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Period_Extension'));
        // Create Contract - To Be Signed
        toBeSignedContract = createContract(false, NS, date.newInstance(year, 3, 1), date.newInstance(year, 12, 31));
        toBeSignedContract.Name = supplier.Name + ' - Contract ' + year;
        toBeSignedContract.ULit_Negoptim__TO1__c = 1000;
        toBeSignedContract.ULit_Negoptim__Contract_Numbder__c = 'C004';
        toBeSignedContract.ULit_Negoptim__Contract_Type__c = 'Contract';
        toBeSignedContract.ULit_Negoptim__Status__c = 'Validated';
        toBeSignedContract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('To_Be_Signed'));
        // Create Contract - Simulation
        simulationContract1 = createContract(false, NS, date.newInstance(year, 3, 1), date.newInstance(year, 12, 31));
        simulationContract1.Name = supplier.Name + ' - Contract ' + year;
        simulationContract1.ULit_Negoptim__TO1__c = 1000;
        simulationContract1.ULit_Negoptim__Contract_Numbder__c = 'C005';
        simulationContract1.ULit_Negoptim__Contract_Type__c = 'Contract';
        simulationContract1.ULit_Negoptim__Status__c = 'In preparation';
        simulationContract1.ULiT_Negoptim__Is_OnTop_Simulation__c = true;
        simulationContract1.SPITM_AftSales_Service_LastResponse_Date__c = date.newInstance(year, 3, 1);
        simulationContract1.SPITM_AftSales_Service_Last_Request_Date__c = date.newInstance(year, 3, 1);
        simulationContract1.SPITM_Is_Annex3_Sent_By_Partner__c = true;
        simulationContract1.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Simulation'));
        // Create Contract - Simulation
        simulationContract2 = createContract(false, NS, date.newInstance(year, 3, 1), date.newInstance(year, 12, 31));
        simulationContract2.Name = supplier.Name + ' - Contract ' + year;
        simulationContract2.ULit_Negoptim__TO1__c = 1000;
        simulationContract2.ULit_Negoptim__Contract_Numbder__c = 'C006';
        simulationContract2.ULit_Negoptim__Contract_Type__c = 'Contract';
        simulationContract2.ULit_Negoptim__Status__c = 'In preparation';
        simulationContract2.ULiT_Negoptim__Is_OnTop_Simulation__c = false;
        simulationContract2.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Simulation'));
        insert new List<ULiT_Negoptim__Contract__c> {negoBaseContract, negoBaseInitContract, toBeSignedContract, peContract, simulationContract1, simulationContract2};
    }

    testmethod static void case1_populateFields() {
        Test.startTest();
        init();
        Test.stopTest();
        ULiT_Negoptim__Contract__c contract = peContract.clone(false);
        contract.ULit_Negoptim__Contract_Numbder__c = 'C007';
        contract.ULiT_Negoptim__Contract_Commercial_BDate__c = peContract.ULiT_Negoptim__Contract_Commercial_BDate__c.addYears(1);
        contract.ULiT_Negoptim__Contract_BDate__c = contract.ULiT_Negoptim__Contract_Commercial_BDate__c;
        contract.ULiT_Negoptim__Contract_Commercial_EDate__c = peContract.ULiT_Negoptim__Contract_Commercial_EDate__c.addYears(1);
        contract.ULiT_Negoptim__Contract_EDate__c = contract.ULiT_Negoptim__Contract_Commercial_EDate__c;
        contract.ULiT_Negoptim__Parent_Contract__c = peContract.Id;
        insert contract;
        System.assertEquals(peContract.SPITM_Is_3Net_Contract__c, [SELECT SPITM_Is_3Net_Contract__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_3Net_Contract__c);
        System.assertEquals(peContract.SPITM_Is_Excluded_To_Nego_Summary__c, [SELECT SPITM_Is_Excluded_To_Nego_Summary__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Is_Excluded_To_Nego_Summary__c);
        System.assertEquals(peContract.SPITM_Accord_Non_Ristournble__c, [SELECT SPITM_Accord_Non_Ristournble__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id].SPITM_Accord_Non_Ristournble__c);
    }

    testmethod static void case2_autoUpdatePE() {
        Test.startTest();
        init();
        Test.stopTest();
        toBeSignedContract.ULiT_Negoptim__Status__c = 'Signed';
        update toBeSignedContract;
        System.assertEquals('SPITM_Rien_A_Signer', [SELECT ULiT_Negoptim__Status__c FROM ULiT_Negoptim__Contract__c WHERE Id = :peContract.Id].ULiT_Negoptim__Status__c);
    }

    testmethod static void case3_validateNBI() {
        Test.startTest();
        init();
        Test.stopTest();
        negoBaseInitContract.ULiT_Negoptim__Status__c = 'Validated';
        update negoBaseInitContract;
        System.assertEquals('Validated', [SELECT ULiT_Negoptim__Status__c FROM ULiT_Negoptim__Contract__c WHERE Id = :negoBaseContract.Id].ULiT_Negoptim__Status__c);
    }

    testmethod static void case4_populateSignatories() {
        Test.startTest();
        init();
        Test.stopTest();
        Contact signerContact = new Contact(Title = 'S1', LastName = 'L1');
        insert signerContact;
        ULiT_Negoptim__Contact_Operational_Directory__c contactOperationalDirectory = new ULiT_Negoptim__Contact_Operational_Directory__c(ULiT_Negoptim__Contact__c = signerContact.Id,
                                                                                                                                          ULiT_Negoptim__Supplier__c = supplier.Id,
                                                                                                                                          ULiT_Negoptim__IsActive__c = true,
                                                                                                                                          ULiT_Negoptim__IsSignatory__c = true,
                                                                                                                                          ULiT_Negoptim__Nego_Scope__c = NS.Id);
        insert contactOperationalDirectory;
        ULiT_Negoptim__Contract__c simulationContract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        simulationContract.Name = supplier.Name + ' - Contract ' + year;
        simulationContract.ULit_Negoptim__TO1__c = 1000;
        simulationContract.ULit_Negoptim__Contract_Numbder__c = 'C010';
        simulationContract.ULit_Negoptim__Contract_Type__c = 'Simulation';
        simulationContract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, 'Simulation');
        simulationContract.ULiT_Negoptim__Status__c = 'Validated';
        insert simulationContract;
        System.assertEquals(signerContact.Id, [SELECT ULiT_Negoptim__PartnerSigned_By__c FROM ULiT_Negoptim__Contract__c WHERE Id = :simulationContract.Id].ULiT_Negoptim__PartnerSigned_By__c);
    }

    testmethod static void case5_changeTopContract() {
        Test.startTest();
        init();
        Test.stopTest();
        simulationContract2.ULiT_Negoptim__Is_OnTop_Simulation__c = true;
        update simulationContract2;
        simulationContract2 = [SELECT SPITM_AftSales_Service_LastResponse_Date__c, SPITM_AftSales_Service_Last_Request_Date__c, SPITM_Is_Annex3_Sent_By_Partner__c
                               FROM ULiT_Negoptim__Contract__c
                               WHERE Id = :simulationContract2.Id];
        System.assertEquals(simulationContract1.SPITM_AftSales_Service_LastResponse_Date__c, simulationContract2.SPITM_AftSales_Service_LastResponse_Date__c);
        System.assertEquals(simulationContract1.SPITM_AftSales_Service_Last_Request_Date__c, simulationContract2.SPITM_AftSales_Service_Last_Request_Date__c);
        System.assertEquals(simulationContract1.SPITM_Is_Annex3_Sent_By_Partner__c, simulationContract2.SPITM_Is_Annex3_Sent_By_Partner__c);
    }

    /* BUSINESS UNIT */
    // Country List creation.
    private static ULit_Negoptim__Country_List__c createCountry(Boolean doInsert, String name, String code) {
        ULit_Negoptim__Country_List__c myCountry = new ULit_Negoptim__Country_List__c(Name = name, ULit_Negoptim__Country_Code__c = code);
        if (doInsert) insert myCountry;
        System.assert(true);
        return myCountry;
    }

    // Region creation.
    private static ULit_Negoptim__Orga_BU__c createBURegion(Boolean doInsert, String name) {
        ULit_Negoptim__Orga_BU__c region = new ULit_Negoptim__Orga_BU__c(Name = name, ULit_Negoptim__BU_Code__c = name, ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Open',
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                           ULit_Negoptim__Set_BU_List_Entity__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                           ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                           ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Payment__c = true,
                                           ULit_Negoptim__Set_Invoice__c = true,
                                           ULit_Negoptim__Set_Product_Listing__c = true,
                                           ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                           ULit_Negoptim__Set_SF_Planning__c = true,
                                           ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                           ULit_Negoptim__Set_Budget_Maker__c = true,
                                           ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                           ULit_Negoptim__Set_PriceList_Statement__c = true);
        region.RecordTypeId = buRTIds.get('Region');
        if (doInsert) insert region;
        System.assert(true);
        return region;
    }

    // Country creation.
    private static ULit_Negoptim__Orga_BU__c createBUCountry(Boolean doInsert, ULit_Negoptim__Country_List__c c, Id regionId) {
        ULit_Negoptim__Orga_BU__c country = new ULit_Negoptim__Orga_BU__c(Name = c.Name, ULit_Negoptim__BU_Code__c = c.ULit_Negoptim__Country_Code__c + getRandomNumber(), ULit_Negoptim__ISO_Country_Code__c = c.ULit_Negoptim__Country_Code__c,
                                            ULit_Negoptim__Country__c = c.Name, ULit_Negoptim__Country_Zone_origin__c = regionId, ULit_Negoptim__Status__c = 'Open',
                                            ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                            ULit_Negoptim__Set_BU_List_Entity__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                            ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                            ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Payment__c = true,
                                            ULit_Negoptim__Set_Invoice__c = true,
                                            ULit_Negoptim__Set_Product_Listing__c = true,
                                            ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                            ULit_Negoptim__Set_SF_Planning__c = true,
                                            ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                            ULit_Negoptim__Set_Budget_Maker__c = true,
                                            ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                            ULit_Negoptim__Set_PriceList_Statement__c = true);
        country.RecordTypeId = buRTIds.get('Country');
        if (doInsert) insert country;
        System.assert(true);
        return country;
    }

    /* SUPPLIER + NEGOSCOPE */
    // Supplier creation.
    private static ULit_Negoptim__Sup_Supplier__c createSupplier(Boolean doInsert, ULit_Negoptim__Orga_BU__c country, Boolean withNS) {
    	String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Sup_Supplier__c supplier = new ULit_Negoptim__Sup_Supplier__c(Name = 'SUP ' + rdm, ULit_Negoptim__Code__c = rdm, ULit_Negoptim__Country_origin__c = country.Id, ULit_Negoptim__Acc_Country__c = country.Name,
        								ULit_Negoptim__Is_Default_NegoScope__c = withNS, ULit_Negoptim__Acc_Address_External_Synchro__c = false, ULit_Negoptim__Admin_Address_External_Synchro__c = false,
        								ULit_Negoptim__Status__c = 'Active');
        if (doInsert) insert supplier;
        System.assert(true);
        return supplier;
    }

    // Get the list of NS related to the supplier passed by parameter.
	private static List<ULit_Negoptim__Sup_sup_NegoScope__c> getNSs(Id SupplierId) {
		System.assert(true);
        return [SELECT Id, Name, ULit_Negoptim__Supplier__c, ULit_Negoptim__Supplier__r.Name, ULit_Negoptim__Supplier__r.ULit_Negoptim__Code__c, ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                OwnerId, ULit_Negoptim__NS_Code__c, ULit_Negoptim__NS_Code_Long__c
                FROM ULit_Negoptim__Sup_sup_NegoScope__c
                WHERE ULit_Negoptim__Supplier__c = :supplierId];

 	}

     /* CONTRACT + CONDITION */
    // Contract creation.
    private static ULit_Negoptim__Contract__c createContract(Boolean doInsert, ULit_Negoptim__Sup_sup_NegoScope__c NS, Date beginDate, Date endDate) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Contract__c contract = new ULit_Negoptim__Contract__c(Name = NS.ULit_Negoptim__Supplier__r.Name + ' - Contract ' + year, ULit_Negoptim__Supplier__c = NS.ULit_Negoptim__Supplier__c,
                                               ULit_Negoptim__Supplier_Nego_Scope__c = NS.Id, ULit_Negoptim__Contract_BU__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                                               ULit_Negoptim__BU_Source__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c, ULit_Negoptim__Contract_BDate__c = beginDate,
                                               ULit_Negoptim__Contract_EDate__c = endDate, ULit_Negoptim__Duration__c = beginDate.monthsBetween(endDate) + 1,
                                               ULit_Negoptim__Contract_Commercial_BDate__c = beginDate, ULit_Negoptim__Contract_Commercial_EDate__c = endDate,
                                               ULit_Negoptim__Contract_Type__c = 'Contract', ULit_Negoptim__Status__c = 'Signed', ULit_Negoptim__Ext_id__c = rdm,
                                               ULit_Negoptim__Duration_type__c = 'Month', ULit_Negoptim__Contract_Numbder__c = 'c' + rdm,
                                               RecordTypeId = contractRTIds.get('To_Be_Signed'));
        if (doInsert) Database.insert(contract);
		System.assert(true);
        return contract;
    }

    // Method to increment randomNumber
    private static Integer getRandomNumber() {
        if (randomNumber == null)
            randomNumber = (Integer)(Math.random()*9999);
        return randomNumber++;
    }

    /**
     * Get Object RecordType Map of Name/Id
     * @param sObjectType
     * */
    public static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();
        // Check Accessibility.
        // if(!checkAccessibilityFields(Schema.SObjectType.RecordType.fields.getMap(), new String [] {'Id', 'DeveloperName'})) {
        // 	return rtMap;
        // }
        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }
    /**
     * Get Object RecordType Id by Name
     * @param sObjectType
     * @param recordTypeName
     * @example: Id recordTypeId = NegoptimHelper.getObjectRecordTypeId(ULit_Negoptim__Orga_BU__c.SObjectType, 'Country');
     * */
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName) {
        // If sObjectType is wrong, then an Exception is thrown.
        Id rt;
        Map<Id, Schema.RecordTypeInfo> sObjectTypeRecordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosById();
        for (Id RTId : sObjectTypeRecordTypeInfo.keySet()) {
            if (sObjectTypeRecordTypeInfo.get(RTId).getDeveloperName() == recordTypeName) {
                rt = RTId;
            }
        }
        if (rt == null) {
            // If recordTypeName is wrong, then an Exception is thrown.
            throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.');
        }
        //Return the record type id
        return rt;
    }
    public class RecordTypeException extends Exception {}
}