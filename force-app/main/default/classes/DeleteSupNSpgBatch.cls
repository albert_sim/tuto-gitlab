/**
 * @author ULIT
 // Database.executeBatch(new DeleteSupNSpgBatch('Dev Console', null), 10);
 * */
public class DeleteSupNSpgBatch implements Database.Batchable<SObject> {

    private ULit_Negoptim.NegoptimBatch nb;
    public final String query;

    public DeleteSupNSpgBatch(String startedFrom, Set<Id> setIDs) {
        this.nb = new ULit_Negoptim.NegoptimBatch(DeleteSupNSpgBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateless, startedFrom);
        String q = 'SELECT Id, Name FROM ULiT_Negoptim__Sup_NS_PG__c ';
        q += 'WHERE ULiT_Negoptim__Sup_PG__r.ULiT_Negoptim__Product__r.recordtypeId = \'0129E000000hHGFQA2\'';
        if (setIDs != null && !setIDs.isEmpty()) {
            q += ' AND Id IN (\'' + String.join(new List<Id>(setIDs), '\',\'') +'\')';
        }
        this.query = q;
        nb.logParameter('setIDs', setIDs);
        nb.logParameter('query', this.query);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    // Execute method implementation.
    public void execute(Database.BatchableContext BC, List<ULiT_Negoptim__Sup_NS_PG__c > scope) {
        // Delete Contract Discounts
        List<Database.DeleteResult> results = Database.delete(scope, false);
        for (Integer i = 0; i < results.size(); i++) {
            Database.DeleteResult result = results.get(i);
            if (!result.isSuccess()) {
                String errorMessage = 'Sup NS PG : ' + '\n';
                Database.Error[] errors = result.getErrors();
                for (Database.Error err : errors) {
                    errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                }
                nb.logError(errorMessage);
            }
        }
        nb.saveLog(bc);
    }

    public void finish(Database.BatchableContext BC) {
        nb.sendEmail(bc, null, 'DeleteSupNSpgBatch');
    }
}