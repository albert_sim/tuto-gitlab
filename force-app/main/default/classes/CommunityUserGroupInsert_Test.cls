/*
 * author : ULit
 * @date 07 october 2021
 * */
@isTest
public class CommunityUserGroupInsert_Test {
    private static UserRole role;
    private static User currentUser;
    private static User communityUser;
    private static Group groupInstance;
    private static Contact communityUsercontact;
    private static Account communityUserAccount;
    
    @isTest 
    static void testCase() {
        //update user (give it a role)
        currentUser = new User(Id = UserInfo.getUserId());
        if (currentUser.UserRoleId == null) {
            role = [SELECT Id FROM UserRole Limit 1];
            currentUser.UserRoleId = role.Id;
            update currentUser;
        }
        System.runAs(currentUser) {
            Test.startTest();
            //Create Account
            communityUserAccount = new Account(Name = 'PortalAccount');
            insert communityUserAccount;
            //Create Contact
            communityUsercontact = new Contact(FirstName = 'PortalUser', LastName = 'Contact', Email = 'CommunityUser@email.com', 
                                               ULiT_Negoptim__Is_User_Active__c = true, ULiT_Negoptim__Default_Username__c = 'CommunityUser@email.com',
                                               ULiT_Negoptim__Is_Community_User__c = true, AccountId = communityUserAccount.Id);
            insert communityUsercontact;
            //Create Group
            groupInstance =  new Group(Name = 'Group test', DeveloperName = 'Group_test');
            insert groupInstance;
            //Create User Community
            Id profileId = [SELECT Id FROM Profile WHERE Name = 'ITM - Utilisateur Portail Fournisseur' LIMIT 1]?.Id;
            Organization organization = [SELECT Id, Name, TimeZoneSidKey, LanguageLocaleKey, DefaultLocaleSidKey, isSandbox FROM Organization];
            Blob key = Crypto.generateAesKey(128);
            String uniqueString = EncodingUtil.convertToHex(key);
            communityUser = new User();
            communityUser.Username = communityUsercontact.ULiT_Negoptim__Default_Username__c;
            communityUser.ContactId = communityUsercontact.Id;
            communityUser.ProfileId = profileId;
            communityUser.alias = uniqueString.substring(0, 8);
            communityUser.CommunityNickname = uniqueString.substring(0, 8);
            communityUser.IsActive = communityUsercontact.ULiT_Negoptim__Is_User_Active__c;
            communityUser.FirstName = communityUsercontact.FirstName;
            communityUser.LastName = communityUsercontact.LastName;
            communityUser.Phone = communityUsercontact.Phone;
            communityUser.Email = communityUsercontact.email;
            communityUser.EmailEncodingKey = 'UTF-8';
            communityUser.TimeZoneSidKey = organization.TimeZoneSidKey;
            communityUser.LocaleSidKey = organization.DefaultLocaleSidKey;
            communityUser.LanguageLocaleKey = organization.LanguageLocaleKey;
            insert communityUser;
            CommunityUserGroupInsert.InputWrapper invocableActionInputs = new CommunityUserGroupInsert.InputWrapper(communityUser.Id, groupInstance.Id);
            List<CommunityUserGroupInsert.InputWrapper> inputWrapper = new List<CommunityUserGroupInsert.InputWrapper>{invocableActionInputs};
            CommunityUserGroupInsert.insertGroupMember(inputWrapper);
            Test.stopTest();
            List<GroupMember> groupMemberList = [SELECT Id, UserOrGroupId FROM GroupMember WHERE GroupId =: groupInstance.Id AND UserOrGroupId  =: communityUser.Id];
            System.assertEquals(1, groupMemberList.size());
            System.assertEquals(communityUser.Id, groupMemberList.get(0).UserOrGroupId);
       }
    }
}