/**
 * @author ULiT
 * @description unit test class for UpdateContractStatusBatch
 * @date 22 september 2021
*/
@isTest
private class UpdateContractStatusBatchTest {
    
    static ULiT_Negoptim__Contract__c contract;
    
    static void init() {
        
        Integer year = Date.today().year();
        
        Map<String, Id> buRTIds = new Map<String, Id>();
        for(RecordType item : [SELECT Id, DeveloperName
                               FROM RecordType
                               WHERE SobjectType = 'ULiT_Negoptim__Orga_BU__c'
                               AND IsActive = true])
        {
            buRTIds.put(item.DeveloperName, item.Id);
        }
        
        ULiT_Negoptim__Country_List__c countryList = new ULiT_Negoptim__Country_List__c(Name = 'Country',
                                                                                        ULiT_Negoptim__Country_Code__c = 'CTR');
        insert countryList;

        ULiT_Negoptim__Orga_BU__c region = new ULiT_Negoptim__Orga_BU__c(Name = 'Region',
                                                                         ULiT_Negoptim__BU_Code__c = 'RGN',
                                                                         ULiT_Negoptim__Country__c = countryList.Name,
                                                                         ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                                                         ULiT_Negoptim__Status__c = 'Open', 
                                                                         RecordTypeId = buRTIds.get('Region'));
        insert region;
        
        ULiT_Negoptim__Orga_BU__c country = new ULiT_Negoptim__Orga_BU__c(Name = 'Country',
                                                                          ULiT_Negoptim__BU_Code__c = 'CTR1',
                                                                          ULiT_Negoptim__ISO_Country_Code__c = countryList.ULiT_Negoptim__Country_Code__c,
                                                                          ULiT_Negoptim__Country__c = countryList.Name,
                                                                          ULiT_Negoptim__Country_Zone_origin__c = region.Id,
                                                                          ULiT_Negoptim__Status__c = 'Open',
                                                                          ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                                                          ULiT_Negoptim__Set_Contract_BU_Target__c = true,
                                                                          RecordTypeId = buRTIds.get('Country'));
        insert country;
        
        ULiT_Negoptim__Sup_Supplier__c supplier = new  ULiT_Negoptim__Sup_Supplier__c(Name = 'supplier1',
                                                                                      ULiT_Negoptim__Code__c = 'code1',
                                                                                      ULiT_Negoptim__Country_origin__c = country.Id,
                                                                                      ULiT_Negoptim__Acc_Country__c = country.Name,
                                                                                      ULiT_Negoptim__Is_Default_NegoScope__c = false,
                                                                                      ULiT_Negoptim__Status__c = 'Active',
                                                                                      ULiT_Negoptim__Acc_Address_External_Synchro__c = false,
                                                                                      ULiT_Negoptim__Admin_Address_External_Synchro__c = false);
        insert supplier;
        
        ULiT_Negoptim__Sup_sup_NegoScope__c NS = new ULiT_Negoptim__Sup_sup_NegoScope__c(Name = 'NS',
                                                                                          ULiT_Negoptim__Supplier__c = supplier.Id,
                                                                                          ULiT_Negoptim__Status__c = 'Active',
                                                                                          ULiT_Negoptim__Acc_Address_External_Synchro__c = false,
                                                                                          ULiT_Negoptim__Admin_Address_External_Synchro__c = false, 
                                                                                          ULiT_Negoptim__Use_Electronic_Signature__c = false);
        
        insert NS;
        List<RecordType> contractRecordTypes =  [SELECT Id FROM RecordType WHERE SObjectType = 'ULiT_Negoptim__Contract__c' AND DeveloperName = 'Period_Extension'];
        Id periodExtensionRecordTypeId = !contractRecordTypes.isEmpty() ? contractRecordTypes[0].Id : null;
        contract = new ULiT_Negoptim__Contract__c(Name = supplier.Name + ' - Contract ' + year,
                                                  ULiT_Negoptim__Supplier__c = supplier.Id,
                                                  ULiT_Negoptim__Contract_BU__c = supplier.ULiT_Negoptim__Country_origin__c,
                                                  ULiT_Negoptim__Supplier_Nego_Scope__c = NS.Id,
                                                  ULiT_Negoptim__Contract_BDate__c = date.newInstance(year, 1, 1),
                                                  ULiT_Negoptim__Contract_EDate__c = date.newInstance(year, 12, 31),
                                                  ULiT_Negoptim__Duration__c = 12,
                                                  ULiT_Negoptim__Contract_Type__c = 'Contract',
                                                  ULiT_Negoptim__Ext_id__c = 'c123',
                                                  ULiT_Negoptim__Contract_Commercial_BDate__c =  Date.newInstance(year, 1, 1),
                                                  ULiT_Negoptim__Contract_Commercial_EDate__c = Date.newInstance(year, 12, 31),
                                                  ULiT_Negoptim__Status__c = 'In preparation',
                                                  recordTypeId = periodExtensionRecordTypeId);
        
        insert contract;
    }
    
    testmethod static void case1() {
        init();
        Test.startTest();
        UpdateContractStatusSchedule sch = new UpdateContractStatusSchedule();
        sch.execute(null);
        // Database.executeBatch(new UpdateContractStatusBatch('UNIT_TEST'));
        Test.stopTest();
        contract = [SELECT Id, ULiT_Negoptim__Status__c FROM ULiT_Negoptim__Contract__c WHERE Id = :contract.Id];
        System.assertEquals('SPITM_Sans_Signature', contract.ULiT_Negoptim__Status__c);
    }
}