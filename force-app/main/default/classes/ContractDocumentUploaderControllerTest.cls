/*
 * author : ULit
 * @date 23 Serptember 2021
 * @version 1.0
 * */
@isTest
private with sharing class ContractDocumentUploaderControllerTest {
    public ContractDocumentUploaderControllerTest() {

    }
    private static Integer year;
    private static ContentVersion payload;
    private static UploadedFileWrapper fileWrapper;
    public static final Map<String, Id> contractRTIds = getObjectRecordTypeMapIds(ULiT_Negoptim__Contract__c.SObjectType);
    public static final Map<String, Id> buRTIds = getObjectRecordTypeMapIds(ULiT_Negoptim__Orga_BU__c.SObjectType);
    private static Id rootId;
    private static String currencyIsoCode;
    private static ULiT_Negoptim__Orga_BU__c region;
    private static ULiT_Negoptim__Orga_BU__c country;
    private static ULiT_Negoptim__Orga_BU__c legal;
    private static ULiT_Negoptim__Orga_BU__c store;
    private static ULiT_Negoptim__Sup_Supplier__c supplier;
    private static ULiT_Negoptim__Sup_sup_NegoScope__c NS;
    private static ULiT_Negoptim__Contract__c contract;
    private static Integer randomNumber;
    // Method to increment randomNumber
    private static Integer getRandomNumber() {
        if (randomNumber == null)
            randomNumber = (Integer)(Math.random()*9999);
        return randomNumber++;
    }
    static void init() {
        try {
            year = System.Today().year();
            // Root Element Id
            rootId = getRootId();
            // Get the Corporate Currency.
			currencyIsoCode = 'EUR';
            // Get all recortd type ids for Orga_BU__c SObject
            Map<String, Id> buRTIds = getObjectRecordTypeMapIds(ULiT_Negoptim__Orga_BU__c.SObjectType);
            // Add Country
            ULiT_Negoptim__Country_List__c myCountry = createCountry(true, 'FRANCE', 'FR');
            // Create Region
            region = createBURegion(true, 'EU');
            // Create Country
            country = createBUCountry(true, myCountry, region.Id);
            // Create Legal Form.
            legal = createBULegal(true, myCountry);
            // Create Store
            store = createBUStores(true, country, legal.Id, 3).get(0);
            // Create Supplier
            supplier = createSupplier(false, country, true);
            insert supplier;
            // Get the default NegoScope created on supplier creation
            NS = getNSs(supplier.Id)[0];
            // Create Contract
            contract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
            contract.Name = supplier.Name + ' - Contract ' + year;
            contract.ULiT_Negoptim__Contract_Numbder__c = 'C001';
            contract.ULiT_Negoptim__Contract_Type__c = 'Contract';
            contract.ULiT_Negoptim__D_N__c = 'N';
            contract.RecordTypeId = getObjectRecordTypeId(ULiT_Negoptim__Contract__c.SObjectType, ('To_Be_Signed'));
            insert new List<ULiT_Negoptim__Contract__c>{contract};
        } catch (Exception ex) {
            System.debug('++++++++++++++++++++++++++++++');
            System.debug(ex.getMessage() );
            System.debug(ex.getStackTraceString());
            System.debug('++++++++++++++++++++++++++++++');
        }
        try {
            payload = new ContentVersion();
            fileWrapper = new UploadedFileWrapper();
            /**
             * Create payload CV
             */
            String concatinatedPayloadBodies = 'edfgdrsgrtshyrstghysrtghysrt5hy';
            payload.Title = 'TEST.test';
            payload.PathOnClient = 'TEST';
            payload.VersionData = Blob.valueOf(concatinatedPayloadBodies);
            insert payload;
            fileWrapper.contentVersionId = payload.Id;
            Map<Id,ContentVersion> contentVersionFullData;
            // Get the content version Ids in a Map
            // Get All data from Database
            contentVersionFullData =  new Map<Id, ContentVersion>([SELECT Id,ContentDocumentId,VersionData,FileType,Title
                                                                    FROM ContentVersion
                                                                    WHERE Id = :payload.Id]);
            fileWrapper.documentId = contentVersionFullData.get(payload.Id).ContentDocumentId;
            fileWrapper.name = 'TEST';
            List<ContentDocumentLink> contentDocumentLinkList = new List<ContentDocumentLink>();
            // Add the Contentversion to the record (parentId) via ContentDocumentLink for both CVs
            ContentDocumentLink payloadCDL = new ContentDocumentLink();
            payloadCDL.LinkedEntityId = contract.Id;
            // Get the ContentDocumentId from Database (ContentDocumentId generate after insert)
            payloadCDL.ContentDocumentId = contentVersionFullData.get(payload.Id).ContentDocumentId;
            insert payloadCDL;
        } catch (Exception ex) {
            System.debug('The following exception has occurred: ' + ex.getMessage());
            System.debug('Error Cause:'+ex.getCause());
            System.debug('Error Line:'+ex.getLineNumber());
        }
    }
    @isTest static void uploadFile_case1() {
        init();
        ContractDocumentUploaderController.createContractDocument(JSON.serialize(new List<UploadedFileWrapper>{fileWrapper}), contract.Id);
        system.assert(true);
    }
    public static Id getRootId() {
            List<ULiT_Negoptim__Orga_HE__c> elements = [SELECT Id FROM ULiT_Negoptim__Orga_HE__c WHERE ULiT_Negoptim__Parent_Element__c = null];
            if(elements != NULL && elements.size() > 0) {
                return elements.get(0).Id;
            } else {
                ULiT_Negoptim__Orga_HE__c root = new ULiT_Negoptim__Orga_HE__c(Name = 'ROOT', ULiT_Negoptim__Elt_Code__c = 'ROOT', ULiT_Negoptim__Parent_Element__c = null, ULiT_Negoptim__Level__c = 0, ULiT_Negoptim__Status_BDate__c = date.newInstance(System.today().year(), 1, 1));
                insert root;
                return root.Id;
            }
    }
    /**
     * Get Object RecordType Id by Name
     * @param sObjectType
     * @param recordTypeName
     * @example: Id recordTypeId = NegoptimHelper.getObjectRecordTypeId(Orga_BU__c.SObjectType, 'Country');
     * */
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName) {
        // If sObjectType is wrong, then an Exception is thrown.
        Id rt;
        Map<Id, Schema.RecordTypeInfo> sObjectTypeRecordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosById();
        for (Id RTId : sObjectTypeRecordTypeInfo.keySet()) {
            if (sObjectTypeRecordTypeInfo.get(RTId).getDeveloperName() == recordTypeName) {
                rt = RTId;
            }
        }
        //Return the record type id
        return rt;
    }
    /* CONTRACT + CONDITION */
    // Contract creation.
    public static ULiT_Negoptim__Contract__c createContract(Boolean doInsert, ULiT_Negoptim__Sup_sup_NegoScope__c NS, Date beginDate, Date endDate) {
        String rdm = String.valueOf(getRandomNumber());
        ULiT_Negoptim__Contract__c contract = new ULiT_Negoptim__Contract__c(Name = NS.ULiT_Negoptim__Supplier__r.Name + ' - Contract ' + year, ULiT_Negoptim__Supplier__c = NS.ULiT_Negoptim__Supplier__c,
                                               ULiT_Negoptim__Supplier_Nego_Scope__c = NS.Id, ULiT_Negoptim__Contract_BU__c = NS.ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Country_origin__c,
                                               ULiT_Negoptim__BU_Source__c = NS.ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Country_origin__c, ULiT_Negoptim__Contract_BDate__c = beginDate,
                                               ULiT_Negoptim__Contract_EDate__c = endDate, ULiT_Negoptim__Duration__c = beginDate.monthsBetween(endDate) + 1,
                                               ULiT_Negoptim__Contract_Commercial_BDate__c = beginDate, ULiT_Negoptim__Contract_Commercial_EDate__c = endDate,
                                               ULiT_Negoptim__Contract_Type__c = 'Contract', ULiT_Negoptim__Status__c = 'Signed', ULiT_Negoptim__Ext_id__c = rdm, ULiT_Negoptim__D_N__c = 'N',
                                               ULiT_Negoptim__Duration_type__c = 'Month', ULiT_Negoptim__Contract_Numbder__c = 'c' + rdm,
                                               RecordTypeId = contractRTIds.get('To_Be_Signed'), ULiT_Negoptim__Is_Managed_By_ExternalSource__c = false);
        if (doInsert) Database.insert(contract);
		System.assert(true);
        return contract;
    }
    // Get the list of NS related to the supplier passed by parameter.
	public static List<ULiT_Negoptim__Sup_sup_NegoScope__c> getNSs(Id SupplierId) {
		System.assert(true);
        return [SELECT Id, Name, ULiT_Negoptim__Supplier__c, ULiT_Negoptim__Supplier__r.Name, ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Code__c, ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Country_origin__c,
                OwnerId, ULiT_Negoptim__NS_Code__c, ULiT_Negoptim__NS_Code_Long__c
                FROM ULiT_Negoptim__Sup_sup_NegoScope__c
                WHERE ULiT_Negoptim__Supplier__c = :supplierId];

 	}
    /* SUPPLIER + NEGOSCOPE */
    // Supplier creation.
    public static ULiT_Negoptim__Sup_Supplier__c createSupplier(Boolean doInsert, ULiT_Negoptim__Orga_BU__c country, Boolean withNS) {
    	String rdm = String.valueOf(getRandomNumber());
        ULiT_Negoptim__Sup_Supplier__c supplier = new ULiT_Negoptim__Sup_Supplier__c(Name = 'SUP ' + rdm, ULiT_Negoptim__Code__c = rdm, ULiT_Negoptim__Country_origin__c = country.Id, ULiT_Negoptim__Acc_Country__c = country.Name,
        								ULiT_Negoptim__Is_Default_NegoScope__c = withNS, ULiT_Negoptim__Acc_Address_External_Synchro__c = false, ULiT_Negoptim__Admin_Address_External_Synchro__c = false,
        								ULiT_Negoptim__Status__c = 'Active', CurrencyIsoCode = currencyIsoCode);
        if (doInsert) insert supplier;
        System.assert(true);
        return supplier;
    }
    /* BUSINESS UNITS */
    // List of stores creation.
    // Stores should have Country_List__c to assign the name of the Country_List__c to Country__c for every store we are creating instead of
    // assigning the name of Orga_BU__c, it will cause a trigger error (on before insert and update)
	public static List<ULiT_Negoptim__Orga_BU__c> createBUStores(Boolean doInsert, ULiT_Negoptim__Orga_BU__c country, Id legalId, Integer nbr) {
        List<ULiT_Negoptim__Orga_BU__c> stores = new List<ULiT_Negoptim__Orga_BU__c>();
        for(Integer i = 0; i < nbr; i++) {
            ULiT_Negoptim__Orga_BU__c store = new ULiT_Negoptim__Orga_BU__c(Name = 'Store ' + i, ULiT_Negoptim__BU_Code__c = 'Store ' + i, ULiT_Negoptim__Country_origin__c = country.Id,
            								ULiT_Negoptim__Country_Zone_origin__c = country.ULiT_Negoptim__Country_Zone_origin__c, ULiT_Negoptim__Country__c = country.Name,
                                            ULiT_Negoptim__Legal_Form__c = legalId, ULiT_Negoptim__Status__c = 'Open', ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                          	CurrencyIsoCode = currencyIsoCode, ULiT_Negoptim__Set_BU_List_Entity__c = true);
            store.RecordTypeId = buRTIds.get('Store');
            stores.add(store);
        }
        System.debug(JSON.serialize(buRTIds));
        if (doInsert) insert stores;
        System.assert(true);
        return stores;
    }
    public static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        system.debug(' getObjectRecordTypeMapIds >>> ');
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();

        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }
    public static ULiT_Negoptim__Country_List__c createCountry(Boolean doInsert, String name, String code) {
        ULiT_Negoptim__Country_List__c myCountry = new ULiT_Negoptim__Country_List__c(Name = name, ULiT_Negoptim__Country_Code__c = code);
        if (doInsert) insert myCountry;
        System.assert(true);
        return myCountry;
    }
    public static ULiT_Negoptim__Orga_BU__c createBURegion(Boolean doInsert, String name) {
        ULiT_Negoptim__Orga_BU__c region = new ULiT_Negoptim__Orga_BU__c(Name = name, ULiT_Negoptim__BU_Code__c = name, ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULiT_Negoptim__Status__c = 'Open',
                                           ULiT_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                           ULiT_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                           ULiT_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                           ULiT_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                           ULiT_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                           ULiT_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                           ULiT_Negoptim__Set_BU_List_Entity__c = true,
                                           ULiT_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                           ULiT_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                           ULiT_Negoptim__Set_Contract_BU_Source__c = true,
                                           ULiT_Negoptim__Set_Contract_BU_Target__c = true,
                                           ULiT_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                           ULiT_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                           ULiT_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                           ULiT_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                           ULiT_Negoptim__Set_Invoice_Detail__c = true,
                                           ULiT_Negoptim__Set_Invoice_Payment__c = true,
                                           ULiT_Negoptim__Set_Invoice__c = true,
                                           ULiT_Negoptim__Set_Product_Listing__c = true,
                                           ULiT_Negoptim__Set_SF_Data_Collection__c = true,
                                           ULiT_Negoptim__Set_SF_Planning__c = true,
                                           ULiT_Negoptim__Set_Budget_Maker_Detail__c = true,
                                           ULiT_Negoptim__Set_Budget_Maker__c = true,
                                           ULiT_Negoptim__Set_NetPrice_Statement__c = true,
                                           ULiT_Negoptim__Set_PriceList_Statement__c = true);
        region.RecordTypeId = buRTIds.get('Region');
        if (doInsert) insert region;
        System.assert(true);
        return region;
    }
    // Country creation.
    public static ULiT_Negoptim__Orga_BU__c createBUCountry(Boolean doInsert, ULiT_Negoptim__Country_List__c c, Id regionId) {
        ULiT_Negoptim__Orga_BU__c country = new ULiT_Negoptim__Orga_BU__c(Name = c.Name, ULiT_Negoptim__BU_Code__c = c.ULiT_Negoptim__Country_Code__c + getRandomNumber(), ULiT_Negoptim__ISO_Country_Code__c = c.ULiT_Negoptim__Country_Code__c,
                                            ULiT_Negoptim__Country__c = c.Name, ULiT_Negoptim__Country_Zone_origin__c = regionId, ULiT_Negoptim__Status__c = 'Open',
                                            ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                            ULiT_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                            ULiT_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                            ULiT_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                            ULiT_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                            ULiT_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                            ULiT_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                            ULiT_Negoptim__Set_BU_List_Entity__c = true,
                                            ULiT_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                            ULiT_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                            ULiT_Negoptim__Set_Contract_BU_Source__c = true,
                                            ULiT_Negoptim__Set_Contract_BU_Target__c = true,
                                            ULiT_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                            ULiT_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                            ULiT_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                            ULiT_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                            ULiT_Negoptim__Set_Invoice_Detail__c = true,
                                            ULiT_Negoptim__Set_Invoice_Payment__c = true,
                                            ULiT_Negoptim__Set_Invoice__c = true,
                                            ULiT_Negoptim__Set_Product_Listing__c = true,
                                            ULiT_Negoptim__Set_SF_Data_Collection__c = true,
                                            ULiT_Negoptim__Set_SF_Planning__c = true,
                                            ULiT_Negoptim__Set_Budget_Maker_Detail__c = true,
                                            ULiT_Negoptim__Set_Budget_Maker__c = true,
                                            ULiT_Negoptim__Set_NetPrice_Statement__c = true,
                                            ULiT_Negoptim__Set_PriceList_Statement__c = true,
                                            CurrencyIsoCode = currencyIsoCode);
        country.RecordTypeId = buRTIds.get('Country');
        if (doInsert) insert country;
        System.assert(true);
        return country;
    }
    // Legal creation.
    public static ULiT_Negoptim__Orga_BU__c createBULegal(Boolean doInsert, ULiT_Negoptim__Country_List__c c) {
        ULiT_Negoptim__Orga_BU__c legal = new ULiT_Negoptim__Orga_BU__c(Name = 'Legal ' + c.Name, ULiT_Negoptim__BU_Code__c = 'L' + c.ULiT_Negoptim__Country_Code__c, ULiT_Negoptim__Country__c = c.Name,
                                          ULiT_Negoptim__Status__c = 'Open', ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                          ULiT_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                          ULiT_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                          ULiT_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                          ULiT_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                          ULiT_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                          ULiT_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                          ULiT_Negoptim__Set_BU_List_Entity__c = true,
                                          ULiT_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                          ULiT_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                          ULiT_Negoptim__Set_Contract_BU_Source__c = true,
                                          ULiT_Negoptim__Set_Contract_BU_Target__c = true,
                                          ULiT_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                          ULiT_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                          ULiT_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                          ULiT_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                          ULiT_Negoptim__Set_Invoice_Detail__c = true,
                                          ULiT_Negoptim__Set_Invoice_Payment__c = true,
                                          ULiT_Negoptim__Set_Invoice__c = true,
                                          ULiT_Negoptim__Set_Product_Listing__c = true,
                                          ULiT_Negoptim__Set_SF_Data_Collection__c = true,
                                          ULiT_Negoptim__Set_SF_Planning__c = true,
                                          ULiT_Negoptim__Set_Budget_Maker_Detail__c = true,
                                          ULiT_Negoptim__Set_Budget_Maker__c = true,
                                          ULiT_Negoptim__Set_NetPrice_Statement__c = true,
                                          ULiT_Negoptim__Set_PriceList_Statement__c = true);
        legal.RecordTypeId = buRTIds.get('Legal');
        if (doInsert) insert legal;
        System.assert(true);
        return legal;
    }
    public class UploadedFileWrapper {
        public String name;
        public Id documentId;
        public Id contentVersionId;
    }
}