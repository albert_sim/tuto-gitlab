/**
 * @author ULiT
 * @date 21 September 2021
 * @description update the value of SPITM_Is_Include_NIP__c and SPITM_Is_Include_Promo_Condition__c for contracts
*/
public class UpdateContractPromoConditionFieldsBatch implements Database.Batchable<AggregateResult> {
    
    private ULiT_Negoptim.NegoptimBatch nb;
    private String query;
    
    public UpdateContractPromoConditionFieldsBatch(String startedFrom, List<Id> contractIdsList) {
        this.nb = new ULiT_Negoptim.NegoptimBatch(UpdateContractPromoConditionFieldsBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateless, startedFrom);
                
        query = 'SELECT COUNT(Id), ULiT_Negoptim__Contract__c, ULiT_Negoptim__Condition_Type__r.Name ULiT_Negoptim__Condition_Type__c';
        query += ' FROM ULiT_Negoptim__Contract_Discount__c';
        query += ' WHERE ULiT_Negoptim__Condition_Type__r.Name IN (\'NIP\', \'RP0\', \'EN0\')';
        query += ' AND ULiT_Negoptim__Contract__r.RecordType.DeveloperName IN (\'Signed\', \'To_Be_Signed\')';
        if (contractIdsList != null && !contractIdsList.isEmpty()) {
            query += ' AND ULiT_Negoptim__Contract__c IN  (\'' + String.join(contractIdsList, '\',\'') + '\')';
        }
        query += ' GROUP BY ULiT_Negoptim__Contract__c, ULiT_Negoptim__Condition_Type__r.Name';
        
        this.nb.logParameter('contractIdsList', contractIdsList);
        this.nb.logParameter('query', this.query);       
    }
    
    public Iterable<AggregateResult> start(Database.BatchableContext bc) {
        return new ULiT_Negoptim.AggregateResultIterable(this.query);
    }
    
    public void execute(Database.BatchableContext bc, List<AggregateResult> scope) {
        System.Savepoint sp = Database.setSavepoint();
        try {
            Set<Id> containsNipConditionContracts = new Set<Id>();
            Set<Id> containsPromoConditionContracts = new Set<Id>();
            for (AggregateResult result : scope) {
                Id contractId = Id.valueOf(String.valueOf(result.get('ULiT_Negoptim__Contract__c')));
                String tarrifCondition = String.valueOf(result.get('ULiT_Negoptim__Condition_Type__c'));
                // if the contract has NIP
                if (tarrifCondition == 'NIP') {
                    containsNipConditionContracts.add(contractId);
                }
                // if the contract has RP0 or EN0
                if (tarrifCondition == 'RP0' || tarrifCondition == 'EN0') {
                    containsPromoConditionContracts.add(contractId);
                }
            }
            
            List<ULiT_Negoptim__Contract__c> contracts = [SELECT Id, SPITM_Is_Include_NIP__c, SPITM_Is_Include_Promo_Condition__c
                                                          FROM ULiT_Negoptim__Contract__c
                                                          WHERE Id IN :containsNipConditionContracts
                                                          OR Id IN :containsPromoConditionContracts];
            for (ULiT_Negoptim__Contract__c item : contracts) {
                // if the contract id is in the set of contract that contains a new NIP condtion, then set SPITM_Is_Include_NIP__c to true
                item.SPITM_Is_Include_NIP__c = containsNipConditionContracts.contains(item.Id);
                // if the contract id is in the set of contract that contains a new RP0 or EN0 condtion, then set SPITM_Is_Include_Promo_Condition__c to true
                item.SPITM_Is_Include_Promo_Condition__c = containsPromoConditionContracts.contains(item.Id);
            }
            // update the contracts            
            List<Database.SaveResult> results = Database.update(contracts, false);
            this.nb.logResults(results, contracts);
            
        } catch (Exception ex) {
            this.nb.logError(ex);
            Database.rollback(sp);
        }
        this.nb.saveLog(bc);
    }
    
    
    public void finish(Database.BatchableContext bc) {
        this.nb.saveLog(bc);
    }
}