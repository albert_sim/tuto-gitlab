/**
 * @author ULiT
 * @date 16 September 2021
 * @description executes batches, used in screen flows
*/
global class ContractSignatoryBatchCaller {
        
    @InvocableMethod(label = 'ExecuteBatch')
    global static void execute() {
        ULiT_Negoptim.NegoptimBatch.executeBatch(new UpdateContractSignatoriesBatch('Dev Console', null), 10);
    }
    
}