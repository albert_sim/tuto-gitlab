@isTest
private class SAVCmpControllerTest{
	static testMethod void test_Coverage(){
		String currencyIsoCode = [SELECT IsoCode
		                          FROM CurrencyType
		                          WHERE IsCorporate = true
		                          LIMIT 1].IsoCode;
		ULiT_Negoptim__Orga_BU__c country = new ULiT_Negoptim__Orga_BU__c(Name = 'C10');
		country.ULiT_Negoptim__BU_Code__c = 'C10';
		country.ULiT_Negoptim__ISO_Country_Code__c = 'C10';
		country.ULiT_Negoptim__Country__c = 'C10';
		country.ULiT_Negoptim__Status__c = 'Open';
		country.ULiT_Negoptim__BU_ODate__c = Date.newInstance(Date.today().year(), 1, 1);
		country.ULiT_Negoptim__Set_Assortment_Budget_BU_Source__c = true;
		country.ULiT_Negoptim__Set_Assortment_Budget_BU_Target__c = true;
		country.ULiT_Negoptim__Set_Assortment_Nego_BU_Source__c = true;
		country.ULiT_Negoptim__Set_Assortment_Nego_BU_Target__c = true;
		country.ULiT_Negoptim__Set_Assortment_Target_BU_Source__c = true;
		country.ULiT_Negoptim__Set_Assortment_Target_BU_Target__c = true;
		country.ULiT_Negoptim__Set_BU_List_Entity__c = true;
		country.ULiT_Negoptim__Set_Com_Condition_Counterparty__c = true;
		country.ULiT_Negoptim__Set_Com_Condition_Master_BU__c = true;
		country.ULiT_Negoptim__Set_Contract_BU_Source__c = true;
		country.ULiT_Negoptim__Set_Contract_BU_Target__c = true;
		country.ULiT_Negoptim__Set_Contract_Condition_BU_Scope__c = true;
		country.ULiT_Negoptim__Set_Contract_Discount_Main_Term__c = true;
		country.ULiT_Negoptim__Set_Contract_Discount_Sub_Term__c = true;
		country.ULiT_Negoptim__Set_Invoice_BUDispatch_Detail__c = true;
		country.ULiT_Negoptim__Set_Invoice_Detail__c = true;
		country.ULiT_Negoptim__Set_Invoice_Payment__c = true;
		country.ULiT_Negoptim__Set_Invoice__c = true;
		country.ULiT_Negoptim__Set_Product_Listing__c = true;
		country.ULiT_Negoptim__Set_SF_Data_Collection__c = true;
		country.ULiT_Negoptim__Set_SF_Planning__c = true;
		country.ULiT_Negoptim__Set_Budget_Maker_Detail__c = true;
		country.ULiT_Negoptim__Set_Budget_Maker__c = true;
		country.ULiT_Negoptim__Set_NetPrice_Statement__c = true;
		country.ULiT_Negoptim__Set_PriceList_Statement__c = true;
		country.CurrencyIsoCode = currencyIsoCode;
		country.RecordTypeId = [SELECT Id, DeveloperName
		                        FROM RecordType
		                        WHERE DeveloperName = 'Country' AND SobjectType = 'ULiT_Negoptim__Orga_BU__c' AND IsActive = true
		                        LIMIT 1].Id;
		insert country;
		ULiT_Negoptim__Sup_Supplier__c supplier = new ULiT_Negoptim__Sup_Supplier__c(Name = 'SUP 1');
		supplier.ULiT_Negoptim__Code__c = 'SUP 1';
		supplier.ULiT_Negoptim__Country_origin__c = country.Id;
		supplier.ULiT_Negoptim__Acc_Country__c = country.Name;
		supplier.ULiT_Negoptim__Is_Default_NegoScope__c = true;
		supplier.ULiT_Negoptim__Acc_Address_External_Synchro__c = false;
		supplier.ULiT_Negoptim__Admin_Address_External_Synchro__c = false;
		supplier.ULiT_Negoptim__Status__c = 'Active';
		supplier.CurrencyIsoCode = currencyIsoCode;
		insert supplier;
		ULiT_Negoptim__Sup_sup_NegoScope__c NS = [SELECT Id
		                                          FROM ULiT_Negoptim__Sup_sup_NegoScope__c
		                                          WHERE ULiT_Negoptim__Supplier__c = :supplier.Id
		                                          LIMIT 1];

		ULiT_Negoptim__Contract__c contract = new ULiT_Negoptim__Contract__c(Name = supplier.Name + ' - Contract 2020');
		contract.ULiT_Negoptim__Supplier__c = supplier.Id;
		contract.ULiT_Negoptim__Supplier_Nego_Scope__c = NS.Id;
		contract.ULiT_Negoptim__Contract_BU__c = country.Id;
		contract.ULiT_Negoptim__BU_Source__c = country.Id;
		contract.ULiT_Negoptim__Contract_BDate__c = Date.newInstance(Date.today().year(), 1, 1);
		contract.ULiT_Negoptim__Contract_EDate__c = Date.newInstance(Date.today().year(), 12, 31);
		contract.ULiT_Negoptim__Duration__c = contract.ULiT_Negoptim__Contract_BDate__c.monthsBetween(contract.ULiT_Negoptim__Contract_EDate__c)+1;
		contract.ULiT_Negoptim__Contract_Commercial_BDate__c = Date.newInstance(Date.today().year(), 1, 1);
		contract.ULiT_Negoptim__Contract_Commercial_EDate__c = Date.newInstance(Date.today().year(), 12, 31);
		contract.ULiT_Negoptim__Status__c = 'Signed';
		contract.ULiT_Negoptim__Ext_id__c = '10';
		contract.ULiT_Negoptim__D_N__c = 'N';
		contract.ULiT_Negoptim__Duration_type__c = 'Month';
		contract.ULiT_Negoptim__Contract_Numbder__c = 'c10';
		insert contract;
		SAVCmpController.ResultWrapper resultwrapper = (SAVCmpController.ResultWrapper)JSON.deserialize(SAVCmpController.getData(contract.Id), SAVCmpController.ResultWrapper.class);
		SAVCmpController.saveContract(contract.Id, true);
		SAVCmpController.saveAfterSaleAndContacts(JSON.serialize(resultwrapper.afterSalesMap), JSON.serialize(resultwrapper.contactList));
	}
}