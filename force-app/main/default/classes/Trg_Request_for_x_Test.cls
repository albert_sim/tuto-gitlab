/**
 * unit test for Request For X.
 */
@isTest
public class Trg_Request_for_x_Test {
    private static Integer year;
    private static Id rootId;
    private static String currencyIsoCode;
    private static Map<String, Id> buRTIds;
    private static ULiT_Negoptim__Country_List__c myCountry;
    private static ULiT_Negoptim__Orga_BU__c region;
    private static ULiT_Negoptim__Orga_BU__c country;
    private static ULiT_Negoptim__Orga_BU__c groupBU;
    private static ULiT_Negoptim__Orga_BU__c legal;
    private static ULiT_Negoptim__Sup_Supplier__c supplier;
    private static ULiT_Negoptim__Sup_sup_NegoScope__c NS;
    private static ULiT_Negoptim__Sup_sup_NegoScope__c negoScope;
    private static ULiT_Negoptim__Orga_HE__c department;
    private static ULiT_Negoptim__Orga_HE__c section;
    private static ULiT_Negoptim__Sup_Brand__c brand;
    private static Product2 product;
    private static ULiT_Negoptim__Supplier_PG_Mapping__c pg;
    private static ULiT_Negoptim__Contract__c contract;
    private static ULiT_Negoptim__Sell_in_Flow__c sellIn;
    private static ULiT_Negoptim__Commercial_Plan__c commercialPlan;
    private static ULiT_Negoptim__Pol_Com_Condition__c tariffCondition;
    private static ULiT_Negoptim__Commercial_Plan__c event;
    
    static void init() {
        try {
            year = System.Today().year();
            // Root Element Id
            //rootId = ULiT_Negoptim.NegoptimHelper.getRootId();
            // Get the Corporate Currency.
            /*currencyIsoCode = ULiT_Negoptim.NegoptimHelper.getCorporateCurrency();
            // Get all recortd type ids for Orga_BU__c SObject
            buRTIds = ULiT_Negoptim.NegoptimHelper.getObjectRecordTypeMapIds(Orga_BU__c.SObjectType);
            // Add Country
            myCountry = ULiT_Negoptim.TestDataFactory.createCountry(true, 'FRANCE', 'FR');
            // Create Region
            region = ULiT_Negoptim.TestDataFactory.createBURegion(true, 'MEA'); 
            // Create Country
            country = ULiT_Negoptim.TestDataFactory.createBUCountry(true, myCountry, region.Id);
            // Create Supplier
            supplier = ULiT_Negoptim.TestDataFactory.createSupplier(false, country, true); 
            insert supplier;        
            country.ULiT_Negoptim__Related_Client__c = supplier.Id;
            //country.ULiT_Negoptim__Status__c = 'Open';
            update country;
            // Get the default negoscope created on supplier creation
            NS = ULiT_Negoptim.TestDataFactory.getNSs(supplier.Id)[0];
            // Create group BU.
            groupBU = ULiT_Negoptim.TestDataFactory.createGroupBU(true,'EMC');*/
        } catch (Exception ex) {
            System.debug('++++++++++++++++++++++++++++++'); 
            System.debug(ex.getMessage() ); 
            System.debug(ex.getStackTraceString() );
            System.debug('++++++++++++++++++++++++++++++');
        }
    }
}