/*
 * author : ULit
 * @date 07 october 2021
 * */
@isTest
public class GetBaseURLInvocable_Test {
	@isTest
    static void test_case() {
        String baseUrl = GetBaseURLInvocable.getBaseURLActionInvocable().get(0);
        System.assertEquals(URL.getOrgDomainUrl().toExternalForm(), baseUrl);
    }
}