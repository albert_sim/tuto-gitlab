/**
 * @author ULIT
 * */
public class DeleteContractDiscountBatch implements Database.Batchable<SObject> {

    private ULit_Negoptim.NegoptimBatch nb;
    public final String query;
    
    public DeleteContractDiscountBatch(String startedFrom, Set<Id> contractDiscountIds) {
        this.nb = new ULit_Negoptim.NegoptimBatch(DeleteContractDiscountBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateless, startedFrom);
        String q = 'SELECT Id, Name FROM ULiT_Negoptim__Contract_Discount__c';
        if (contractDiscountIds != null && !contractDiscountIds.isEmpty()) {
            q += ' WHERE Id IN (\'' + String.join(new List<Id>(contractDiscountIds), '\',\'') +'\')';
        }
        this.query = q;
        nb.logParameter('ContractDiscountIds', contractDiscountIds);
        nb.logParameter('query', this.query);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    // Execute method implementation.
    public void execute(Database.BatchableContext BC, List<ULiT_Negoptim__Contract_Discount__c> scope) {
        // Delete Contract Discounts
        List<Database.DeleteResult> results = Database.delete(scope, false);
        for (Integer i = 0; i < results.size(); i++) {
            Database.DeleteResult result = results.get(i);
            if (!result.isSuccess()) {
                String errorMessage = 'Contract Discount: ' + '\n';
                Database.Error[] errors = result.getErrors();
                for (Database.Error err : errors) {
                    errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                }
                nb.logError(errorMessage);
            }
        }
        nb.saveLog(bc);
    }

    public void finish(Database.BatchableContext BC) {
        nb.sendEmail(bc, null, 'DeleteContractDiscountBatch');
    }
}