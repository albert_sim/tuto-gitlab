public class Trg_ULiT_NetPrice_Statement_Handler {
   
    public static void onAfterUpdate(List<ULiT_Negoptim__NetPrice_Statement__c> newNetPriceStatements, Map<Id, ULiT_Negoptim__NetPrice_Statement__c> oldNetPriceStatementsMap) {
        synchroniseAssortmentDetailsToNetPriceStatementDetails(newNetPriceStatements, oldNetPriceStatementsMap);
    }
    
    private static void synchroniseAssortmentDetailsToNetPriceStatementDetails(List<ULiT_Negoptim__NetPrice_Statement__c> newNetPriceStatements, Map<Id, ULiT_Negoptim__NetPrice_Statement__c> oldNetPriceStatementsMap) {
        Map<Id, Id> assortmentBUToNetPriceStatementIdsMap = new Map<Id, Id>();
        Map<Id, ULiT_Negoptim__NetPrice_Statement__c> newNetPriceStatementsMap = new Map<Id, ULiT_Negoptim__NetPrice_Statement__c>(newNetPriceStatements);
        String[] NPSDClientStatus = new String[] {
            'Rejected', 'Demand for explanation', null
        };
        List<ULiT_Negoptim__NetPrice_Statement_Detail__c> netPriceStatementDetailsToUpdate = new List<ULiT_Negoptim__NetPrice_Statement_Detail__c>();
        // loop over newNetPriceStatements and find NetPrice_Statement__c with Status__c modified to 'Validated' or 'Rejected'
        for (ULiT_Negoptim__NetPrice_Statement__c item : newNetPriceStatements) {
            if (oldNetPriceStatementsMap.containsKey(item.Id) && oldNetPriceStatementsMap.get(item.Id).ULiT_Negoptim__Status__c != item.ULiT_Negoptim__Status__c
                && (item.ULiT_Negoptim__Status__c == 'Validated' || item.ULiT_Negoptim__Status__c == 'Rejected'))
            {
                assortmentBUToNetPriceStatementIdsMap.put(item.ULiT_Negoptim__Assortment_BU__c, item.Id);
            }
        }
        
        Map<String, ULiT_Negoptim__NetPrice_Statement_Detail__c> netPriceStatementMap = new Map<String, ULiT_Negoptim__NetPrice_Statement_Detail__c>();
        for (ULiT_Negoptim__NetPrice_Statement_Detail__c npsd : [SELECT Id, ULiT_Negoptim__Product__c, ULiT_Negoptim__Partner_Comment__c, ULiT_Negoptim__NetPrice_Statement__r.ULiT_Negoptim__Contract__c, ULiT_Negoptim__Promo_Event_Detail__c
                                                                 FROM ULiT_Negoptim__NetPrice_Statement_Detail__c
                                                                 WHERE ULiT_Negoptim__NetPrice_Statement__c IN :assortmentBUToNetPriceStatementIdsMap.values()])
        {
            String key = String.valueOf(npsd.ULiT_Negoptim__Product__c) + String.valueOf(npsd.ULiT_Negoptim__NetPrice_Statement__r.ULiT_Negoptim__Contract__c) + String.valueOf(npsd.ULiT_Negoptim__Promo_Event_Detail__c);
            netPriceStatementMap.put(key, npsd);
        }
        Map<Id, ULiT_Negoptim__Assortment_Detail__c> netPriceStatementDetailToAssortmentDetailIdsMap = new Map<Id, ULiT_Negoptim__Assortment_Detail__c>();
        
        for (ULiT_Negoptim__Assortment_Detail__c asd : [SELECT Id, ULiT_Negoptim__Product__c, ULiT_Negoptim__Promo_Event_Detail__c, ULiT_Negoptim__Status__c,
                                                        ULiT_Negoptim__Contract_Nego_NetPrice_Gap_Comment__c, ULiT_Negoptim__Assortment_BU__c, ULiT_Negoptim__Assortment_BU__r.ULiT_Negoptim__Contract__c
                                                        FROM ULiT_Negoptim__Assortment_Detail__c
                                                        WHERE ULiT_Negoptim__Assortment_BU__c IN :assortmentBUToNetPriceStatementIdsMap.keySet()
                                                        AND ULiT_Negoptim__Version__c = NULL])
        {
            String key = String.valueOf(asd.ULiT_Negoptim__Product__c) + String.valueOf(asd.ULiT_Negoptim__Assortment_BU__r.ULiT_Negoptim__Contract__c) + String.valueOf(asd.ULiT_Negoptim__Promo_Event_Detail__c);
            if (netPriceStatementMap.containsKey(key)) {
                netPriceStatementDetailToAssortmentDetailIdsMap.put(netPriceStatementMap.get(key).Id, asd);
                netPriceStatementMap.get(key).ULiT_Negoptim__Partner_Comment__c = asd.ULiT_Negoptim__Contract_Nego_NetPrice_Gap_Comment__c;
                netPriceStatementMap.get(key).ULiT_Negoptim__Client_Status__c = asd.ULiT_Negoptim__Status__c == 'Validated' ? 'Approved' :
                NPSDClientStatus.contains(asd.ULiT_Negoptim__Status__c) ? asd.ULiT_Negoptim__Status__c : null;
                netPriceStatementDetailsToUpdate.add(netPriceStatementMap.get(key));
            }
        }
        
        List<Database.SaveResult> results = Database.update(netPriceStatementDetailsToUpdate, false);
        
        for (Database.SaveResult result : results) {
            String msg = '';
            if (!result.isSuccess()) {
                for (Database.Error err : result.getErrors()) {
                    msg += err.getMessage() + '\n';
                }
                ULiT_Negoptim__NetPrice_Statement_Detail__c detail = netPriceStatementDetailsToUpdate.get(results.indexOf(result));
                newNetPriceStatementsMap?.get(assortmentBUToNetPriceStatementIdsMap?.get(netPriceStatementDetailToAssortmentDetailIdsMap?.get(detail.Id)?.ULiT_Negoptim__Assortment_BU__c))?.addError(msg);
            }
        }
    }
    
}