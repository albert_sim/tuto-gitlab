public with sharing class MassUpdateDeleteLog {
    private String batchName = 'Undefined';
    private AsyncApexJob apexJob;
    private String startedFrom;
    private ULit_Negoptim__Log__c log;
    private ULiT_Negoptim__Batch_Settings__mdt batchMetadata;
    private BatchType bt;
    private String traceLog;
    private Boolean IsNotificationSent;
    private List<String> errors;
    /**
     * @description A map to keep track of relevent information and input data helpful in debug. Example: the query
	 */
    private Map<String, Object> parameters;
    
    /**
    * @description constructor
    * @param batchName: the batch that create this log , startedFrom: the class that generate the batch
    * @return ULiT_Negoptim__Log__c: the new instance 
    */
    public MassUpdateDeleteLog(String batchName, String startedFrom){
        this.batchName = batchName;
        this.startedFrom = startedFrom;
        IsNotificationSent = false;
        this.errors = new List<String>(); 
        this.parameters = new Map<String, Object>();
        this.bt = BatchType.Stateful;
        // Map<Id, Map<String, Batch_Settings__mdt>> batchSettingsMap = loadBatchSettings(new List<String> {batchName}, new List<Id> {UserInfo.getProfileId()});
        // if (batchSettingsMap.containsKey(UserInfo.getProfileId()) && batchSettingsMap.get(UserInfo.getProfileId()).containsKey(batchName)) {
        //     batchMetadata = batchSettingsMap.get(UserInfo.getProfileId()).get(batchName);
        // } else {
        //     batchMetadata = new Batch_Settings__mdt(Active__c = true, Allow_Sending_Email__c = true, Allow_Sending_Email_Technical__c = false, Allow_Sending_Notification__c = true);
        // }
    }
    
    /**
     * @param parameterName
     * @param value
     * @return void
     * @description adds a new parameter to the map of parameters to be logged
	***************************************************************************************************************************************** */
    public void logParameter(String parameterName, Object value) {
        system.debug('inside log params ');
        this.parameters.put(parameterName, value);
    }
    
    public MassUpdateDeleteLog.BatchType getBatchType() {
        return this.bt;
    }
    /**
    * @description create new log instance
    * @param 
    * @return ULiT_Negoptim__Log__c: the new instance 
    */
    public ULiT_Negoptim__Log__c createLog() {
        ULiT_Negoptim__Log__c log = new ULiT_Negoptim__Log__c(
            ULiT_Negoptim__Class_Name__c = this.batchName,
            ULiT_Negoptim__Started_From__c = this.startedFrom,
            ULiT_Negoptim__Apex_Job_ID__c = this.apexJob.Id,
            ULiT_Negoptim__JobItemsProcessed__c = this.apexJob.JobItemsProcessed,
            ULiT_Negoptim__CompletedDate__c = this.apexJob.CompletedDate,
            ULiT_Negoptim__NumberOfErrors__c = this.apexJob.NumberOfErrors,
            ULiT_Negoptim__JobType__c = 'BatchApex',
            ULiT_Negoptim__MethodName__c = this.apexJob.MethodName,
            ULiT_Negoptim__NumberOfWarnings__c = this.getNumberOfErrors(),
            ULiT_Negoptim__Status__c = this.apexJob.Status,
            ULiT_Negoptim__Submitted_Date__c = this.apexJob.CreatedDate,
            ULiT_Negoptim__TotalJobItems__c = this.apexJob.TotalJobItems,
            ULiT_Negoptim__status_detail__c = this.apexJob.extendedStatus,
            ULiT_Negoptim__TraceLog__c = this.generateTraceLog(true)
        );
        return log;
    }
    public ULiT_Negoptim__Log__c saveLog(Database.BatchableContext bc) {
        this.apexJob = getAsyncApexJob(bc);
        /*List<Log__c> logList = [SELECT NumberOfWarnings__c, TraceLog__c
        FROM Log__c
        WHERE Apex_Job_ID__c = :this.apexJob.Id];
        if (!logList.isEmpty()) {
        this.log = logList[0];
        }
        //    this.log = NegoptimLog.mapToLog(this);
        try {
        // check security on upsert Log__c
        String[] logFields = new String[] {
        SObjectType.Log__c.fields.Apex_Job_ID__c.Name,
        SObjectType.Log__c.fields.Class_Name__c.Name,
        SObjectType.Log__c.fields.Started_From__c.Name,
        SObjectType.Log__c.fields.JobItemsProcessed__c.Name,
        SObjectType.Log__c.fields.CompletedDate__c.Name,
        SObjectType.Log__c.fields.NumberOfErrors__c.Name,
        SObjectType.Log__c.fields.JobType__c.Name,
        SObjectType.Log__c.fields.MethodName__c.Name,
        SObjectType.Log__c.fields.NumberOfWarnings__c.Name,
        SObjectType.Log__c.fields.Status__c.Name,
        SObjectType.Log__c.fields.Submitted_Date__c.Name,
        SObjectType.Log__c.fields.TotalJobItems__c.Name,
        SObjectType.Log__c.fields.status_detail__c.Name,
        SObjectType.Log__c.fields.TraceLog__c.Name
        };
        if (NegoptimHelper.checkCreatibility(Log__c.SObjectType, logFields) &&
        NegoptimHelper.checkUpdatibility(Log__c.SObjectType, logFields))
        {
        Database.UpsertResult result = Database.upsert(log, false);
        }*/
        // send notification 
        Boolean isFinish = (apexJob.Status.equals('Completed') ||apexJob.Status.equals('Failed') || apexJob.Status.equals('Aborted'));
        if(isFinish) this.log = this.createLog();
        database.insert(this.log);

        
        if(/*batchMetadata.Allow_Sending_Notification__c &&*/ isFinish && !IsNotificationSent)
            this.sendNotification(bc, this.log.Id, 
                                  this.log.ULiT_Negoptim__Class_Name__c + ' - ' + this.log.ULiT_Negoptim__Status__c,
                                  'The batch Apex job processed ' + this.log.ULiT_Negoptim__TotalJobItems__c +  ' batches with ' +  this.log.ULiT_Negoptim__NumberOfErrors__c + ' failures \n' +
                                  'Warning Messages (' + this.log.ULiT_Negoptim__NumberOfWarnings__c + ' messages)');
                                  
        
/*  } catch (DMLException ex) {
        System.debug('Exception upserting log' + ex + '- cause: ' + ex.getCause());
    }*/
        return this.log;
    }
    public AsyncApexJob getAsyncApexJob(Database.BatchableContext bc) {
        String[] fields = new String[] {'Status', 'NumberOfErrors', 'TotalJobItems', 'JobItemsProcessed',
            'CompletedDate', 'MethodName', 'CreatedBy.name', 'CreatedDate', 'ExtendedStatus'};
                //	if (NegoptimHelper.checkAccessibility(Schema.getGlobalDescribe().get('AsyncApexJob'), fields)) {
                if (this.apexJob == null) {
                    this.apexJob = [SELECT Status, NumberOfErrors, TotalJobItems, JobItemsProcessed,
                                    CompletedDate, MethodName,	CreatedBy.Name, CreatedDate, ExtendedStatus
                                    FROM AsyncApexJob WHERE Id = :bc.getJobId()];
                }
                system.debug('apexJob' +JSON.serialize(this.apexJob));
        return this.apexJob;
    }
    
    // public static Map<Id, Map<String, Batch_Settings__mdt>> loadBatchSettings(List<String> batchNameList, List<Id> profileIdList) {
    //     Map<Id, Map<String, Batch_Settings__mdt>> profileIdToBatchSettingsMap = new Map<Id, Map<String, Batch_Settings__mdt>>();
    //     String query = 'SELECT Id, DeveloperName, Profile_Id__c, Active__c, Allow_Sending_Email__c, Allow_Sending_Email_Technical__c, Sender__c, MasterLabel, Allow_Sending_Notification__c';
    //     query += ' FROM Batch_Settings__mdt';
    //     List<String> conditions = new List<String>();
    //     List<String> batchNameList_escaped = new List<String>();
    //     for (String str : batchNameList) {
    //         batchNameList_escaped.add(String.escapeSingleQuotes(str));
    //     }
    //     if (batchNameList != null) {
    //         conditions.add('MasterLabel In (\'' + String.join(batchNameList_escaped, '\',\'') + '\')');
    //     }
    //     if (profileIdList != null) {
    //         conditions.add('Profile_Id__c IN (\'' + String.join(profileIdList, '\',\'') + '\')');
    //     }
    //     if (!conditions.isEmpty()) {
    //         query += ' WHERE ' + String.join(conditions, ' AND ');
    //     }
    //     for (Batch_Settings__mdt item : Database.query(query)) {
    //         if (!profileIdToBatchSettingsMap.containsKey(item.Profile_Id__c)) {
    //             profileIdToBatchSettingsMap.put(item.Profile_Id__c, new Map<String, Batch_Settings__mdt>());
    //         }
    //         profileIdToBatchSettingsMap.get(item.Profile_Id__c).put(item.MasterLabel, item);
    //     }
    //     return profileIdToBatchSettingsMap;
    // }

    public void sendNotification(Database.BatchableContext bc, Id objectId, String title, String body) {
        Messaging.CustomNotification notification = new Messaging.CustomNotification();
        Id userId = Userinfo.getUserId();
        notification.setBody(body);
        notification.setTitle(title);
        notification.setSenderId(userId);
        CustomNotificationType type = [SELECT Id FROM CustomNotificationType WHERE DeveloperName = 'Negoptim_Custom_Notification'];
        if(type != null) {
            notification.setNotificationTypeId(type.Id);
            notification.setTargetId(objectId); // target object id
            notification.send(new Set<String> { userId });
            IsNotificationSent = true;
        }
    }
    
    public String getWarningTraceLog() {
        String LineSeparator = '<hr/>';
        String newLine = '<br/>';
        String traceLog = '';
        if(!errors.isEmpty()) {
            traceLog += 'Warning messages:' + newLine;
            traceLog += '<ol>';
            for(String error : this.errors) {
                traceLog += '<li>' + error + '</li>';
            }
            traceLog += '</ol>';
            traceLog += LineSeparator;
        }
        return traceLog;
    }
    
    
    private void sendEmail(Messaging.SingleEmailMessage mail) {
        Integer used = Limits.getEmailInvocations();
        Integer emailLimit = Limits.getLimitEmailInvocations();
        if(used >= emailLimit) {
            System.debug('Email limit was exceeded.');
        } else {
            try {
                Messaging.sendEmail(new Messaging.SingleEmailMessage[] {mail});
            } catch(System.EmailException ex) {
                System.debug('Caught Email Exception: ' + ex);
            }
        }
    } 
    
    public void sendEmail(Database.BatchableContext bc, String customHtmlBody, String customSubject) {
        saveLog(bc);
        //String initialCustomHtmlBody = customHtmlBody;
        //String initialCustomSubject = customSubject;
        // if (this.batchMetadata.ULiT_Negoptim__Allow_Sending_Email__c || this.batchMetadata.ULiT_Negoptim__Allow_Sending_Email_Technical__c) {
        //     List<OrgWideEmailAddress> owea = new List<OrgWideEmailAddress>();
        //     if (batchMetadata.ULiT_Negoptim__Sender__c != null) {
        //         // fetch Org Wide Email Address
        //         owea = [SELECT Id, DisplayName FROM OrgWideEmailAddress
        //                WHERE Address = :batchMetadata.ULiT_Negoptim__Sender__c
        //                AND IsAllowAllProfiles = TRUE];
        //     }
        //     if (this.batchMetadata.ULiT_Negoptim__Allow_Sending_Email__c) {
        //         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //         // Set Sender
        //         if (!owea.isEmpty()) {
        //             mail.setOrgWideEmailAddressId(owea.get(0).Id);
        //         } else {
        //             mail.setSenderDisplayName('Negoptim By ULiT');
        //         }
        //         mail.setReplyTo('no-reply@negoptim.com');
        //         mail.setToAddresses(new String[] {UserInfo.getUserId()});
        //         if (customHtmlBody == null) {
        //             customHtmlBody = this.generateTraceLog(false);
        //         }
        //         if (customSubject == null) {
        //             customSubject = this.batchName + ' Batch Summary';
        //         }
        //         mail.setHtmlBody(customHtmlBody);
        //         mail.setSubject(customSubject);
        //         sendEmail(mail);
        //     }
        //     customHtmlBody = initialCustomHtmlBody;
        //     customSubject = initialCustomSubject;
        //     if (this.batchMetadata.ULiT_Negoptim__Allow_Sending_Email_Technical__c) {
        //         Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        //         // Set Sender
        //         if (!owea.isEmpty()) {
        //             mail.setOrgWideEmailAddressId(owea.get(0).Id);
        //         } else {
        //             mail.setSenderDisplayName('Negoptim By ULiT');
        //         }
        //         mail.setReplyTo('no-reply@negoptim.com');
        //         mail.setToAddresses(new String[] {UserInfo.getUserId()});
        //         if (customHtmlBody == null) {
        //             customHtmlBody = this.generateTraceLog(true);
        //         }
        //         if (customSubject == null) {
        //             customSubject = this.batchName + ' Batch Summary';
        //         }
        //         mail.setHtmlBody(customHtmlBody);
        //         mail.setSubject(customSubject);
        //         sendEmail(mail);
        //     }
        // }
    }
    
    public String generateTraceLog(Boolean isTechnical) {
        String traceLog = generateTraceLogHeader(isTechnical);//updateHeaderTraceLog(getHeaderTraceLog(), getNumberOfErrors());
        if (isTechnical) traceLog += getWarningTraceLog();
        return traceLog;
    }
    /**
    * @return trace log header
    * @description generate log header
    ***************************************************************************************************************************************** */
    public String generateTraceLogHeader(Boolean isTechnical) {
        String LineSeparator = '<hr/>';
        String newLine = '<br/>';
        String traceLog = lineSeparator;
        traceLog += '<b>Batch Name: </b>' + batchName + newLine;
        traceLog += '<b>Started From: </b>' + startedFrom + newLine;
        traceLog += '<b>Source: </b>' + UserInfo.getOrganizationName() + newLine;
        traceLog += '<b>Result: </b>';
        //display results in colors depending on the job status
        Integer nbOfErrors = this.log == null ? this.getNumberOfErrors() : (Integer) this.log.ULiT_Negoptim__NumberOfWarnings__c;
        traceLog += '<span style="font-weight: bold;color: ';
        if(this.apexJob.NumberOfErrors == 0 && nbOfErrors == 0) {
            traceLog += 'green;">Successful';
        } else if(this.apexJob.NumberOfErrors > 0) {
            traceLog += 'red;">Failed';
        } else if(nbOfErrors > 0) {
            traceLog += 'orange;">Warning';
        }
        traceLog += '</span>';
        traceLog += lineSeparator;
        if (isTechnical) {
            //Insert Parameters
            if(parameters != null && !parameters.isEmpty()) {
                traceLog += '<u>Log Summary</u>' + newLine;
                traceLog += '<ul>';
                for(String key : parameters.keySet()) {
                    String param = '';
                    if(parameters.get(key) != null)
                        param = String.valueOf(parameters.get(key)).escapeHtml4();
                    traceLog += '<li>' + '<b>' + key + '</b>' +  ': ' +  param  + '</li>';
                }
                traceLog += '</ul>';
                traceLog += lineSeparator;
            }
        }

        //Log details and warning messages
        traceLog += '<u>Trace Log</u>' + newLine;
        traceLog += 'The batch Apex job processed ' + apexJob.TotalJobItems + ' batches with '+ apexJob.NumberOfErrors + ' failures.' + newLine;
        if (isTechnical) {
            if (!String.isEmpty(this.apexJob.ExtendedStatus)) {
                traceLog += '<div style="color: red;"><b>Failure Details: ' + this.apexJob.ExtendedStatus + '</b></div>';
            }
            traceLog += 'Warning Messages (' + nbOfErrors + ' messages)' + newLine;
        }
        return traceLog;
    }
    public Integer getNumberOfErrors(){
        return this.errors.size();
    }
    public void logError(String error) {
        this.errors.add(error);
    }
    
    public enum BatchType {
        Stateless,
        Stateful
    }
}