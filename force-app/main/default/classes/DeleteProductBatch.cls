/**
 * @author ULIT
 * */
public class DeleteProductBatch implements Database.Batchable<SObject> {

    private ULit_Negoptim.NegoptimBatch nb;
    public final String query;
    
    public DeleteProductBatch(String startedFrom, Set<Id> productIds) {
        this.nb = new ULit_Negoptim.NegoptimBatch(DeleteProductBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateless, startedFrom);
        String q = 'SELECT Id FROM Product2';
        q += ' WHERE ULiT_Negoptim__Brand__r.ULiT_Negoptim__Brand_Type_2__c IN (\'Private Brand\', \'First Price\')';
        if (productIds != null && !productIds.isEmpty()) {
            q += ' AND Id IN (\'' + String.join(new List<Id>(productIds), '\',\'') +'\')';
        }
        this.query = q;
        nb.logParameter('productIds', productIds);
        nb.logParameter('query', this.query);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    // Execute method implementation.
    public void execute(Database.BatchableContext BC, List<Product2> scope) {
        // Delete Related ASD
        List<ULit_Negoptim__Assortment_Detail__c> assortmentDetailsToDelete = [SELECT Id
                                                                               FROM ULit_Negoptim__Assortment_Detail__c
                                                                               WHERE ULit_Negoptim__Product__c IN :scope];
        List<Database.DeleteResult> results = Database.delete(assortmentDetailsToDelete, false);
        for (Integer i = 0; i < results.size(); i++) {
            Database.DeleteResult result = results.get(i);
            if (!result.isSuccess()) {
                String errorMessage = 'ASD: ' + '\n';
                Database.Error[] errors = result.getErrors();
                for (Database.Error err : errors) {
                    errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                }
                nb.logError(errorMessage);
            }
        }
        
        // Delete Related PG
        List<ULit_Negoptim__Supplier_PG_Mapping__c> PGToDelete = [SELECT Id
                                                                  FROM ULit_Negoptim__Supplier_PG_Mapping__c
                                                                  WHERE ULit_Negoptim__Product__c IN :scope];
        results = Database.delete(PGToDelete, false);
        for (Integer i = 0; i < results.size(); i++) {
            Database.DeleteResult result = results.get(i);
            if (!result.isSuccess()) {
                String errorMessage = 'PG: ' + '\n';
                Database.Error[] errors = result.getErrors();
                for (Database.Error err : errors) {
                    errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                }
                nb.logError(errorMessage);
            }
        }
        
        // Delete Product
        results = Database.delete(scope, false);
        for (Integer i = 0; i < results.size(); i++) {
            Database.DeleteResult result = results.get(i);
            if (!result.isSuccess()) {
                String errorMessage = 'Product: ' + '\n';
                Database.Error[] errors = result.getErrors();
                for (Database.Error err : errors) {
                    errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                }
                nb.logError(errorMessage);
            }
        }
        nb.saveLog(bc);
    }
    
    public void finish(Database.BatchableContext BC) {
        nb.sendEmail(bc, null, 'DeleteProductBatch');
    }
}