/**
 * @author ULiT
 * @date 04-10-2021
 * @description unit test class for Trg_Contract_Discount_Move_Setup trigger
******************************************************************************************************************************************* */
@isTest
private class Trg_Contract_Discount_Move_Setup_Test {

    private static Integer randomNumber;
    private static Integer year;
    private static ULit_Negoptim__Orga_BU__c region;
    private static ULit_Negoptim__Orga_BU__c country;
    private static ULit_Negoptim__Sup_Supplier__c supplier;
    private static ULit_Negoptim__Sup_sup_NegoScope__c NS;

    static ULiT_Negoptim__Contract__c contract;
    private static ULit_Negoptim__Pol_Com_Condition__c tariffCondition1;
    private static ULit_Negoptim__Pol_Com_Condition__c tariffCondition2;
    private static ULit_Negoptim__Contract_Discount__c condition;
    private static ULiT_Negoptim__Contract_Discount_Move_Setup__c moveSetup;

    public static final Map<String, Id> contractRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Contract__c.SObjectType);
    public static final Map<String, Id> buRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Orga_BU__c.SObjectType);

    static void init() {
        year = System.Today().year();
        // Add Country
        ULit_Negoptim__Country_List__c myCountry = createCountry(true, 'FRANCE', 'FR');
        // Create Region
        region = createBURegion(true, 'EU');
        // Create Country
        country = createBUCountry(true, myCountry, region.Id);
        // Create Supplier
        supplier = createSupplier(false, country, true);
        insert supplier;
        // Get the default NegoScope created on supplier creation
        NS = getNSs(supplier.Id)[0];
        // Create Contracts
        // Create Contract - Nego Base
        contract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        contract.Name = supplier.Name + ' - Contract ' + year;
        contract.ULit_Negoptim__TO1__c = 1000;
        contract.ULit_Negoptim__Contract_Numbder__c = 'C001';
        contract.ULit_Negoptim__Contract_Type__c = 'Contract';
        contract.ULit_Negoptim__Status__c = 'In preparation';
        contract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Nego_Base_Init'));
        insert contract;
        // Create Policy - Tariff Conditions
        tariffCondition1 = createTariffCondition(false, 0, 2);
        tariffCondition1.Name = 'Source Tariff Condition';
        tariffCondition1.ULit_Negoptim__Condition_status__c = 'Open';
        tariffCondition1.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        tariffCondition1.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        tariffCondition1.ULit_Negoptim__Status_BDate__c = date.newInstance(year - 2 , 1, 1);
        tariffCondition2 = createTariffCondition(false, 0, 2);
        tariffCondition2.Name = 'Target Tariff Condition';
        tariffCondition2.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        tariffCondition2.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        tariffCondition2.ULit_Negoptim__Condition_status__c = 'Open';
        tariffCondition2.ULit_Negoptim__Status_BDate__c = date.newInstance(year - 2 , 1, 1);
        insert new List<ULit_Negoptim__Pol_Com_Condition__c> {tariffCondition1, tariffCondition2};
        // Create Condition
        condition = createCondition(false, tariffCondition1, contract, contract.ULit_Negoptim__Contract_BDate__c, contract.ULit_Negoptim__Contract_EDate__c);
        insert condition;
        // Create Contract Discount Move Setup
        moveSetup = new ULiT_Negoptim__Contract_Discount_Move_Setup__c(Name = 'Move Setup Test',
                                                                       ULiT_Negoptim__Comment__c = 'Move Comment',
                                                                       ULiT_Negoptim__Contract_Record_Type__c = 'Nego_Base_Init',
                                                                       ULiT_Negoptim__Contract_Status__c = contract.ULit_Negoptim__Status__c,
                                                                       ULiT_Negoptim__Reference_BU__c = country.Id,
                                                                       ULiT_Negoptim__Reference_NegoYear__c = year,
                                                                       ULiT_Negoptim__Status__c = 'Ready to apply',
                                                                       ULiT_Negoptim__Tariff_Condition_Source__c = tariffCondition1.Id,
                                                                       ULiT_Negoptim__Tariff_Condition_Target__c = tariffCondition2.Id);
        insert moveSetup;
    }

    testmethod static void case1_runMove() {
        Test.startTest();
        init();
        moveSetup.ULit_Negoptim__Status__c = 'Run';
        update moveSetup;
        Test.stopTest();        
        System.assertEquals('Applied', [SELECT ULit_Negoptim__Status__c FROM ULiT_Negoptim__Contract_Discount_Move_Setup__c WHERE Id = :moveSetup.Id].ULit_Negoptim__Status__c);
    }

    testmethod static void case2_cancelMove() {
        Test.startTest();
        init();
        moveSetup.ULit_Negoptim__Status__c = 'Cancel';
        update moveSetup;
        Test.stopTest();        
        System.assertEquals('Ready to apply', [SELECT ULit_Negoptim__Status__c FROM ULiT_Negoptim__Contract_Discount_Move_Setup__c WHERE Id = :moveSetup.Id].ULit_Negoptim__Status__c);
    }

    /* BUSINESS UNIT */
    // Country List creation.
    private static ULit_Negoptim__Country_List__c createCountry(Boolean doInsert, String name, String code) {
        ULit_Negoptim__Country_List__c myCountry = new ULit_Negoptim__Country_List__c(Name = name, ULit_Negoptim__Country_Code__c = code);
        if (doInsert) insert myCountry;
        System.assert(true);
        return myCountry;
    }

    // Region creation.
    private static ULit_Negoptim__Orga_BU__c createBURegion(Boolean doInsert, String name) {
        ULit_Negoptim__Orga_BU__c region = new ULit_Negoptim__Orga_BU__c(Name = name, ULit_Negoptim__BU_Code__c = name, ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Open',
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                           ULit_Negoptim__Set_BU_List_Entity__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                           ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                           ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Payment__c = true,
                                           ULit_Negoptim__Set_Invoice__c = true,
                                           ULit_Negoptim__Set_Product_Listing__c = true,
                                           ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                           ULit_Negoptim__Set_SF_Planning__c = true,
                                           ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                           ULit_Negoptim__Set_Budget_Maker__c = true,
                                           ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                           ULit_Negoptim__Set_PriceList_Statement__c = true);
        region.RecordTypeId = buRTIds.get('Region');
        if (doInsert) insert region;
        System.assert(true);
        return region;
    }

    // Country creation.
    private static ULit_Negoptim__Orga_BU__c createBUCountry(Boolean doInsert, ULit_Negoptim__Country_List__c c, Id regionId) {
        ULit_Negoptim__Orga_BU__c country = new ULit_Negoptim__Orga_BU__c(Name = c.Name, ULit_Negoptim__BU_Code__c = c.ULit_Negoptim__Country_Code__c + getRandomNumber(), ULit_Negoptim__ISO_Country_Code__c = c.ULit_Negoptim__Country_Code__c,
                                            ULit_Negoptim__Country__c = c.Name, ULit_Negoptim__Country_Zone_origin__c = regionId, ULit_Negoptim__Status__c = 'Open',
                                            ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                            ULit_Negoptim__Set_BU_List_Entity__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                            ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                            ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Payment__c = true,
                                            ULit_Negoptim__Set_Invoice__c = true,
                                            ULit_Negoptim__Set_Product_Listing__c = true,
                                            ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                            ULit_Negoptim__Set_SF_Planning__c = true,
                                            ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                            ULit_Negoptim__Set_Budget_Maker__c = true,
                                            ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                            ULit_Negoptim__Set_PriceList_Statement__c = true);
        country.RecordTypeId = buRTIds.get('Country');
        if (doInsert) insert country;
        System.assert(true);
        return country;
    }

    /* SUPPLIER + NEGOSCOPE */
    // Supplier creation.
    private static ULit_Negoptim__Sup_Supplier__c createSupplier(Boolean doInsert, ULit_Negoptim__Orga_BU__c country, Boolean withNS) {
    	String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Sup_Supplier__c supplier = new ULit_Negoptim__Sup_Supplier__c(Name = 'SUP ' + rdm, ULit_Negoptim__Code__c = rdm, ULit_Negoptim__Country_origin__c = country.Id, ULit_Negoptim__Acc_Country__c = country.Name,
        								ULit_Negoptim__Is_Default_NegoScope__c = withNS, ULit_Negoptim__Acc_Address_External_Synchro__c = false, ULit_Negoptim__Admin_Address_External_Synchro__c = false,
        								ULit_Negoptim__Status__c = 'Active');
        if (doInsert) insert supplier;
        System.assert(true);
        return supplier;
    }

    // Get the list of NS related to the supplier passed by parameter.
	private static List<ULit_Negoptim__Sup_sup_NegoScope__c> getNSs(Id SupplierId) {
		System.assert(true);
        return [SELECT Id, Name, ULit_Negoptim__Supplier__c, ULit_Negoptim__Supplier__r.Name, ULit_Negoptim__Supplier__r.ULit_Negoptim__Code__c, ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                OwnerId, ULit_Negoptim__NS_Code__c, ULit_Negoptim__NS_Code_Long__c
                FROM ULit_Negoptim__Sup_sup_NegoScope__c
                WHERE ULit_Negoptim__Supplier__c = :supplierId];

 	}

    /* CONTRACT + CONDITION */
    // Contract creation.
    private static ULit_Negoptim__Contract__c createContract(Boolean doInsert, ULit_Negoptim__Sup_sup_NegoScope__c NS, Date beginDate, Date endDate) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Contract__c contract = new ULit_Negoptim__Contract__c(Name = NS.ULit_Negoptim__Supplier__r.Name + ' - Contract ' + year, ULit_Negoptim__Supplier__c = NS.ULit_Negoptim__Supplier__c,
                                               ULit_Negoptim__Supplier_Nego_Scope__c = NS.Id, ULit_Negoptim__Contract_BU__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                                               ULit_Negoptim__BU_Source__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c, ULit_Negoptim__Contract_BDate__c = beginDate,
                                               ULit_Negoptim__Contract_EDate__c = endDate, ULit_Negoptim__Duration__c = beginDate.monthsBetween(endDate) + 1,
                                               ULit_Negoptim__Contract_Commercial_BDate__c = beginDate, ULit_Negoptim__Contract_Commercial_EDate__c = endDate,
                                               ULit_Negoptim__Contract_Type__c = 'Contract', ULit_Negoptim__Status__c = 'Signed', ULit_Negoptim__Ext_id__c = rdm,
                                               ULit_Negoptim__Duration_type__c = 'Month', ULit_Negoptim__Contract_Numbder__c = 'c' + rdm,
                                               RecordTypeId = contractRTIds.get('To_Be_Signed'));
        if (doInsert) Database.insert(contract);
		System.assert(true);
        return contract;
    }

    // Tarrif Condition creation.
    public static ULit_Negoptim__Pol_Com_Condition__c createTariffCondition(Boolean doInsert, Integer pickListValue, Integer indexValue) {
        String rdm = String.valueOf(getRandomNumber());
        // Get field describe.
        Schema.DescribeFieldResult fieldResult = ULit_Negoptim__Pol_Com_Condition__c.ULit_Negoptim__Nego_Discount_Type__c.getDescribe();
        // Get the picklist value.
        String plv = fieldResult.getPicklistValues().get(pickListValue).getValue();
        // Create an instance of Tarrif Condition.
        ULit_Negoptim__Pol_Com_Condition__c tariffCondition1 = new ULit_Negoptim__Pol_Com_Condition__c(Name = plv, ULit_Negoptim__Nego_Discount_Type__c = plv, ULit_Negoptim__Index__c = 'Index' + indexValue,
                                        ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Condition_Code__c = rdm,
                                        ULit_Negoptim__Abrev__c = 'TC', ULit_Negoptim__Gen_Name_com__c = 'TC', ULit_Negoptim__Name_Com__c = 'TC', ULit_Negoptim__Name_Fi__c = 'TC', ULit_Negoptim__Name_Leg__c = 'TC',
                                        ULit_Negoptim__Gen_Name_Fi__c = 'TC', ULit_Negoptim__Gen_Name_Leg__c = 'TC', ULit_Negoptim__VAT_Type__c = 'Rebate', ULit_Negoptim__Acc_Document_Type__c = 'Credit Note Request');
        // Do insertion.
        if (doInsert) insert tariffCondition1;
        System.assert(true);
        return tariffCondition1;
    }

    // Condition creation.
    public static ULit_Negoptim__Contract_Discount__c createCondition(Boolean doInsert, ULit_Negoptim__Pol_Com_Condition__c tariffCondition1, ULit_Negoptim__Contract__c contract, Date beginDate, Date endDate) {
        ULit_Negoptim__Contract_Discount__c condition = new ULit_Negoptim__Contract_Discount__c(ULit_Negoptim__Nego_Discount_Type__c = tariffCondition1.ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Condition_Type__c = tariffCondition1.Id,
                                                                                                ULit_Negoptim__Contract__c = contract.Id, ULit_Negoptim__Product_Scope__c = contract.ULit_Negoptim__Supplier_Nego_Scope__c, ULit_Negoptim__BU_Scope__c = contract.ULit_Negoptim__BU_Source__c,
                                                                                                ULit_Negoptim__Disc_BDate__c = beginDate, ULit_Negoptim__Disc_EDate__c = endDate);
        if (doInsert) Database.insert(condition);
        System.assert(true);
        return condition;
    }

    // Method to increment randomNumber
    private static Integer getRandomNumber() {
        if (randomNumber == null)
            randomNumber = (Integer)(Math.random()*9999);
        return randomNumber++;
    }

    /**
     * Get Object RecordType Map of Name/Id
     * @param sObjectType
     * */
    public static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();
        // Check Accessibility.
        // if(!checkAccessibilityFields(Schema.SObjectType.RecordType.fields.getMap(), new String [] {'Id', 'DeveloperName'})) {
        // 	return rtMap;
        // }
        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }
    /**
     * Get Object RecordType Id by Name
     * @param sObjectType
     * @param recordTypeName
     * @example: Id recordTypeId = NegoptimHelper.getObjectRecordTypeId(ULit_Negoptim__Orga_BU__c.SObjectType, 'Country');
     * */
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName) {
        // If sObjectType is wrong, then an Exception is thrown.
        Id rt;
        Map<Id, Schema.RecordTypeInfo> sObjectTypeRecordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosById();
        for (Id RTId : sObjectTypeRecordTypeInfo.keySet()) {
            if (sObjectTypeRecordTypeInfo.get(RTId).getDeveloperName() == recordTypeName) {
                rt = RTId;
            }
        }
        if (rt == null) {
            // If recordTypeName is wrong, then an Exception is thrown.
            throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.');
        }
        //Return the record type id
        return rt;
    }
    public class RecordTypeException extends Exception {}

}