/**
 * @author ULiT
 * @date 23-08-2021
 * @description Schedule class to schedule execution of UpdateContractStatusBatch
******************************************************************************************************************************************* */
public class UpdateContractStatusSchedule implements Schedulable {
    public UpdateContractStatusSchedule() {
    }

    public void execute(SchedulableContext sc) {
        UpdateContractStatusBatch b = new UpdateContractStatusBatch(UpdateContractStatusSchedule.class.getName());
        Database.executebatch(b);
    }

    public static void run(String expression) {
        String cronExpression = expression != null ? expression : '0 0 23 ? * * *';
        System.schedule(UpdateContractStatusSchedule.class.getName(), cronExpression, new UpdateContractStatusSchedule());
    }
}