public with sharing class templateImporterCmpController {
    public templateImporterCmpController() {

    }
    @AuraEnabled
    public static Boolean updateExistingTemplate(String data){
        TemplateFileWrapper templateToCreate = (TemplateFileWrapper) JSON.deserialize(data, TemplateFileWrapper.class);
        try {
            List<ULiT_Negoptim__NegoTemplate__c> existingTemplates = [SELECT Id, Name, ULiT_Negoptim__Template_Filter__c FROM ULiT_Negoptim__NegoTemplate__c WHERE Name LIKE :templateToCreate.template.Name];
            ULiT_Negoptim__NegoTemplate__c existingTemplate;
            if (!existingTemplates.isEmpty()) {
                existingTemplate = existingTemplates[0];
                templateToCreate.template.Id = existingTemplate.Id;
                if (existingTemplate.ULiT_Negoptim__Template_Filter__c != null) {
                    List<ULiT_Negoptim__Filter__c> filters = [SELECT Id, Name, (SELECT Id FROM ULiT_Negoptim__Filters_Criteria__r) FROM ULiT_Negoptim__Filter__c WHERE Id =: existingTemplate.ULiT_Negoptim__Template_Filter__c];
                    if (!filters.isEmpty()) {
                        ULiT_Negoptim__Filter__c filter = filters[0];
                        delete filter.ULiT_Negoptim__Filters_Criteria__r;
                        for (ULiT_Negoptim__Filter_Criteria__c criteria : templateToCreate.criterias) {
                            criteria.ULiT_Negoptim__Filter__c = filter.Id;
                        }
                        templateToCreate.template.ULiT_Negoptim__Template_Filter__c = filter.Id ;
                    } else {
                        if (templateToCreate.filter != null) {
                            insert templateToCreate.filter;
                            for (ULiT_Negoptim__Filter_Criteria__c criteria : templateToCreate.criterias) {
                                criteria.ULiT_Negoptim__Filter__c = templateToCreate.filter.Id;
                            }
                            templateToCreate.template.ULiT_Negoptim__Template_Filter__c = templateToCreate.filter.Id;
                        }
                    }
                    if (filters.isEmpty() || templateToCreate.filter != null) {
                        if(templateToCreate.criterias != null && !templateToCreate.criterias.isEmpty()) {
                            insert templateToCreate.criterias;
                        }
                    }
                }
                update templateToCreate.template;
            }
            return true;
        } catch (Exception ex) {
                System.debug('Error:'+ex);
                System.debug('Error Cause:'+ex.getCause());
                System.debug('Error Line:'+ex.getLineNumber());
                System.debug('Error Message:'+ex.getMessage());
        }
        return false;
    }

    @AuraEnabled
    public static Boolean importTemplates(String data){
        List<TemplateFileWrapper> templatesToCreate = (List<TemplateFileWrapper>) JSON.deserialize(data, List<TemplateFileWrapper>.class);
        Map<Integer, ULiT_Negoptim__NegoTemplate__c> templatesToInsert = new Map<Integer, ULiT_Negoptim__NegoTemplate__c>();
        Map<Integer, ULiT_Negoptim__Filter__c> filtersToInsert = new Map<Integer, ULiT_Negoptim__Filter__c>();
        Map<Integer, List<ULiT_Negoptim__Filter_Criteria__c>> criteriasToInsert = new Map<Integer, List<ULiT_Negoptim__Filter_Criteria__c>>();
        for (Integer index = 0; index < templatesToCreate.size(); index++) {
            templatesToInsert.put(index, templatesToCreate.get(index).template);
            if (templatesToCreate.get(index).filter != null) {
                filtersToInsert.put(index, templatesToCreate.get(index).filter);
                if (templatesToCreate.get(index).criterias != null) {
                    criteriasToInsert.put(index, templatesToCreate.get(index).criterias);
                }
            }
        }
        try {
            insert filtersToInsert.values();
            for (Integer index : templatesToInsert.keySet()) {
                if (filtersToInsert.get(index) != null) {
                    templatesToInsert.get(index).ULiT_Negoptim__Template_Filter__c = filtersToInsert.get(index).Id;
                    if(criteriasToInsert.get(index) != null && !criteriasToInsert.get(index).isEmpty()) {
                        for(ULiT_Negoptim__Filter_criteria__c criteria : criteriasToInsert.get(index)) {
                            criteria.ULiT_Negoptim__Filter__c = filtersToInsert.get(index).Id;
                        }
                    }
                }
            }
            list<ULiT_Negoptim__Filter_Criteria__c> toBeInserted = new List<ULiT_Negoptim__Filter_Criteria__c>();
            for (List<ULiT_Negoptim__Filter_Criteria__c> criterias : criteriasToInsert.values()) {
                toBeInserted.addAll(criterias);
            }
            insert toBeInserted;
            insert templatesToInsert.values();
            return true;
        } catch (Exception ex) {
                System.debug('Error:'+ex);
                System.debug('Error Cause:'+ex.getCause());
                System.debug('Error Line:'+ex.getLineNumber());
                System.debug('Error Message:'+ex.getMessage());
        }
        return false;
    }
    
    public class TemplateFileWrapper {
        public ULiT_Negoptim__NegoTemplate__c template ;
        public ULiT_Negoptim__Filter__c filter ;
        public List<ULiT_Negoptim__Filter_Criteria__c> criterias ;
    }
}