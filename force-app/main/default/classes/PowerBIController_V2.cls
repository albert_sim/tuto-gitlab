public class PowerBIController_V2 extends OAuthController {
    @TestVisible private static String APPLICATION_NAME = 'PowerBI';
    public String currentRecordId {get; set;}
    public String nsCode {get; set;}
    ////public ULiT_Negoptim__Price_List_Statement__c priceListStatement {get; set;}
    public PowerBIController_V2 (ApexPages.StandardController controller) {
        currentRecordId = ApexPages.CurrentPage().getparameters().get('id');
        if(currentRecordId != null) {
            ULiT_Negoptim__Price_List_Statement__c priceListStatement = [SELECT Id, Name, ULiT_Negoptim__NS_Code_Long__c
                                                                         FROM ULiT_Negoptim__Price_List_Statement__c
                                                                         WHERE Id = :currentRecordId];
            nsCode = priceListStatement.ULiT_Negoptim__NS_Code_Long__c;
        } else {
            nsCode = ApexPages.CurrentPage().getparameters().get('nsCode');
        }
        this.application_name = APPLICATION_NAME;
    }
    
    public String getValidateResult()
    {
        return validateResult;
    }
    
    /**
    * Validates the callback code and generates the access and refresh tokens
    *
    * @return null to refresh the page
    */
    public PageReference redirectOnCallback() {
        return super.redirectOnCallback(null);
    }
    
    public PageReference refreshAccessToken() {
        return super.refreshAccessToken(ApexPages.currentPage());
    }
}