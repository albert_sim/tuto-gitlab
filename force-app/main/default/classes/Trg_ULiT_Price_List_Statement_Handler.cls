/**
 * @author ULiT
 * @date 01 July 2021
*/
public class Trg_ULiT_Price_List_Statement_Handler {

    public static final Map<String, Id> priceListStatementRTIds = getObjectRecordTypeMapIds(ULiT_Negoptim__Price_List_Statement__c.SObjectType);
    
    /*
     * @description called in Trg_Price_List_Statement in case Trigger.isAfter && Trigger.isUpdate
    */
    public static void onAfterUpdate(List<ULiT_Negoptim__Price_List_Statement__c> newPriceListStatements, Map<Id, ULiT_Negoptim__Price_List_Statement__c> oldPriceListStatementsMap) {
        synchroToSupplierPG(newPriceListStatements, oldPriceListStatementsMap);
    }
    /**
     * Synchronize Price_List_Statement__c to Supplier_PG_Mapping__c upon validation
    **/
    private static void synchroToSupplierPG(List<ULiT_Negoptim__Price_List_Statement__c> newPriceListStatements, Map<Id, ULiT_Negoptim__Price_List_Statement__c> oldPriceListStatementsMap) {
        Set<Id> priceListStatementIds = new Set<Id>();
        // Check records moved to VALIDATED
        for(ULiT_Negoptim__Price_List_Statement__c item : newPriceListStatements) {
            if((item.RecordTypeId == priceListStatementRTIds.get('Depreciation_Rate') || item.RecordTypeId == priceListStatementRTIds.get('Simulation'))
               && (item.ULiT_Negoptim__Status__c == 'Validated' && item.ULiT_Negoptim__Status__c != oldPriceListStatementsMap.get(item.Id).ULiT_Negoptim__Status__c))
            {
                priceListStatementIds.add(item.Id);
            }
        }
        if (!System.isBatch() && !priceListStatementIds.isEmpty()) {
            // call PriceListPGSynchroBatch for update PGs
            ULiT_Negoptim.NegoptimBatch.executeBatch(new PriceListPGSynchroBatch('Trg_ULiT_Price_List_Statement_Handler', priceListStatementIds, null), 200);
        }
    }
    
    private static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();
        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }
}