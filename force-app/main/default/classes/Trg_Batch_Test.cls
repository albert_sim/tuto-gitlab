@isTest
public class Trg_Batch_Test {
    private static Id rootId;
    private static ULit_Negoptim__Orga_BU__c country;
    private static ULit_Negoptim__Orga_HE__c department;
    private static ULit_Negoptim__Sup_Supplier__c supplier;
    private static ULit_Negoptim__Orga_HE__c section;
    private static Product2 product;
    private static ULiT_Negoptim__Supplier_PG_Mapping__c supPGMapping;
    private static Integer randomNumber;
    private static final Integer year = System.Today().year();
    private static Id productIntegrationRT;
	private static ULit_Negoptim__Sup_sup_NegoScope__c NS;
	private static ULit_Negoptim__Sup_sup_NegoScope__c SubNs;
	private static ULit_Negoptim__Contract__c contract;
	private static ULit_Negoptim__Pol_Com_Condition__c tariffCondition1;
	private static ULit_Negoptim__Pol_Com_Condition__c tariffCondition2;
	private static ULit_Negoptim__Pol_Com_Condition__c tariffCondition3;
    public static final Map<String, Id> conditionRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Contract_Discount__c.SObjectType);
    public static final Map<String, Id> contractRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Contract__c.SObjectType);
    public static final Map<String, Id> buRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Orga_BU__c.SObjectType);

    static void init() {
        department = createDepartment(true, rootId);
        System.debug('>>>' + department.Id);
        section = createSection(true, department.Id);
        System.debug(section.Id);
        // Insert Country List.
        ULit_Negoptim__Country_List__c myCountry = createCountry(true, 'FRANCE', 'FR');
        // Insert Region.
        ULit_Negoptim__Orga_BU__c region = createBURegion(true, 'EU');
        // Insert Country.
        country = createBUCountry(true, myCountry, region.Id);
        // Insert supplier.
        supplier = createSupplier(true, country, true);
        System.debug(supplier.Id);
        product = createProduct(true, section.Id, supplier.Id);
        productIntegrationRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Product_Integration' AND SobjectType = 'ULit_Negoptim__Batch__c' LIMIT 1]?.Id;
        // Get the default negoscope created on supplier creation.
        NS = getNSs(supplier.Id)[0];
        SubNs = createNegoScope(false, supplier, 'Sub NS name');
        SubNs.ULit_Negoptim__Parent_Nego_Scope__c = NS.Id;
        insert SubNs;
        // Insert Policy - Tariff conditions.
        tariffCondition1 = createTariffCondition(false, 0, 1);
        tariffCondition1.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        tariffCondition1.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        tariffCondition2 = createTariffCondition(false, 0, 2);
        tariffCondition2.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        tariffCondition2.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        tariffCondition3 = createTariffCondition(false, 0, 3);
        tariffCondition3.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        tariffCondition3.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        insert new List<ULit_Negoptim__Pol_Com_Condition__c>{tariffCondition1, tariffCondition2, tariffCondition3};
            // Insert contract.
            contract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        contract.ULit_Negoptim__TO1__c = 100000;
        insert contract;
    }

    @istest
    static void case1_Supplier_PG_Mapping_Integration(){
        init();
        Test.startTest();
        ULit_Negoptim__Batch__c batchIntegration = new ULit_Negoptim__Batch__c(Name = 'Batch Test Integration', RecordTypeId = productIntegrationRT, ULit_Negoptim__Status__c='In Progress');
        
        supPGMapping = createPG(false, supplier.Id, department.Id, section.Id);
        supPGMapping.SPITM_Tax_1_Legacy__c = 1;
        supPGMapping.SPITM_Tax_2_Legacy__c = 2;
        supPGMapping.SPITM_Tax_3_Legacy__c = 3;
        insert supPGMapping;
        insert batchIntegration;
        Test.stopTest();
        supPGMapping = [SELECT Id, SPITM_Batch__c, ULiT_Negoptim__Tax_1_New__c, ULiT_Negoptim__Tax_2_New__c, ULiT_Negoptim__Tax_3_New__c 
                        FROM ULiT_Negoptim__Supplier_PG_Mapping__c 
                        WHERE Id = :supPGMapping.Id];
        batchIntegration = [SELECT Id, ULit_Negoptim__Status__c FROM ULit_Negoptim__Batch__c WHERE Id = :batchIntegration.Id];
        
        System.assertEquals(1, supPGMapping.ULiT_Negoptim__Tax_1_New__c);
        System.assertEquals(2, supPGMapping.ULiT_Negoptim__Tax_2_New__c);
        System.assertEquals(3, supPGMapping.ULiT_Negoptim__Tax_3_New__c);
        System.assertEquals(batchIntegration.Id, supPGMapping.SPITM_Batch__c);
        System.assertEquals('Completed', batchIntegration.ULit_Negoptim__Status__c);
      
    }

    @istest
    static void case2_calculateContractIndex() {
		init();
		// set tariff Index
        tariffCondition1.ULit_Negoptim__Is_Tariff_Condition__c = true;
		tariffCondition1.ULit_Negoptim__Index__c = 'Index1';
		update tariffCondition1;
        contract.ULit_Negoptim__Rise_Rate__c = 3.14;
        contract.ULit_Negoptim__TO1__c = 849014;
        update contract;
		// Insert conditions.
		ULit_Negoptim__Contract_Discount__c condition1 = createCondition(false, tariffCondition1, contract, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
		condition1.ULit_Negoptim__Value_per__c = 4;
		condition1.ULit_Negoptim__Value_amt__c = 0;
		condition1.ULit_Negoptim__Rank__c = 1;
		condition1.ULit_Negoptim__Is_Tariff_Condition__c = true;
        ULit_Negoptim__Contract_Discount__c condition2 = createCondition(false, tariffCondition1, contract, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
		condition2.ULit_Negoptim__Value_per__c = 31.50;
		condition2.ULit_Negoptim__Value_amt__c = 0;
		condition2.ULit_Negoptim__Rank__c = 2;
		condition2.ULit_Negoptim__Is_Tariff_Condition__c = true;
        condition2.ULit_Negoptim__Product_Scope__c = SubNs.Id;
        condition2.ULit_Negoptim__Base_TO_Nego__c = 383000;
        ULit_Negoptim__Contract_Discount__c condition3 = createCondition(false, tariffCondition1, contract, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        condition3.ULit_Negoptim__Value_per__c = 23.12;
		condition3.ULit_Negoptim__Value_amt__c = 0;
		condition3.ULit_Negoptim__Rank__c = 3;
		condition3.ULit_Negoptim__Is_Tariff_Condition__c = true;
        condition3.ULit_Negoptim__Product_Scope__c = SubNs.Id;
        condition3.ULit_Negoptim__Base_TO_Nego__c = 382000;
        insert new List<ULit_Negoptim__Contract_Discount__c>{condition1, condition2, condition3};
        contract.ULit_Negoptim__Is_Managed_By_ExternalSource__c = true;
        contract.SPITM_Run_Calculation__c = true;
        update contract;
		// Start Test
        Test.startTest();
        Id contractIntegrationRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contract_Integration' AND SobjectType = 'ULit_Negoptim__Batch__c' LIMIT 1]?.Id;
        ULit_Negoptim__Batch__c batch = new ULit_Negoptim__Batch__c(RecordTypeId = contractIntegrationRT, ULit_Negoptim__Status__c = 'In Progress');
        insert batch;
        Test.stopTest();
        List<ULit_Negoptim__Contract_Discount__c> conditionList = [SELECT Id,  ULit_Negoptim__YTD_Nego_Computed__c FROM ULit_Negoptim__Contract_Discount__c];
        contract = [SELECT Id, ULit_Negoptim__Index1__c, ULit_Negoptim__Index1_perc__c, ULit_Negoptim__Total_TO_Tariff_Nego__c FROM ULit_Negoptim__Contract__c WHERE Id = :contract.Id];
		// Assertions.
		System.assertEquals(73.740, contract.ULit_Negoptim__Index1__c.setScale(3));
        System.assertEquals(28.505, contract.ULit_Negoptim__Index1_perc__c.setScale(3));
        System.assertEquals(1187517.020, contract.ULit_Negoptim__Total_TO_Tariff_Nego__c.setScale(3));
		System.assertEquals(47500.681, conditionList[0].ULit_Negoptim__YTD_Nego_Computed__c.setScale(3));
        System.assertEquals(176124.088, conditionList[1].ULit_Negoptim__YTD_Nego_Computed__c.setScale(3));
        System.assertEquals(114878.252, conditionList[2].ULit_Negoptim__YTD_Nego_Computed__c.setScale(3));
	}

    /* PRODUCT GROUP + PURCHASE */
    // Supplier PG Mapping creation.
    public static ULiT_Negoptim__Supplier_PG_Mapping__c createPG(Boolean doInsert, Id supplierId, Id departmentId, Id sectionId) {
        String rdm = String.valueOf(getRandomNumber());
        ULiT_Negoptim__Supplier_PG_Mapping__c PG = new ULiT_Negoptim__Supplier_PG_Mapping__c(ULiT_Negoptim__Supplier__c = supplierId, ULiT_Negoptim__Department__c = departmentId,
                                                                                             ULiT_Negoptim__Section__c = sectionId, Name = 'PG' + rdm, ULiT_Negoptim__SNA_Code__c = rdm, 
                                                                                             ULiT_Negoptim__GIMA_Code__c = rdm);
        if (doInsert) Database.insert(PG);
        System.assert(true);
        return PG;
    }
    // Product creation.
    public static Product2 createProduct(Boolean doInsert, Id sectionId, Id supplierId) {
        Id brandInitRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Brand_Initialization' AND SobjectType = 'ULiT_Negoptim__Sup_Brand__c']?.Id;
        Id brandRegRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Brand_Regular' AND SobjectType = 'ULiT_Negoptim__Sup_Brand__c']?.Id;
       
        ULiT_Negoptim__Sup_Brand__c parentBrand = new ULiT_Negoptim__Sup_Brand__c(Name = 'parentBrand',
                                                                                  ULiT_Negoptim__Brand_Owner__c = supplierId,
                                                                                  ULiT_Negoptim__Status_BDate__c = Date.today(),
                                                                                  RecordTypeId = brandInitRT);
        insert parentBrand;
        ULiT_Negoptim__Sup_Brand__c brand = new ULiT_Negoptim__Sup_Brand__c(Name = 'brand',
                                                                            ULiT_Negoptim__Brand_Owner__c = supplierId,
                                                                            ULiT_Negoptim__Status_BDate__c = Date.today(),
                                                                            ULiT_Negoptim__Brand_Parent__c = parentBrand.Id,
                                                                            RecordTypeId = brandRegRT);
        insert brand;
        String rdm = String.valueOf(getRandomNumber());
        Product2 product = new Product2(Name = 'PRD ' + rdm, ProductCode = 'P' + rdm, ULit_Negoptim__Product_EAN__c = '10000' + rdm,
                                        ULit_Negoptim__Category__c = sectionId, ULit_Negoptim__Product_MasterSupplier__c = supplierId, IsActive = true,
                                        ULit_Negoptim__Ext_Id__c = '10000' + rdm + '_' + 'P' + rdm,
                                        ULiT_Negoptim__Brand__c = brand.Id);
        if (doInsert) insert product;
        System.assert(true);
        return product;
    }
    /* HIERARCHY ELEMENT + BRAND + PRODUCT + SELL IN */
    // Department creation.
    public static ULit_Negoptim__Orga_HE__c createDepartment(Boolean doInsert, Id parentId) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Orga_HE__c department = new ULit_Negoptim__Orga_HE__c(Name = 'DPT ' + rdm, ULit_Negoptim__Elt_Code__c = 'D' + rdm, ULit_Negoptim__Parent_Element__c = parentId, ULit_Negoptim__Level__c = 1,
                                                                             ULit_Negoptim__Dispatch_Inv_Hierarchy_Starting_Point__c = false, ULit_Negoptim__Purchases_DB_Upper_Starting_Point__c = true,
                                                                             ULit_Negoptim__Set_Assortment_nego_level__c = true, ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), 
                                                                             ULit_Negoptim__Status__c = 'Active');
        if (doInsert) insert department;
        System.assert(true);
        return department;
    }
    // Section creation.
    public static ULit_Negoptim__Orga_HE__c createSection(Boolean doInsert, Id parentId) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Orga_HE__c section = new ULit_Negoptim__Orga_HE__c(Name = 'SEC ' + rdm, ULit_Negoptim__Elt_Code__c = 'S' + rdm, 
                                                                          ULit_Negoptim__Parent_Element__c = parentId, ULit_Negoptim__Level__c = 2,
                                                                          ULit_Negoptim__Dispatch_Inv_Hierarchy_Starting_Point__c = true, 
                                                                          ULit_Negoptim__Purchases_DB_Upper_Starting_Point__c = false,
                                                                          ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Active', ULit_Negoptim__Set_Product_link_level__c = true);
        if (doInsert) insert section;
        System.assert(true);
        return section;
    }
    /* BUSINESS UNIT */
    // Country List creation.
    public static ULit_Negoptim__Country_List__c createCountry(Boolean doInsert, String name, String code) {
        ULit_Negoptim__Country_List__c myCountry = new ULit_Negoptim__Country_List__c(Name = name, ULit_Negoptim__Country_Code__c = code);
        if (doInsert) insert myCountry;
        System.assert(true);
        return myCountry;
    }
    // Region creation.
    public static ULit_Negoptim__Orga_BU__c createBURegion(Boolean doInsert, String name) {
        ULit_Negoptim__Orga_BU__c region = new ULit_Negoptim__Orga_BU__c(Name = name, ULit_Negoptim__BU_Code__c = name, ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Open');
        region.RecordTypeId =[SELECT Id FROM RecordType WHERE DeveloperName = 'Region' AND SobjectType = 'ULit_Negoptim__Orga_BU__c']?.Id;
        if (doInsert) insert region;
        System.assert(true);
        return region;
    }
    // Country creation.
    public static ULit_Negoptim__Orga_BU__c createBUCountry(Boolean doInsert, ULit_Negoptim__Country_List__c c, Id regionId) {
        ULit_Negoptim__Orga_BU__c country = new ULit_Negoptim__Orga_BU__c(Name = c.Name, ULit_Negoptim__BU_Code__c = c.ULit_Negoptim__Country_Code__c + getRandomNumber(), ULit_Negoptim__ISO_Country_Code__c = c.ULit_Negoptim__Country_Code__c,
                                                                          ULit_Negoptim__Country__c = c.Name, ULit_Negoptim__Country_Zone_origin__c = regionId, ULit_Negoptim__Status__c = 'Open',
                                                                          ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                                                          ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                                                          ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                                                          ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                                                          ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                                                          ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                                                          ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                                                          ULit_Negoptim__Set_BU_List_Entity__c = true,
                                                                          ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                                                          ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                                                          ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                                                          ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                                                          ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                                                          ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                                                          ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                                                          ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                                                          ULit_Negoptim__Set_Invoice_Detail__c = true,
                                                                          ULit_Negoptim__Set_Invoice_Payment__c = true,
                                                                          ULit_Negoptim__Set_Invoice__c = true,
                                                                          ULit_Negoptim__Set_Product_Listing__c = true,
                                                                          ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                                                          ULit_Negoptim__Set_SF_Planning__c = true);
        country.RecordTypeId =[SELECT Id FROM RecordType WHERE DeveloperName = 'Country' AND SobjectType = 'ULit_Negoptim__Orga_BU__c']?.Id;
        if (doInsert) insert country;
        System.assert(true);
        return country;
    }
    /* SUPPLIER + NEGOSCOPE */
    // Supplier creation.
    public static ULit_Negoptim__Sup_Supplier__c createSupplier(Boolean doInsert, ULit_Negoptim__Orga_BU__c country, Boolean withNS) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Sup_Supplier__c supplier = new ULit_Negoptim__Sup_Supplier__c(Name = 'SUP ' + rdm, ULit_Negoptim__Code__c = rdm, ULit_Negoptim__Country_origin__c = country.Id, ULit_Negoptim__Acc_Country__c = country.Name,
                                                                                     ULit_Negoptim__Is_Default_NegoScope__c = withNS, ULit_Negoptim__Acc_Address_External_Synchro__c = false, ULit_Negoptim__Admin_Address_External_Synchro__c = false,
                                                                                     ULit_Negoptim__Status__c = 'Active');
        if (doInsert) insert supplier;
        System.assert(true);
        return supplier;
    }

    // Get the list of NS related to the supplier passed by parameter.
	private static List<ULit_Negoptim__Sup_sup_NegoScope__c> getNSs(Id SupplierId) {
        System.assert(true);
        return [SELECT Id, Name, ULit_Negoptim__Supplier__c, ULit_Negoptim__Supplier__r.Name, ULit_Negoptim__Supplier__r.ULit_Negoptim__Code__c, ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                OwnerId, ULit_Negoptim__NS_Code__c, ULit_Negoptim__NS_Code_Long__c
                FROM ULit_Negoptim__Sup_sup_NegoScope__c
                WHERE ULit_Negoptim__Supplier__c = :supplierId];
        
    }

    // Create an instance of Sup_sup_NegoScope__c
    public static ULit_Negoptim__Sup_sup_NegoScope__c createNegoScope(Boolean doInsert, ULit_Negoptim__Sup_Supplier__c supplier, String name) {
        ULit_Negoptim__Sup_sup_NegoScope__c negoScope = new ULit_Negoptim__Sup_sup_NegoScope__c(ULit_Negoptim__Supplier__c = supplier.id, Name = name,
                                                                                                ULit_Negoptim__Acc_Address_External_Synchro__c = false,
                                                                                                ULit_Negoptim__Admin_Address_External_Synchro__c = false);
        if (doInsert) insert negoScope;
        return negoScope;
    }

    // Tarrif Condition creation.
    public static ULit_Negoptim__Pol_Com_Condition__c createTariffCondition(Boolean doInsert, Integer pickListValue, Integer indexValue) {
    	String rdm = String.valueOf(getRandomNumber());
    	// Get field describe.
        Schema.DescribeFieldResult fieldResult = ULit_Negoptim__Pol_Com_Condition__c.ULit_Negoptim__Nego_Discount_Type__c.getDescribe();
        // Get the picklist value.
        String plv = fieldResult.getPicklistValues().get(pickListValue).getValue();
        // Create an instance of Tarrif Condition.
        ULit_Negoptim__Pol_Com_Condition__c tariffCondition = new ULit_Negoptim__Pol_Com_Condition__c(Name = plv, ULit_Negoptim__Nego_Discount_Type__c = plv, ULit_Negoptim__Index__c = 'Index' + indexValue,
                                                                                                      ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Condition_Code__c = rdm,
                                                                                                      ULit_Negoptim__Abrev__c = 'TC', ULit_Negoptim__Gen_Name_com__c = 'TC', ULit_Negoptim__Name_Com__c = 'TC', ULit_Negoptim__Name_Fi__c = 'TC', ULit_Negoptim__Name_Leg__c = 'TC',
                                                                                                      ULit_Negoptim__Gen_Name_Fi__c = 'TC', ULit_Negoptim__Gen_Name_Leg__c = 'TC', ULit_Negoptim__VAT_Type__c = 'Rebate', ULit_Negoptim__Acc_Document_Type__c = 'Credit Note Request',
                                                                                                      ULit_Negoptim__Is_Tariff_Condition__c = true);
        // Do insertion.
        if (doInsert) insert tariffCondition;
        System.assert(true);
        return tariffCondition;
    }
    
    // Condition creation.
    public static ULit_Negoptim__Contract_Discount__c createCondition(Boolean doInsert, ULit_Negoptim__Pol_Com_Condition__c tariffCondition, ULit_Negoptim__Contract__c contract, Date beginDate, Date endDate) {
        ULit_Negoptim__Contract_Discount__c condition = new ULit_Negoptim__Contract_Discount__c(ULit_Negoptim__Nego_Discount_Type__c = tariffCondition.ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Condition_Type__c = tariffCondition.Id,
                                                                                                ULit_Negoptim__Contract__c = contract.Id, ULit_Negoptim__Product_Scope__c = contract.ULit_Negoptim__Supplier_Nego_Scope__c, ULit_Negoptim__BU_Scope__c = contract.ULit_Negoptim__BU_Source__c,
                                                                                                ULit_Negoptim__Disc_BDate__c = beginDate, ULit_Negoptim__Disc_EDate__c = endDate,
                                                                                                RecordTypeId = conditionRTIds.get('To_Be_Signed'));
        if (doInsert) Database.insert(condition);
        System.assert(true);
        return condition;
    }

    // Contract creation.
    public static ULit_Negoptim__Contract__c createContract(Boolean doInsert, ULit_Negoptim__Sup_sup_NegoScope__c NS, Date beginDate, Date endDate) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Contract__c contract = new ULit_Negoptim__Contract__c(Name = NS.ULit_Negoptim__Supplier__r.Name + ' - Contract ' + year, ULit_Negoptim__Supplier__c = NS.ULit_Negoptim__Supplier__c,
                                                                             ULit_Negoptim__Supplier_Nego_Scope__c = NS.Id, ULit_Negoptim__Contract_BU__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                                                                             ULit_Negoptim__BU_Source__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c, ULit_Negoptim__Contract_BDate__c = beginDate,
                                                                             ULit_Negoptim__Contract_EDate__c = endDate, ULit_Negoptim__Duration__c = beginDate.monthsBetween(endDate) + 1,
                                                                             ULit_Negoptim__Contract_Commercial_BDate__c = beginDate, ULit_Negoptim__Contract_Commercial_EDate__c = endDate,
                                                                             ULit_Negoptim__Contract_Type__c = 'Contract', ULit_Negoptim__Status__c = 'Signed', ULit_Negoptim__Ext_Id__c = rdm, ULit_Negoptim__D_N__c = 'N',
                                                                             ULit_Negoptim__Duration_type__c = 'Month', ULit_Negoptim__Contract_Numbder__c = 'c' + rdm,
                                                                             RecordTypeId = contractRTIds.get('To_Be_Signed'), ULit_Negoptim__Is_Managed_By_ExternalSource__c = false);
        if (doInsert) Database.insert(contract);
        System.assert(true);
        return contract;
    }
    // Method to increment randomNumber
    private static Integer getRandomNumber() {
        if (randomNumber == null)
            randomNumber = (Integer)(Math.random()*9999);
        return randomNumber++;
    }

    /**
     * Get Object RecordType Map of Name/Id
     * @param sObjectType
     * */
    public static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();
        // Check Accessibility.
        // if(!checkAccessibilityFields(Schema.SObjectType.RecordType.fields.getMap(), new String [] {'Id', 'DeveloperName'})) {
        // 	return rtMap;
        // }
        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }
}