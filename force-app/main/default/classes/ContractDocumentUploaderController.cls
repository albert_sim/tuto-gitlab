/**
* @author ULiT
* @description create contract documents from Uploaded files using the negoptimContractDocumentsUploader LWC
**************************************************************************************************************************** */
public with sharing class ContractDocumentUploaderController {
    @AuraEnabled(Cacheable = false)
    public static void createContractDocument(String uploadedFiles, Id contractId) {
        try {
            List<UploadedFileWrapper> files = (List<UploadedFileWrapper>) JSON.deserialize(uploadedFiles, List<UploadedFileWrapper>.class);
            List<ULiT_Negoptim__Contract_Document__c> documents = new List<ULiT_Negoptim__Contract_Document__c>();
            for (UploadedFileWrapper file : files) {
                documents.add(new ULiT_Negoptim__Contract_Document__c(ULiT_Negoptim__Document_Name__c = file.name, ULiT_Negoptim__Document__c = String.valueOf(file.documentId), ULiT_Negoptim__Contract__c = contractId, ULiT_Negoptim__Status__c = 'Printed', ULiT_Negoptim__lastSeq__c = true, ULiT_Negoptim__Version__c = 1));
            }
            insert documents;
        } catch (Exception e) {
            System.debug('The following exception has occurred: ' + e.getMessage());
            System.debug('Error Cause:'+ e.getCause());
            System.debug('Error Line:'+ e.getLineNumber());
            throw new AuraHandledException(e.getMessage());
        }
    }
    public class UploadedFileWrapper {
        public String name;
        public Id documentId;
        public Id contentVersionId;
    }
}