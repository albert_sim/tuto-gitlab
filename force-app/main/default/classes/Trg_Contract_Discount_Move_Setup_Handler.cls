/**
 * @author ULiT
 * @date 26-08-2021
 * @description Apex handler for trigger on ULiT_Negoptim__Contract_Discount_Move_Setup__c to manage Mass Move preparation of conditions
******************************************************************************************************************************************* */
public with sharing class Trg_Contract_Discount_Move_Setup_Handler {

    public static void OnAfterUpdate(List<ULiT_Negoptim__Contract_Discount_Move_Setup__c> nConditionMoveSetup, Map<Id, ULiT_Negoptim__Contract_Discount_Move_Setup__c> oConditionMoveSetup) {
        prepareMassMoveConditions(nConditionMoveSetup, oConditionMoveSetup);
    }

    /**
     * Execute MassPrepareMoveConditionBatch when status = Applied
     * @param List<ULiT_Negoptim__Contract_Discount_Move_Setup__c>, Map<Id, ULiT_Negoptim__Contract_Discount_Move_Setup__c>.
     * On After Update.
     **/
    private static void prepareMassMoveConditions(List<ULiT_Negoptim__Contract_Discount_Move_Setup__c> nConditionMoveSetup, Map<Id, ULiT_Negoptim__Contract_Discount_Move_Setup__c> oConditionMoveSetup) {
        Map<String, ULiT_Negoptim__Contract_Discount_Move_Setup__c> conditionMoveSetupMap = new Map<String, ULiT_Negoptim__Contract_Discount_Move_Setup__c>();
        Set<Id> tariffConditionSourceSet = new Set<Id>();
        Set<Integer> yearSet = new Set<Integer>();
        Set<Id> buSet = new Set<Id>();
        Set<String> recordTypeSet = new Set<String>();
        Set<String> statusSet = new Set<String>();
        for (ULiT_Negoptim__Contract_Discount_Move_Setup__c item : nConditionMoveSetup) {
            if (item.ULiT_Negoptim__Status__c != oConditionMoveSetup.get(item.Id).ULiT_Negoptim__Status__c
                && (item.ULiT_Negoptim__Status__c == 'Run' || item.ULiT_Negoptim__Status__c == 'Cancel'))
            {
                // Contract_Status__c is Multi-Picklist
                if (item.ULiT_Negoptim__Contract_Status__c != null) {
                    List<String> selectedStatuses = item.ULiT_Negoptim__Contract_Status__c.split(';');
                    for (String status : selectedStatuses) {
                        String key = Integer.valueOf(item.ULiT_Negoptim__Reference_NegoYear__c) + ''
                                    + item.ULiT_Negoptim__Reference_BU__c + ''
                                    + item.ULiT_Negoptim__Contract_Record_Type__c + ''
                                    + status + ''
                            		+ item.ULiT_Negoptim__Tariff_Condition_Source__c;
                        conditionMoveSetupMap.put(key, item);
                        statusSet.add(status);
                    }
                }
                tariffConditionSourceSet.add(item.ULiT_Negoptim__Tariff_Condition_Source__c);
                yearSet.add(Integer.valueOf(item.ULiT_Negoptim__Reference_NegoYear__c));
                buSet.add(item.ULiT_Negoptim__Reference_BU__c);
                recordTypeSet.add(item.ULiT_Negoptim__Contract_Record_Type__c);
            }
        }
        if (!conditionMoveSetupMap.isEmpty() && !tariffConditionSourceSet.isEmpty() && !yearSet.isEmpty() && !buSet.isEmpty() && !recordTypeSet.isEmpty() && !statusSet.isEmpty()) {
            Database.executeBatch(new MassPrepareMoveConditionBatch(Trg_Contract_Discount_Move_Setup_Handler.class.getName(), conditionMoveSetupMap, tariffConditionSourceSet, yearSet, buSet, recordTypeSet, statusSet), 200);
        }
    }
}