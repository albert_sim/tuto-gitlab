/**
 * @author ULiT
 * @date 06 September 2021
 * @description executes batches, used in screen flows
*/
global class ExecuteBatchInvocable {
        
    @InvocableMethod(label = 'ExecuteBatch')
    global static void execute(List<BatchParametersWrapper> batchParamsList) {
        if (batchParamsList != null && !batchParamsList.isEmpty()) {
            BatchParametersWrapper batchParams = batchParamsList[0];
            switch on batchParams.batchName {
                when 'ULiT_Negoptim.ConsolidateContractBatch' {
                    ULiT_Negoptim.NegoptimBatch.executeBatch(new ULiT_Negoptim.ConsolidateContractBatch(ExecuteBatchInvocable.class.getName() + ' [' + batchParams.startedFrom + ']',
                                                                                                        batchParams.supplierBU != null ? new List<Id> {batchParams.supplierBU} : null,
                                                                                                        batchParams.supplierId != null ? new List<Id> {batchParams.supplierId} : null,
                                                                                                        batchParams.negoScopeId != null ? new List<Id> {batchParams.negoScopeId} : null,
                                                                                                        batchParams.contractBU != null ? new List<Id> {batchParams.contractBU} : null,
                                                                                                        batchParams.year), 50);
                }
            }
        }
    }
    
    global class BatchParametersWrapper {
        
        @InvocableVariable(required=true)
        global String batchName;
        
        @InvocableVariable
        global String startedFrom;
        @InvocableVariable
        global Id supplierBU;
        @InvocableVariable
        global Id supplierId;
        @InvocableVariable
        global Id negoScopeId;
        @InvocableVariable
        global Id contractBU;
        @InvocableVariable
        global Integer year;
    }
}