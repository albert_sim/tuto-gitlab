/**
 * @author ULiT
 * @date 09 September 2021
 * @description update the value of ULiT_Negoptim__Use_Electronic_Signature__c to TRUE for all negoscopes with 
 * ULiT_Negoptim__Use_Electronic_Signature__c = FALSE
*/
public class UpdateNSUseElectronicSignatureBatch implements Database.Batchable<SObject> {
    
    private ULiT_Negoptim.NegoptimBatch nb;
    private String query;
    
    public UpdateNSUseElectronicSignatureBatch(String startedFrom, List<Id> nsIdList) {
        this.nb = new ULiT_Negoptim.NegoptimBatch(UpdateNSUseElectronicSignatureBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateless, startedFrom);
        this.query = 'SELECT Id, ULiT_Negoptim__Use_Electronic_Signature__c';
        this.query += ' FROM ULiT_Negoptim__Sup_sup_NegoScope__c';
        this.query += ' WHERE ULiT_Negoptim__Use_Electronic_Signature__c = FALSE';        
        if (nsIdList != null && !nsIdList.isEmpty()) {
            this.query += ' AND Id IN (\'' + String.join(nsIdList, '\',\'') + '\')';
        }
        
        this.nb.logParameter('nsIdList', nsIdList);
        this.nb.logParameter('query', this.query);       
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(this.query);
    }
    
    
    public void execute(Database.BatchableContext bc, List<ULiT_Negoptim__Sup_sup_NegoScope__c> scope) {
        System.Savepoint sp = Database.setSavepoint();
        try {
            for (ULiT_Negoptim__Sup_sup_NegoScope__c ns : scope) {
                ns.ULiT_Negoptim__Use_Electronic_Signature__c = true;
            }
            List<Database.SaveResult> results = Database.update(scope, false);
            this.nb.logResults(results, scope);
        } catch (Exception ex) {
            this.nb.logError(ex);
            Database.rollback(sp);
        }
        this.nb.saveLog(bc);
    }
    
    
    public void finish(Database.BatchableContext bc) {
        this.nb.saveLog(bc);
    }
}