/**
 * @author ULiT
 * @date 09 September 2021
 * @description Test class for UpdateNSUseElectronicSignatureBatch
*/
@isTest
private class UpdateNSUseElectronicSignatureBatchTest {
    
    static ULiT_Negoptim__Sup_sup_NegoScope__c NS1;
    static ULiT_Negoptim__Sup_sup_NegoScope__c NS2;
    
    static void init () {
        Integer year = Date.today().year();
        
        Map<String, Id> buRTIds = new Map<String, Id>();
        
        for(RecordType item : [SELECT Id, DeveloperName
                               FROM RecordType
                               WHERE SobjectType = 'ULiT_Negoptim__Orga_BU__c'
                               AND IsActive = true])
        {
            buRTIds.put(item.DeveloperName, item.Id);
        }
            
        ULiT_Negoptim__Country_List__c countryList = new ULiT_Negoptim__Country_List__c(Name = 'Country',
                                                                                        ULiT_Negoptim__Country_Code__c = 'CTR');
        insert countryList;

        ULiT_Negoptim__Orga_BU__c region = new ULiT_Negoptim__Orga_BU__c(Name = 'Region',
                                                                         ULiT_Negoptim__BU_Code__c = 'RGN',
                                                                         ULiT_Negoptim__Country__c = countryList.Name,
                                                                         ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                                                         ULiT_Negoptim__Status__c = 'Open', 
                                                                         RecordTypeId = buRTIds.get('Region'));
        insert region;
        
        ULiT_Negoptim__Orga_BU__c country = new ULiT_Negoptim__Orga_BU__c(Name = 'Country',
                                                                          ULiT_Negoptim__BU_Code__c = 'CTR1',
                                                                          ULiT_Negoptim__ISO_Country_Code__c = countryList.ULiT_Negoptim__Country_Code__c,
                                                                          ULiT_Negoptim__Country__c = countryList.Name,
                                                                          ULiT_Negoptim__Country_Zone_origin__c = region.Id,
                                                                          ULiT_Negoptim__Status__c = 'Open',
                                                                          ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                                                          RecordTypeId = buRTIds.get('Country'));
        insert country;
        
        ULiT_Negoptim__Sup_Supplier__c supplier = new  ULiT_Negoptim__Sup_Supplier__c(Name = 'supplier1',
                                                                                      ULiT_Negoptim__Code__c = 'code1',
                                                                                      ULiT_Negoptim__Country_origin__c = country.Id,
                                                                                      ULiT_Negoptim__Acc_Country__c = country.Name,
                                                                                      ULiT_Negoptim__Is_Default_NegoScope__c = false,
                                                                                      ULiT_Negoptim__Status__c = 'Active',
                                                                                      ULiT_Negoptim__Acc_Address_External_Synchro__c = false,
                                                                                      ULiT_Negoptim__Admin_Address_External_Synchro__c = false);
        insert supplier;
        
        NS1 = new ULiT_Negoptim__Sup_sup_NegoScope__c(Name = 'NS1',
                                                      ULiT_Negoptim__Supplier__c = supplier.Id,
                                                      ULiT_Negoptim__Status__c = 'Active',
                                                      ULiT_Negoptim__Acc_Address_External_Synchro__c = false,
                                                      ULiT_Negoptim__Admin_Address_External_Synchro__c = false, 
                                                      ULiT_Negoptim__Use_Electronic_Signature__c = false);
        
        NS2 = new ULiT_Negoptim__Sup_sup_NegoScope__c(Name = 'NS2',
                                                      ULiT_Negoptim__Supplier__c = supplier.Id,
                                                      ULiT_Negoptim__Status__c = 'Active',
                                                      ULiT_Negoptim__Acc_Address_External_Synchro__c = false,
                                                      ULiT_Negoptim__Admin_Address_External_Synchro__c = false, 
                                                      ULiT_Negoptim__Use_Electronic_Signature__c = false);
        
        insert new List <ULiT_Negoptim__Sup_sup_NegoScope__c> {NS1, NS2};
    }
    
    testmethod static void case1_Filter_NS_Id() {
        init();
        Test.startTest();
        Database.executeBatch(new UpdateNSUseElectronicSignatureBatch('UpdateNSUseElectronicSignatureBatchTest', new List <Id> {NS1.Id}), 100);
        Test.stopTest();
        System.assert([SELECT Id, ULiT_Negoptim__Use_Electronic_Signature__c FROM ULiT_Negoptim__Sup_sup_NegoScope__c WHERE Id = :NS1.Id].ULiT_Negoptim__Use_Electronic_Signature__c);
        System.assert(![SELECT Id, ULiT_Negoptim__Use_Electronic_Signature__c FROM ULiT_Negoptim__Sup_sup_NegoScope__c WHERE Id = :NS2.Id].ULiT_Negoptim__Use_Electronic_Signature__c);
        
    }
    
    testmethod static void case2_NO_Filter_NS_Id() {
        init();
        Test.startTest();
        Database.executeBatch(new UpdateNSUseElectronicSignatureBatch('UpdateNSUseElectronicSignatureBatchTest', null), 100);
        Test.stopTest();
        List<ULiT_Negoptim__Sup_sup_NegoScope__c> nsList = [SELECT Id, ULiT_Negoptim__Use_Electronic_Signature__c
                                                            FROM ULiT_Negoptim__Sup_sup_NegoScope__c
                                                            WHERE ULiT_Negoptim__Use_Electronic_Signature__c = FALSE];
        System.assertEquals(0, nslist.size());
    }
}