/**
 * @author ULiT
 * @date 27-08-2021
 * @description Batch class to Fill in mass the move contract discount fields of all the contracts/discounts
 * respecting the defined criteria (BU/Year/Record Type/Status/Condition tariffaire)
******************************************************************************************************************************************* */
public with sharing class MassPrepareMoveConditionBatch implements Database.Stateful, Database.Batchable<SObject> {

    private ULit_Negoptim.NegoptimBatch nb;
    public final String query;
    private Map<String, ULiT_Negoptim__Contract_Discount_Move_Setup__c> conditionMoveSetupMap;
    private Set<Id> moveSetupIdSet;

    // Constructor.
    public MassPrepareMoveConditionBatch(String startedFrom, Map<String, ULiT_Negoptim__Contract_Discount_Move_Setup__c> conditionMoveSetupMap,
                                         Set<Id> tariffConditionSourceSet, Set<Integer> yearSet, Set<Id> buSet, Set<String> recordTypeSet, Set<String> statusSet)
    {
        this.nb = new ULit_Negoptim.NegoptimBatch(MassPrepareMoveConditionBatch.class.getName(), ULit_Negoptim.NegoptimBatch.BatchType.Stateful, startedFrom);
        this.moveSetupIdSet = new Set<Id>();
        String q = 'SELECT Id, ULiT_Negoptim__Contract__r.ULiT_Negoptim__Reference_Year__c, ULiT_Negoptim__Contract__r.ULiT_Negoptim__Contract_BU__c,';
        q += ' ULiT_Negoptim__Contract__r.RecordType.DeveloperName, ULiT_Negoptim__Contract__r.ULiT_Negoptim__Status__c, ULiT_Negoptim__Condition_Type__c';
        q += ' FROM ULiT_Negoptim__Contract_Discount__c';
        q += ' WHERE ULiT_Negoptim__Condition_Type__c IN (\'' + String.join(new List<Id>(tariffConditionSourceSet), '\', \'') + '\')';
        q += ' AND ULiT_Negoptim__Contract__r.ULiT_Negoptim__Reference_Year__c IN (' + String.join(new List<Integer>(yearSet), ', ') + ')';
        q += ' AND ULiT_Negoptim__Contract__r.ULiT_Negoptim__Contract_BU__c IN (\'' + String.join(new List<Id>(buSet), '\', \'') + '\')';
        q += ' AND ULiT_Negoptim__Contract__r.RecordType.DeveloperName IN (\'' + String.join(new List<String>(recordTypeSet), '\', \'') + '\')';
        q += ' AND ULiT_Negoptim__Contract__r.ULiT_Negoptim__Status__c IN (\'' + String.join(new List<String>(statusSet), '\', \'') + '\')';
        this.query = q;
        this.conditionMoveSetupMap = conditionMoveSetupMap;
        // push inputs to log
        this.nb.logParameter('tariffConditionSourceSet', tariffConditionSourceSet);
        this.nb.logParameter('yearSet', yearSet);
        this.nb.logParameter('buSet', buSet);
        this.nb.logParameter('recordTypeSet', recordTypeSet);
        this.nb.logParameter('statusSet', statusSet);
        this.nb.logParameter('query', this.query);
    }

    // Start method implementation.
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    // Execute method implementation.
    public void execute(Database.BatchableContext BC, List<ULit_Negoptim__Contract_Discount__c> scope) {
        System.SavePoint sp = Database.setSavepoint();
        try {
            Map<Id, ULiT_Negoptim__Contract_Discount__c> discountsToUpdateMap = new Map<Id, ULiT_Negoptim__Contract_Discount__c>();
            for (ULiT_Negoptim__Contract_Discount__c item : scope) {
                String key = Integer.valueOf(item.ULiT_Negoptim__Contract__r.ULiT_Negoptim__Reference_Year__c) + '' 
                            + item.ULiT_Negoptim__Contract__r.ULiT_Negoptim__Contract_BU__c + '' 
                            + item.ULiT_Negoptim__Contract__r.RecordType.DeveloperName + '' 
                            + item.ULiT_Negoptim__Contract__r.ULiT_Negoptim__Status__c + ''
                    		+ item.ULiT_Negoptim__Condition_Type__c;
                if (this.conditionMoveSetupMap.containsKey(key)) {
                    ULiT_Negoptim__Contract_Discount_Move_Setup__c moveSetup = this.conditionMoveSetupMap.get(key);
                    if (moveSetup.ULiT_Negoptim__Status__c == 'Run') {
                        item.ULit_Negoptim__Is_TargetToMove__c = true;
                        item.ULit_Negoptim__TargetToMove_Condition__c = moveSetup.ULiT_Negoptim__Tariff_Condition_Target__c;
                        item.ULit_Negoptim__TargetToMove_per__c = null;
                        item.ULit_Negoptim__TargetToMove_amt__c = null;
                        item.ULit_Negoptim__TargetToMove_U__c = null;
                        item.ULit_Negoptim__TargetToMove_Justif__c = moveSetup.ULiT_Negoptim__Comment__c;
                        discountsToUpdateMap.put(item.Id, item);
                        moveSetupIdSet.add(moveSetup.Id);
                    } else if (moveSetup.ULiT_Negoptim__Status__c == 'Cancel') {
                        item.ULit_Negoptim__Is_TargetToMove__c = false;
                        item.ULit_Negoptim__TargetToMove_Condition__c = null;
                        item.ULit_Negoptim__TargetToMove_per__c = null;
                        item.ULit_Negoptim__TargetToMove_amt__c = null;
                        item.ULit_Negoptim__TargetToMove_U__c = null;
                        item.ULit_Negoptim__TargetToMove_Justif__c = null;
                        discountsToUpdateMap.put(item.Id, item);
                        moveSetupIdSet.add(moveSetup.Id);
                    }
                }
            }
            if (!discountsToUpdateMap.isEmpty()) {
                // check security on update Contract_Discount__c fields
                String[] contractDiscountUpdateFields = new String[] {
                    SObjectType.ULiT_Negoptim__Contract_Discount__c.fields.ULit_Negoptim__Is_TargetToMove__c.Name,
                    SObjectType.ULiT_Negoptim__Contract_Discount__c.fields.ULit_Negoptim__TargetToMove_Condition__c.Name,
                    SObjectType.ULiT_Negoptim__Contract_Discount__c.fields.ULit_Negoptim__TargetToMove_per__c.Name,
                    SObjectType.ULiT_Negoptim__Contract_Discount__c.fields.ULit_Negoptim__TargetToMove_amt__c.Name,
                    SObjectType.ULiT_Negoptim__Contract_Discount__c.fields.ULit_Negoptim__TargetToMove_U__c.Name,
                    SObjectType.ULiT_Negoptim__Contract_Discount__c.fields.ULit_Negoptim__TargetToMove_Justif__c.Name
                };
                if (ULiT_Negoptim.NegoptimHelper.checkUpdatibility(ULiT_Negoptim__Contract_Discount__c.SObjectType, contractDiscountUpdateFields)) {
                    List<Database.SaveResult> results = Database.update(discountsToUpdateMap.values(), false);
                    for (Integer i = 0; i < results.size(); i++) {
                        Database.SaveResult result = results.get(i);
                        ULiT_Negoptim__Contract_Discount__c condition = discountsToUpdateMap.values().get(i);
                        if (!result.isSuccess()) {
                            String errorMessage = condition + '\n';
                            Database.Error[] errors = result.getErrors();
                            for (Database.Error err : errors) {
                                errorMessage += err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                            }
                            this.nb.logError(errorMessage);
                        }
                    }
                }
            }
        } catch(DMLException e) {
            Database.rollback(sp);
            this.nb.logError('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
        } catch(Exception e) {
            system.debug('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
            Database.rollback(sp);
            this.nb.logError(e);
        }
    }

    // Finish method implementation.
    public void finish(Database.BatchableContext BC) {
        if (!moveSetupIdSet.isEmpty()) {
            // Update Status to Applied when batch is finished
            List<ULiT_Negoptim__Contract_Discount_Move_Setup__c> moveSetupsToUpdate = new List<ULiT_Negoptim__Contract_Discount_Move_Setup__c>();
            for (ULiT_Negoptim__Contract_Discount_Move_Setup__c item : [SELECT Id, ULiT_Negoptim__Status__c FROM ULiT_Negoptim__Contract_Discount_Move_Setup__c WHERE Id IN :moveSetupIdSet]) {
                if (item.ULiT_Negoptim__Status__c == 'Run') {
                    item.ULiT_Negoptim__Status__c = 'Applied';
                    this.nb.logMessage('Move Setup {' + item.Id + '} is applied');
                } else if (item.ULiT_Negoptim__Status__c == 'Cancel') {
                    item.ULiT_Negoptim__Status__c = 'Ready to apply';
                    this.nb.logMessage('Move Setup {' + item.Id + '} is canceled');
                }
                moveSetupsToUpdate.add(item);
            }
            // check security on update Contract_Discount__c fields
            String[] moveSetupUpdateFields = new String[] {
                SObjectType.ULiT_Negoptim__Contract_Discount_Move_Setup__c.fields.ULiT_Negoptim__Status__c.Name
            };
            if (ULiT_Negoptim.NegoptimHelper.checkUpdatibility(ULiT_Negoptim__Contract_Discount_Move_Setup__c.SObjectType, moveSetupUpdateFields)) {
                List<Database.SaveResult> results = Database.update(moveSetupsToUpdate, false);
                for (Integer i = 0; i < results.size(); i++) {
                    Database.SaveResult result = results.get(i);
                    ULiT_Negoptim__Contract_Discount_Move_Setup__c moveSetup = moveSetupsToUpdate.get(i);
                    if (!result.isSuccess()) {
                        String errorMessage = moveSetup + '\n';
                        Database.Error[] errors = result.getErrors();
                        for (Database.Error err : errors) {
                            errorMessage += err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                        }
                        this.nb.logError(errorMessage);
                    }
                }
            }
        }
        // save log
        this.nb.saveLog(bc);
        // String customSubject = this.nb.getBatchName() + this.nb.getAsyncApexJob(bc).Status;
        this.nb.sendEmail(bc, null, MassPrepareMoveConditionBatch.class.getName());
    }
}