/**
 * @author ULiT
 * @description Publish Assortment Details comments to the respective NetPrice Statement Details
 * Assortment details and NetPrice statement details are matched by product, contract and promo detail (possible null value if no promo exist on product)
 * @date 25 May 2021
*/
global class PublishAssortmentDetailCommentsAction {
    
    @InvocableMethod(label = 'Publish ASD Comments to NPSD')
    global static void publishComments(List<Id> assortmentBUList) {
        System.SavePoint sp = Database.setSavepoint();
        try {
            String[] NPSDClientStatus = new String[] {
                'Rejected', 'Demand for explanation', null
            };
            Map<String, ULiT_Negoptim__NetPrice_Statement_Detail__c> netPriceStatementMap = new Map<String, ULiT_Negoptim__NetPrice_Statement_Detail__c>();
            List<ULiT_Negoptim__NetPrice_Statement_Detail__c> netPriceStatementDetailsToUpdate = new List<ULiT_Negoptim__NetPrice_Statement_Detail__c>();
            for (ULiT_Negoptim__NetPrice_Statement_Detail__c npsd : [SELECT Id, ULiT_Negoptim__Product__c, ULiT_Negoptim__Partner_Comment__c, ULiT_Negoptim__Client_Status__c,
                                                                     ULiT_Negoptim__NetPrice_Statement__r.ULiT_Negoptim__Contract__c, ULiT_Negoptim__Promo_Event_Detail__c
                                                                     FROM ULiT_Negoptim__NetPrice_Statement_Detail__c
                                                                     WHERE ULiT_Negoptim__NetPrice_Statement__c IN (SELECT Id FROM ULiT_Negoptim__NetPrice_Statement__c WHERE ULiT_Negoptim__Assortment_BU__c IN :assortmentBUList)])
            {
                String key = String.valueOf(npsd.ULiT_Negoptim__Product__c) + String.valueOf(npsd.ULiT_Negoptim__NetPrice_Statement__r.ULiT_Negoptim__Contract__c) + String.valueOf(npsd.ULiT_Negoptim__Promo_Event_Detail__c);
                netPriceStatementMap.put(key, npsd);
            }
            
            for (ULiT_Negoptim__Assortment_Detail__c asd : [SELECT Id, ULiT_Negoptim__Product__c, ULiT_Negoptim__Promo_Event_Detail__c, ULiT_Negoptim__Status__c,
                                                            ULiT_Negoptim__Contract_Nego_NetPrice_Gap_Comment__c, ULiT_Negoptim__Assortment_BU__c, ULiT_Negoptim__Assortment_BU__r.ULiT_Negoptim__Contract__c
                                                            FROM ULiT_Negoptim__Assortment_Detail__c
                                                            WHERE ULiT_Negoptim__Assortment_BU__c IN :assortmentBUList
                                                            AND ULiT_Negoptim__Version__c = NULL])
            {
                String key = String.valueOf(asd.ULiT_Negoptim__Product__c) + String.valueOf(asd.ULiT_Negoptim__Assortment_BU__r.ULiT_Negoptim__Contract__c) + String.valueOf(asd.ULiT_Negoptim__Promo_Event_Detail__c);
                if (netPriceStatementMap.containsKey(key)) {
                    netPriceStatementMap.get(key).ULiT_Negoptim__Partner_Comment__c = asd.ULiT_Negoptim__Contract_Nego_NetPrice_Gap_Comment__c;
                    netPriceStatementMap.get(key).ULiT_Negoptim__Client_Status__c= asd.ULiT_Negoptim__Status__c == 'Validated' ? 'Approved' :
                    NPSDClientStatus.contains(asd.ULiT_Negoptim__Status__c) ? asd.ULiT_Negoptim__Status__c : null;
                    netPriceStatementDetailsToUpdate.add(netPriceStatementMap.get(key));
                }
            }
            if (!netPriceStatementDetailsToUpdate.isEmpty()) {
                update netPriceStatementDetailsToUpdate;
            }
            
        } catch (Exception ex) {
            Database.rollback(sp);
            throw new HandledException(ex.getMessage());
        }
    }
}