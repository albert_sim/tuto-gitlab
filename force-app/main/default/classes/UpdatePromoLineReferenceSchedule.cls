/**
 * @author ULiT
 * @date 08-09-2021
 * @description Schedule class to schedule execution of UpdatePromoLineReferenceBatch
******************************************************************************************************************************************* */
public class UpdatePromoLineReferenceSchedule implements Schedulable {
    public UpdatePromoLineReferenceSchedule() {}
    
    public void execute(SchedulableContext sc) {
        UpdatePromoLineReferenceBatch b = new UpdatePromoLineReferenceBatch(UpdatePromoLineReferenceSchedule.class.getName());
        Database.executebatch(b);
    }
    
    public static void run(String expression) {
        String cronExpression = expression != null ? expression : '0 0 23 ? * * *';
        System.schedule(UpdatePromoLineReferenceSchedule.class.getName(), cronExpression, new UpdatePromoLineReferenceSchedule());
    }
}