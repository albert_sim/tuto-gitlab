@isTest
public class ProductCodeEANFlowFilterInvocable_Test {
    private static Product2 product;
    private static String codeEAN;
    private static Integer randomNumber;
    private static Integer year = System.today().year();
    static void init() {
        codeEAN = '11111111';
        ULiT_Negoptim__Orga_HE__c root = createRoot(true);
        ULiT_Negoptim__Orga_HE__c department = createDepartment(false, root.Id);
        department.Name = 'DPT';
        department.ULiT_Negoptim__Elt_Code__c = 'DPT Code';
        insert department;
        //Create category
        ULiT_Negoptim__Orga_HE__c section = createSection(false, department.Id);
        section.ULiT_Negoptim__Set_Product_link_level__c = true;
        section.ULiT_Negoptim__Set_Assortment_nego_level__c = true;
        insert section;

        // orga bu
        ULiT_Negoptim__Orga_BU__c buRegion = createBURegion(true, 'bu Region');
        ULiT_Negoptim__Sup_Supplier__c supplier = createSupplier(true, buRegion, true);
        product = createProduct(false, section.Id, supplier.Id);
        product.ULiT_Negoptim__Product_EAN__c = codeEAN;
        product.ULiT_Negoptim__Status__c = 'Open';
        List<RecordType> productRT = [SELECT Id FROM RecordType WHERE SobjectType = 'Product2' AND DeveloperName = 'Product'];
        product.RecordTypeId = productRT.get(0).Id;
        insert product;
    }
    @isTest
    static void test_case() {
        init();
        Test.startTest();
        List<ProductCodeEANFlowFilterInvocable.ResultWrapper> resWrapper = new List<ProductCodeEANFlowFilterInvocable.ResultWrapper>();
        resWrapper = ProductCodeEANFlowFilterInvocable.getProductByEAN(new List<String>{codeEAN});
        Test.stopTest();
        System.assertEquals(codeEAN, resWrapper.get(0).products.get(0).ULiT_Negoptim__Product_EAN__c);
    }
    // Department creation.
    public static ULiT_Negoptim__Orga_HE__c createRoot(Boolean doInsert) {
        String rdm = String.valueOf(getRandomNumber());
        ULiT_Negoptim__Orga_HE__c department = new ULiT_Negoptim__Orga_HE__c(Name = 'DPT ' + rdm, ULiT_Negoptim__Elt_Code__c = 'D' + rdm, ULiT_Negoptim__Level__c = 0,
                                               ULiT_Negoptim__Dispatch_Inv_Hierarchy_Starting_Point__c = false, ULiT_Negoptim__Purchases_DB_Upper_Starting_Point__c = true,
                                               ULiT_Negoptim__Set_Assortment_nego_level__c = true, ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULiT_Negoptim__Status__c = 'Active');
        if (doInsert) insert department;
        System.assert(true);
        return department;
    }
    // Department creation.
    private static ULiT_Negoptim__Orga_HE__c createDepartment(Boolean doInsert, Id parentId) {
        String rdm = String.valueOf(getRandomNumber());
        ULiT_Negoptim__Orga_HE__c department = new ULiT_Negoptim__Orga_HE__c(Name = 'DPT ' + rdm, ULiT_Negoptim__Elt_Code__c = 'D' + rdm, ULiT_Negoptim__Parent_Element__c = parentId, ULiT_Negoptim__Level__c = 1,
                                               ULiT_Negoptim__Dispatch_Inv_Hierarchy_Starting_Point__c = false, ULiT_Negoptim__Purchases_DB_Upper_Starting_Point__c = true,
                                               ULiT_Negoptim__Set_Assortment_nego_level__c = true, ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULiT_Negoptim__Status__c = 'Active');
        if (doInsert) insert department;
        System.assert(true);
        return department;
    }
    // Section creation.
    private static ULiT_Negoptim__Orga_HE__c createSection(Boolean doInsert, Id parentId) {
        String rdm = String.valueOf(getRandomNumber());
        ULiT_Negoptim__Orga_HE__c section = new ULiT_Negoptim__Orga_HE__c(Name = 'SEC ' + rdm, ULiT_Negoptim__Elt_Code__c = 'S' + rdm, ULiT_Negoptim__Parent_Element__c = parentId, ULiT_Negoptim__Level__c = 2,
                                            ULiT_Negoptim__Dispatch_Inv_Hierarchy_Starting_Point__c = true, ULiT_Negoptim__Purchases_DB_Upper_Starting_Point__c = false,
                                            ULiT_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULiT_Negoptim__Status__c = 'Active', ULiT_Negoptim__Set_Product_link_level__c = true);
        if (doInsert) insert section;
        System.assert(true);
        return section;
    }
    //create product
    private static Product2 createProduct(Boolean doInsert, Id sectionId, Id supplierId) {
        String rdm = String.valueOf(getRandomNumber());
        Product2 product = new Product2(Name = 'PRD ' + rdm, ProductCode = 'P' + rdm, ULiT_Negoptim__Product_EAN__c = '10000' + rdm,
                                        ULiT_Negoptim__Category__c = sectionId, ULiT_Negoptim__Product_MasterSupplier__c = supplierId, IsActive = true,
                                        ULiT_Negoptim__Ext_Id__c = '10000' + rdm + '_' + 'P' + rdm);
        if (doInsert) insert product;
        System.assert(true);
        return product;
    }
    //Create supplier
    public static ULiT_Negoptim__Sup_Supplier__c createSupplier(Boolean doInsert, ULiT_Negoptim__Orga_BU__c country, Boolean withNS) {
        String rdm = String.valueOf(getRandomNumber());
        ULiT_Negoptim__Sup_Supplier__c supplier = new ULiT_Negoptim__Sup_Supplier__c(Name = 'SUP ' + rdm, ULiT_Negoptim__Code__c = rdm, ULiT_Negoptim__Country_origin__c = country.Id, ULiT_Negoptim__Acc_Country__c = country.Name,
                                                                                     ULiT_Negoptim__Is_Default_NegoScope__c = withNS, ULiT_Negoptim__Acc_Address_External_Synchro__c = false, ULiT_Negoptim__Admin_Address_External_Synchro__c = false,
                                                                                     ULiT_Negoptim__Status__c = 'Active'/*, CurrencyIsoCode = currencyIsoCode*/);
        if (doInsert) insert supplier;
        System.assert(true);
        return supplier;
    }    
    //Create region
    public static ULiT_Negoptim__Orga_BU__c createBURegion(Boolean doInsert, String name) {
        ULiT_Negoptim__Orga_BU__c region = new ULiT_Negoptim__Orga_BU__c(Name = name, ULiT_Negoptim__BU_Code__c = name, ULiT_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULiT_Negoptim__Status__c = 'Open',
                                                                         ULiT_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                                                         ULiT_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                                                         ULiT_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                                                         ULiT_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                                                         ULiT_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                                                         ULiT_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                                                         ULiT_Negoptim__Set_BU_List_Entity__c = true,
                                                                         ULiT_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                                                         ULiT_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                                                         ULiT_Negoptim__Set_Contract_BU_Source__c = true,
                                                                         ULiT_Negoptim__Set_Contract_BU_Target__c = true,
                                                                         ULiT_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                                                         ULiT_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                                                         ULiT_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                                                         ULiT_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                                                         ULiT_Negoptim__Set_Invoice_Detail__c = true,
                                                                         ULiT_Negoptim__Set_Invoice_Payment__c = true,
                                                                         ULiT_Negoptim__Set_Invoice__c = true,
                                                                         ULiT_Negoptim__Set_Product_Listing__c = true,
                                                                         ULiT_Negoptim__Set_SF_Data_Collection__c = true,
                                                                         ULiT_Negoptim__Set_SF_Planning__c = true,
                                                                         ULiT_Negoptim__Set_Budget_Maker_Detail__c = true,
                                                                         ULiT_Negoptim__Set_Budget_Maker__c = true,
                                                                         ULiT_Negoptim__Set_NetPrice_Statement__c = true,
                                                                         ULiT_Negoptim__Set_PriceList_Statement__c = true);
        List<RecordType> rt = [SELECT Id FROM RecordType WHERE SobjectType = 'ULiT_Negoptim__Orga_BU__c' AND DeveloperName = 'Region'];
        region.RecordTypeId = rt.get(0).Id;
        if (doInsert) insert region;
        System.assert(true);
        return region;
    }
    private static Integer getRandomNumber() {
        if (randomNumber == null)
            randomNumber = (Integer)(Math.random()*9999);
        return randomNumber++;
    }
}