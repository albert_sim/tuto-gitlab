/**
 * @author ULIT
 * @date 18/12/2020
 * @description apex controller related to negOptim mass update delete LWC
 * */
public with sharing class MassUpdateDeleteCmpController {
    
    /** 
     * @description method to get all the object apinames, has aura enabled
     * @return List <string> 
     * */  
    @AuraEnabled(Cacheable = true)
    public static List<String> getAllObjectNames() {
        List<Schema.sObjectType> allObjects = Schema.getGlobalDescribe().values();
        List<String> objectList = new List<String>();
        for (sObjectType sObjType : allObjects) { 
            String name = sObjType.getDescribe().getName();
            // Exclude all the unwanted Sobjects e.g. History, Share etc..
            // && name.split('__').size() < 3
            if(sObjType.getDescribe().isCustom() && sObjType.getDescribe().isAccessible()){
                if ((!name.containsignorecase('history') && !name.containsignorecase('tag') && !name.containsignorecase('share') && !name.containsignorecase('feed')
                && !name.containsignorecase('ChangeEvent')) || name.toLowerCase().right(3) ==  '__c') {   
                    objectList.add(sObjType.getDescribe().getName());
                }
            }
        }
        objectList.addAll(new List<String>{'Account', 'Asset', 'Campaign', 'Case', 'Contact', 'ContentVersion', 'Contract', 'Dashboard',
        'Document', 'Group', 'Lead', 'Opportunity', 'Order', 'Product2', 'Report', 'Task', 'User', 'UserRole'});
        objectList.sort();
        return objectList;
    }

    /** 
     * @description method to get all fields of an object in JSON format, has aura enabled
     * @param String objectName : the name of object
     * @return String 
     * */  
    @AuraEnabled
    public static String getObjectFields(String objectName) {
        List<FieldWrapper> fwList = new List<FieldWrapper>();
        Map <String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Map <String, Schema.SObjectField> fieldMap = schemaMap.get(objectName).getDescribe().fields.getMap();
        for (Schema.SObjectField sfield : fieldMap.Values()) {
            Schema.describefieldresult dfield = sfield.getDescribe();
            if (dfield.isAccessible()) {
               FieldWrapper fw = new FieldWrapper(dfield.getName(), dfield.getType().name(), dfield.isUpdateable()); 
                fwList.add(fw); 
            }
        }
        return Json.serialize(fwList);
    }


    @AuraEnabled
    public static Map<String, String> getObjectRecordTypes (String objectName) {
        Map<String, String> recorTypesMap = new Map<String, String>();
        for (RecordType rt : [SELECT Id, DeveloperName from RecordType WHERE sObjectType = :objectName]) {
            recorTypesMap.put(rt.Id, rt.DeveloperName);
        }
        return recorTypesMap;
    }
    /** 
     * @description method to get the logs result after the completed batches, has aura enabled
     * @param String apexJobIds : the Ids of completed job
     * @return List<log__c> the list of logs related to the completed jobs 
     * */ 
    @AuraEnabled
    public static List<ULiT_Negoptim__log__c> getLogsDetails(String apexJobIds) {
        if (apexJobIds != null) {
            List<String> apexJobIdsList = (list<String>) json.deserialize (apexJobIds, List<String>.class);
            List<ULiT_Negoptim__log__c> logsDetails;
          /* String[] logDetailFields = new String[] {
                SObjectType.log__c.fields.ID.Name,
                SObjectType.log__c.fields.Status__c.Name,
                SObjectType.log__c.fields.Apex_Job_ID__c.Name,
                SObjectType.log__c.fields.TotalJobItems__c.Name,
                SObjectType.log__c.fields.JobItemsProcessed__c.Name,
                SObjectType.log__c.fields.Status_Detail__c.Name,
                SObjectType.log__c.fields.NumberOfErrors__c.Name,
                SObjectType.log__c.fields.NumberOfWarnings__c.Name
              };*/

           // if (NegoptimHelper.checkAccessibility(log__c.SObjectType, logDetailFields)) {
            logsDetails = [SELECT Id, Name, ULiT_Negoptim__Status__c, ULiT_Negoptim__TraceLog__c, ULiT_Negoptim__Apex_Job_ID__c, ULiT_Negoptim__TotalJobItems__c, 
            ULiT_Negoptim__JobItemsProcessed__c, ULiT_Negoptim__Status_Detail__c, ULiT_Negoptim__NumberOfErrors__c, 
            ULiT_Negoptim__NumberOfWarnings__c FROM ULiT_Negoptim__log__c WHERE ULiT_Negoptim__Apex_Job_ID__c IN :apexJobIdsList];
           // }
        return logsDetails;
        } else {
            return null;
        }
    }


    /** 
     * @description method to get the to first logs to be displayed, has aura enabled
     * @param String apexJobIds : the Ids of completed job
     * @return List<log__c> the list of logs related to the completed jobs 
     * */ 
    @AuraEnabled
    public static List<ULiT_Negoptim__log__c> getFirstLogs() {
            List<ULiT_Negoptim__log__c> logsDetails;
          /* String[] logDetailFields = new String[] {
                SObjectType.log__c.fields.ID.Name,
                SObjectType.log__c.fields.Status__c.Name,
                SObjectType.log__c.fields.Apex_Job_ID__c.Name,
                SObjectType.log__c.fields.TotalJobItems__c.Name,
                SObjectType.log__c.fields.JobItemsProcessed__c.Name,
                SObjectType.log__c.fields.Status_Detail__c.Name,
                SObjectType.log__c.fields.NumberOfErrors__c.Name,
                SObjectType.log__c.fields.NumberOfWarnings__c.Name
              };*/

           // if (NegoptimHelper.checkAccessibility(log__c.SObjectType, logDetailFields)) {
            logsDetails = [SELECT Id, Name, ULiT_Negoptim__Status__c, ULiT_Negoptim__Started_From__c, ULiT_Negoptim__TraceLog__c, ULiT_Negoptim__Apex_Job_ID__c, ULiT_Negoptim__TotalJobItems__c, 
                            ULiT_Negoptim__JobItemsProcessed__c, ULiT_Negoptim__Status_Detail__c, ULiT_Negoptim__NumberOfErrors__c, 
                            ULiT_Negoptim__NumberOfWarnings__c FROM ULiT_Negoptim__log__c WHERE ULiT_Negoptim__Started_From__c LIKE '%MassUpdateDeleteCmpController%' And OwnerId = :userinfo.getUserId() order by CreatedDate Desc limit 5];
           // }
        return logsDetails;
    }


/** 
     * @description method to monitor the batch detail (the execution process), has aura enabled
     * @param String batchIdList : the list of Ids for the running batches
     * @return List<AsyncApexJob> list of batches queried 
     * */ 
    @AuraEnabled
    public static List<AsyncApexJob> getBatchDetails(String batchIdList) {
        if (batchIdList != null) {
            List<String> batchIds = (List<String>) JSON.deserialize (batchIdList, List<String>.class);
            List<AsyncApexJob> batchDetail;
            batchDetail = [SELECT Id, CreatedDate, CreatedById, JobType, ApexClassId, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors, CompletedDate, MethodName,
                            ExtendedStatus, ParentJobId, LastProcessed, LastProcessedOffset FROM AsyncApexJob WHERE Id in :batchIds];
            return batchDetail;
        } 
        else return null;
    } 

     /** 
     * @description method for execution of batch (delete/update) , has aura enabled
     * @param String objName, String query (to filter records), String type (update/delete), String fieldUpdate(fields with their new Values)
     * @return batchId 
     * */ 
    @AuraEnabled
    public static String executeUpdateDelete (String objName, String filter, String mode, String fieldUpdate, Integer batchSize) { 
        Id batchId = null;
        try { 
            if (filter != '') {
               String userQuery = 'SELECT Id FROM ' + objName + ' WHERE ' + filter;
               Database.query(userQuery + ' LIMIT 0');
            }
            //batchId = NegoptimBatch.executeBatch(new MassUpdateDeleteBatch(MassUpdateDeleteCmpController.class.getName(), query, objName, type, fieldUpdate), 200);
            batchId = Database.executeBatch(new MassUpdateDeleteBatch(MassUpdateDeleteCmpController.class.getName(), filter, objName, mode, fieldUpdate), batchSize);
        } catch (DmlException ex) {
            throw new AuraHandledException(ex.getDmlMessage(0));
        } catch (AuraHandledException e) {
            throw new AuraHandledException(e.getMessage());
        } catch (Exception e) {
            system.debug('ERROR ' + e.getMessage());
            throw new AuraHandledException(e.getMessage());
        }
        return batchId;
    }

     /** 
     * @description method to monitor the status of batch , has aura enabled
     * @param apexJobId : id of the apex job 
     * @return string asynApexJob.Status;
     * */ 
    @AuraEnabled
    public static String getStatusOfBatch(String apexJobId) {
       AsyncApexJob asynApexJob;
    //    String[] asyncApexJobFields = new String[] {
    //         SObjectType.AsyncApexJob.fields.Id.Name,
    //         SObjectType.AsyncApexJob.fields.Status.Name,
    //         SObjectType.AsyncApexJob.fields.ApexClassId.Name,
    //         SObjectType.AsyncApexJob.fields.CreatedDate.Name
    //    };
    //    String[] apexClassFields = new String[] {
    //         SObjectType.ApexClass.fields.Name.Name
    //    };
      // if (NegoptimHelper.checkAccessibility(AsyncApexJob.SObjectType, asyncApexJobFields)) {
        asynApexJob = [SELECT Id, Status FROM AsyncApexJob WHERE Id = :apexJobId  ORDER BY CreatedDate DESC LIMIT 1];
      // }
       return asynApexJob.Status;
   }
   @AuraEnabled
   public static String getCountOfTargetedRecords(String objName, String filter) {
       System.debug('inside count ' + filter);
      String query = 'SELECT count(Id) FROM '+ objName + ' WHERE ' + filter;
      String count = '0';
         try {
            List<sObject> result = Database.query(query);
            if(result != null && !result.isEmpty() && result[0].get('expr0') != null) {
                count = String.valueOf(result[0].get('expr0'));
            }
        } catch (DmlException ex) {
            System.debug(ex.getMessage() + '  ' + ex.getLineNumber());
        } catch (Exception ex) {
            System.debug(ex.getMessage() + '  ' + ex.getLineNumber());
        } 
      return count;
  }
}