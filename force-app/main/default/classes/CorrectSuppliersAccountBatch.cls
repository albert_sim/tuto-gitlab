/**
 * @author ULiT
 * @date 11-06-2021
 * @description NOT USED
******************************************************************************************************************************************* */
global with sharing class CorrectSuppliersAccountBatch implements Database.Batchable<SObject> {
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'SELECT Id, Name, CurrencyIsoCode, ULiT_Negoptim__Account__r.Name FROM ULiT_Negoptim__Sup_Supplier__c';
        query += ' WHERE CreatedBy.Name LIKE \'%Boomi%\' AND ULiT_Negoptim__Account__c <> NULL';
        return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext bc, List<ULiT_Negoptim__Sup_Supplier__c> scope) {
        List<ULiT_Negoptim__Sup_Supplier__c> supplierListToUpdate = new List<ULiT_Negoptim__Sup_Supplier__c>();
        Map<Id, Account> accountMap = new Map<Id, Account>();
        for (ULiT_Negoptim__Sup_Supplier__c item : scope) {
            if (item.Name != item.ULiT_Negoptim__Account__r.Name) {
                supplierListToUpdate.add(item);
                Account account = new Account();
                account.CurrencyIsoCode = item.CurrencyIsoCode;
                account.Name = item.Name;
                accountMap.put(item.Id, account);
            }
        }
        List<Database.SaveResult> saveResultList = Database.insert(accountMap.values());
        for (ULiT_Negoptim__Sup_Supplier__c item : supplierListToUpdate) {
            if (accountMap.get(item.Id).Id != null) {
                item.ULiT_Negoptim__Account__c = accountMap.get(item.Id).Id;
            }
        }
        update supplierListToUpdate;
    }
    
    global void finish(Database.BatchableContext bc) {}
}