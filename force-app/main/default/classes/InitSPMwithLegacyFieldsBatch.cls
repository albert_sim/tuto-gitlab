/**
 * @author ULIT
Database.executeBatch(new InitSPMwithLegacyFieldsBatch('Dev Console', null, null), 10);
**/
public class InitSPMwithLegacyFieldsBatch implements Database.Batchable<SObject> {

    private ULit_Negoptim.NegoptimBatch nb;
    private Id batchId;
    public final String query;
    
    public InitSPMwithLegacyFieldsBatch(String startedFrom, Set<Id> setIds, Set<Id> batchIds) {
        this.nb = new ULit_Negoptim.NegoptimBatch(InitSPMwithLegacyFieldsBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateless, startedFrom);
        this.batchId = new List<Id> (batchIds)[0];
        String q = 'SELECT Id, SPITM_Batch__c, ';
               q += 'ULiT_Negoptim__Tax_1_New__c, SPITM_Tax_1_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_2_New__c, SPITM_Tax_2_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_3_New__c, SPITM_Tax_3_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_4_New__c, SPITM_Tax_4_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_5_New__c, SPITM_Tax_5_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_6_New__c, SPITM_Tax_6_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_7_New__c, SPITM_Tax_7_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_8_New__c, SPITM_Tax_8_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_9_New__c, SPITM_Tax_9_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_10_New__c, SPITM_Tax_10_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_11_New__c, SPITM_Tax_11_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_12_New__c, SPITM_Tax_12_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_13_New__c, SPITM_Tax_13_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_14_New__c, SPITM_Tax_14_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_15_New__c, SPITM_Tax_15_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_16_New__c, SPITM_Tax_16_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_17_New__c, SPITM_Tax_17_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_18_New__c, SPITM_Tax_18_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_19_New__c, SPITM_Tax_19_Legacy__c, ';
               q += 'ULiT_Negoptim__Tax_20_New__c, SPITM_Tax_20_Legacy__c, ';
               q += 'ULiT_Negoptim__Actives_Taxes_New__c, SPITM_Actives_Taxes_Legacy__c, ';
               q += 'ULiT_Negoptim__Discount_Taxes_New__c, SPITM_Discount_Taxes_Legacy__c, ';
               q += 'ULiT_Negoptim__NetPrice_Taxes_New__c, SPITM_NetPrices_Taxes_Legacy__c, ';
               q += 'ULiT_Negoptim__Last_Net_Price__c, SPITM_Last_Net_Price_Legacy__c, ';
               q += 'ULiT_Negoptim__Last_Gross_Price__c, SPITM_Last_Gross_Price_Legacy__c';
               q += ' FROM ULiT_Negoptim__Supplier_PG_Mapping__c';
               q += ' WHERE SPITM_Batch__c = Null';

        if (setIds != null && !setIds.isEmpty()) {
            q += ' AND Id IN (\'' + String.join(new List<Id>(setIds), '\',\'') +'\')';
        }
        this.query = q;
        nb.logParameter('setIds', setIds);
        nb.logParameter('query', this.query);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    // Execute method implementation.
    public void execute(Database.BatchableContext BC, List<ULiT_Negoptim__Supplier_PG_Mapping__c> scope) {
        for (ULiT_Negoptim__Supplier_PG_Mapping__c item : scope) {
            item.ULiT_Negoptim__Tax_1_New__c = item.SPITM_Tax_1_Legacy__c;
            item.ULiT_Negoptim__Tax_2_New__c = item.SPITM_Tax_2_Legacy__c;
            item.ULiT_Negoptim__Tax_3_New__c = item.SPITM_Tax_3_Legacy__c;
            item.ULiT_Negoptim__Tax_4_New__c = item.SPITM_Tax_4_Legacy__c;
            item.ULiT_Negoptim__Tax_5_New__c = item.SPITM_Tax_5_Legacy__c;
            item.ULiT_Negoptim__Tax_6_New__c = item.SPITM_Tax_6_Legacy__c;
            item.ULiT_Negoptim__Tax_7_New__c = item.SPITM_Tax_7_Legacy__c;
            item.ULiT_Negoptim__Tax_8_New__c = item.SPITM_Tax_8_Legacy__c;
            item.ULiT_Negoptim__Tax_9_New__c = item.SPITM_Tax_9_Legacy__c;
            item.ULiT_Negoptim__Tax_10_New__c = item.SPITM_Tax_10_Legacy__c;
            item.ULiT_Negoptim__Tax_11_New__c = item.SPITM_Tax_11_Legacy__c;
            item.ULiT_Negoptim__Tax_12_New__c = item.SPITM_Tax_12_Legacy__c;
            item.ULiT_Negoptim__Tax_13_New__c = item.SPITM_Tax_13_Legacy__c;
            item.ULiT_Negoptim__Tax_14_New__c = item.SPITM_Tax_14_Legacy__c;
            item.ULiT_Negoptim__Tax_15_New__c = item.SPITM_Tax_15_Legacy__c;
            item.ULiT_Negoptim__Tax_16_New__c = item.SPITM_Tax_16_Legacy__c;
            item.ULiT_Negoptim__Tax_17_New__c = item.SPITM_Tax_17_Legacy__c;
            item.ULiT_Negoptim__Tax_18_New__c = item.SPITM_Tax_18_Legacy__c;
            item.ULiT_Negoptim__Tax_19_New__c = item.SPITM_Tax_19_Legacy__c;
            item.ULiT_Negoptim__Tax_20_New__c = item.SPITM_Tax_20_Legacy__c;
            item.ULiT_Negoptim__Actives_Taxes_New__c = item.SPITM_Actives_Taxes_Legacy__c;
            item.ULiT_Negoptim__Discount_Taxes_New__c = item.SPITM_Discount_Taxes_Legacy__c;
            item.ULiT_Negoptim__Last_Gross_Price__c = item.SPITM_Last_Gross_Price_Legacy__c;
            item.ULiT_Negoptim__Last_Net_Price__c = item.SPITM_Last_Net_Price_Legacy__c;
            item.ULiT_Negoptim__NetPrice_Taxes_New__c = item.SPITM_NetPrices_Taxes_Legacy__c;
            item.SPITM_Batch__c = batchId;
        }
        
        List<Database.SaveResult> results = Database.update(scope, false);
        for (Integer i = 0; i < results.size(); i++) {
            Database.SaveResult result = results.get(i);
            if (!result.isSuccess()) {
                String errorMessage = 'Supplier PG Mapping: ' + '\n'; Database.Error[] errors = result.getErrors();
                for (Database.Error err : errors) errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n'; nb.logError(errorMessage);
            }
        } nb.saveLog(bc);
    }
    
    public void finish(Database.BatchableContext BC) {
        ULiT_Negoptim__Batch__c batchToUpdated = [SELECT Id FROM ULiT_Negoptim__Batch__c WHERE Id = :batchId][0];
        batchToUpdated.ULiT_Negoptim__Status__c = 'Completed';
        UPDATE batchToUpdated;
        nb.sendEmail(bc, null, 'InitSPMwithLegacyFieldsBatch');
    }
}