/**
 * @author ULiT
 * @date 24/08/2021
 * @description a class that contains invocable method returning Product by their EAN, to be called in flows 
 **/
public class ProductCodeEANFlowFilterInvocable {
    @InvocableMethod(label='Get Product By EAN' description='Get Product By EAN')
    public static List<ResultWrapper> getProductByEAN(List<String> eanList) {
        List<Product2> returnedProductList = new List<Product2>();
        if (!eanList.isEmpty()) {
            String eanCode = eanList.get(0);
            if(!String.isBlank(eanCode)){
            returnedProductList = [SELECT Id, Name, RecordTypeId, ULiT_Negoptim__Status__c, ULiT_Negoptim__Product_EAN__c
                                   FROM Product2 
                                   WHERE ULiT_Negoptim__Product_EAN__c =:eanCode 
                                   AND RecordType.developerName In('Product','Unit_Need','Unit_Need_Initialization') 
                                   AND ULiT_Negoptim__Status__c = 'Open'];
            }
        }
        return !returnedProductList.isEmpty() ? new List<ResultWrapper>{new ResultWrapper(returnedProductList)} : new List<ResultWrapper>();
    }
    //wrapper for output
    public class ResultWrapper {
        @InvocableVariable(label='products' description='products')
        public List<Product2> products;
        
        public ResultWrapper(List<Product2> products) {
            this.products = products;
        }
    }
}