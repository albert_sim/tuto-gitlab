/**
 * @author ULiT
 * @date 11-03-2021
 * @description Generate priceList Statement, Tax Statement and priceList Statement Detail
******************************************************************************************************************************************* */
public with sharing class PriceListPGSynchroBatch implements Database.Batchable<SObject>, Database.Stateful {

    private ULiT_Negoptim.NegoptimBatch nb;
    private String query;
    private Set<Id> updatedNSIdSet = new Set<Id>();

    public PriceListPGSynchroBatch(String startedFrom, Set<Id> priceListStatementIds, Set<Id> priceListStatementDetailIds) {
        nb = new ULiT_Negoptim.NegoptimBatch(PriceListPGSynchroBatch.class.getName(), ULiT_Negoptim.NegoptimBatch.BatchType.Stateful , startedFrom);
        this.query = 'SELECT Id, ULiT_Negoptim__Supplier_Product__c, ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Nego_Scope__c, ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Nego_Scope__r.ULiT_Negoptim__Active_Taxes_New__c,';
        this.query += ' ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Orga_BU__c, ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Reference_Year__c, ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Active_Taxes__c,';
        this.query += ' ULiT_Negoptim__CostPrice_New__c, ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__TarifCost_Date__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__TarifCost_Date__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Last_Gross_Price__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Actives_Taxes_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Discount_Taxes_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__NetPrice_Taxes_New__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_1_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_2_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_3_New__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_4_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_5_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_6_New__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_7_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_8_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_9_New__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_10_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_11_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_12_New__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_13_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_14_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_15_New__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_16_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_17_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_18_New__c,';
        this.query += ' ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_19_New__c, ULiT_Negoptim__Supplier_Product__r.ULiT_Negoptim__Tax_20_New__c,';
        this.query += ' ULiT_Negoptim__Tax_1__c, ULiT_Negoptim__Tax_2__c, ULiT_Negoptim__Tax_3__c, ULiT_Negoptim__Tax_4__c, ULiT_Negoptim__Tax_5__c, ULiT_Negoptim__Tax_6__c, ULiT_Negoptim__Tax_7__c, ULiT_Negoptim__Tax_8__c, ULiT_Negoptim__Tax_9__c, ULiT_Negoptim__Tax_10__c,';
        this.query += ' ULiT_Negoptim__Tax_11__c, ULiT_Negoptim__Tax_12__c, ULiT_Negoptim__Tax_13__c, ULiT_Negoptim__Tax_14__c, ULiT_Negoptim__Tax_15__c, ULiT_Negoptim__Tax_16__c, ULiT_Negoptim__Tax_17__c, ULiT_Negoptim__Tax_18__c, ULiT_Negoptim__Tax_19__c, ULiT_Negoptim__Tax_20__c,';
        this.query += ' ULiT_Negoptim__Active_Taxes__c, ULiT_Negoptim__Discount_Taxes__c, ULiT_Negoptim__NetPrice_Taxes__c';
        this.query += ' FROM ULiT_Negoptim__Price_List_Statement_Detail__c';
        this.query += ' WHERE ULiT_Negoptim__Supplier_Product__c <> NULL AND ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Nego_Scope__c <> NULL';
        this.query += ' AND ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Orga_BU__c <> NULL AND ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Reference_Year__c <> NULL';
        if(priceListStatementIds != null && priceListStatementIds.size() > 0) {
            this.query += ' AND ULiT_Negoptim__PriceList_Statement__c IN (\'' + String.join(new List<Id>(priceListStatementIds), '\', \'') + '\')';
        }
        if(priceListStatementDetailIds != null && priceListStatementDetailIds.size() > 0) {
            this.query += ' AND Id IN (\'' + String.join(new List<Id>(priceListStatementDetailIds), '\', \'') + '\')';
        }
        
        nb.logParameter('priceListStatementIds', priceListStatementIds);
        nb.logParameter('priceListStatementDetailIds', priceListStatementDetailIds);
        nb.logParameter('query', query);
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext bc, List<ULiT_Negoptim__Price_List_Statement_Detail__c> scope) {
        List<ULiT_Negoptim__Supplier_PG_Mapping__c> pgListToUpdate = new List<ULiT_Negoptim__Supplier_PG_Mapping__c>();
        List<ULiT_Negoptim__Sup_Sup_NegoScope__c> nsToUpdate = new List<ULiT_Negoptim__Sup_Sup_NegoScope__c>();
        System.SavePoint sp = Database.setSavepoint();
        try {
            for (ULiT_Negoptim__Price_List_Statement_Detail__c item : scope) {
                ULiT_Negoptim__Supplier_PG_Mapping__c pg = item.ULiT_Negoptim__Supplier_Product__r;
                               
                pg.ULiT_Negoptim__Actives_Taxes_New__c = item.ULiT_Negoptim__Active_Taxes__c;
                pg.ULiT_Negoptim__Discount_Taxes_New__c = item.ULiT_Negoptim__Discount_Taxes__c;
                pg.ULiT_Negoptim__NetPrice_Taxes_New__c = item.ULiT_Negoptim__NetPrice_Taxes__c;
                
                for (Integer i = 1; i <= 20; i++) {
                    pg.put('ULiT_Negoptim__Tax_' + i + '_New__c', item.get('ULiT_Negoptim__Tax_' + i + '__c'));
                }

                pgListToUpdate.add(pg);
                
                if (!updatedNSIdSet.contains(item.ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Nego_Scope__c)) {
                    ULiT_Negoptim__Sup_Sup_NegoScope__c ns  = item.ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Nego_Scope__r;
                    ns.ULiT_Negoptim__Active_Taxes_New__c = item.ULiT_Negoptim__PriceList_Statement__r.ULiT_Negoptim__Active_Taxes__c;
                    
                    updatedNSIdSet.add(ns.Id);
                    nsToUpdate.add(ns);
                }
            }
            if (!pgListToUpdate.isEmpty()) {
                // check security on update Supplier_PG_Mapping__c fields
                String[] PGFields = new String[] {
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__TarifCost_Date__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Last_Gross_Price__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Actives_Taxes_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Discount_Taxes_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__NetPrice_Taxes_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_1_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_2_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_3_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_4_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_5_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_6_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_7_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_8_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_9_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_10_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_11_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_12_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_13_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_14_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_15_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_16_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_17_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_18_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_19_New__c.Name,
                    SObjectType.ULiT_Negoptim__Supplier_PG_Mapping__c.fields.ULiT_Negoptim__Tax_20_New__c.Name
                };
                if (ULiT_Negoptim.NegoptimHelper.checkUpdatibility(ULiT_Negoptim__Supplier_PG_Mapping__c.SObjectType, PGFields)) {
                    Database.update(pgListToUpdate);
                }
            }
            
            if (!nsToUpdate.isEmpty()) {
                String[] nsUpdateFields = new String[] {
                    SObjectType.ULiT_Negoptim__Sup_Sup_NegoScope__c.fields.ULiT_Negoptim__Active_Taxes_New__c.Name
                };
                if (ULiT_Negoptim.NegoptimHelper.checkUpdatibility(ULiT_Negoptim__Sup_Sup_NegoScope__c.SObjectType, nsUpdateFields)) {
                    List<Database.SaveResult> results = Database.update(nsToUpdate);
                    this.nb.logResults(results, nsToUpdate);
                }
            }
        } catch(DMLException e) {
            Database.rollback(sp);
            nb.logError('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
        } catch(Exception e) {
            system.debug('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
            Database.rollback(sp);
            nb.logError(e);
        }
        nb.incrementBatchIndex();
    }
    
    public void finish(Database.BatchableContext bc) {
        AsyncApexJob asyncApexJob = [SELECT Status FROM AsyncApexJob WHERE Id = :bc.getJobId()];
        String customSubject = PriceListPGSynchroBatch.class.getName() + ': ' + asyncApexJob.Status;
        nb.sendEmail(bc, null, customSubject);
    }
}