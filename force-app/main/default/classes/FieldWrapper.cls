public with sharing class FieldWrapper {
    public String fieldName;
    public String fieldType;
    public String value;
    public Boolean isUpdateable;
    public FieldWrapper(String name, String type, Boolean isUpdateable) {
        this.fieldName = name;
        this.fieldType = type;
        this.isUpdateable = isUpdateable;
    }
    public FieldWrapper(String name, String type, String value) {
        this.fieldName = name;
        this.fieldType = type;
        this.value = value;
    }
}