public class RefreshContractDiscountOrderBatch implements Database.Batchable<SObject>, Database.Stateful {
    public final String query;
    
    public RefreshContractDiscountOrderBatch(List<Id> contractIds, String order) {
        order = order == null ? 'ASC' : order;
        String q = 'SELECT Id,';
        q += ' (SELECT Id, Name, ULiT_Negoptim__Index__c, ULiT_Negoptim__Nego_Discount_Type__c, ULiT_Negoptim__Rank__c, ULiT_Negoptim__Display_Order__c FROM ULiT_Negoptim__Contract_Discounts__r ORDER BY ULiT_Negoptim__Index__c, ULiT_Negoptim__Nego_Discount_Type__c, ULiT_Negoptim__Rank__c, ULiT_Negoptim__Condition_Type__r.Name ' + order + ' )';
        q += ' FROM ULiT_Negoptim__Contract__c';
        if(contractIds != null && contractIds.size() > 0) {
            q += ' WHERE Id IN (\'' + String.join(contractIds, '\',\'') +'\')';
        }
        this.query = q;
    }
    
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query);
    }
    // Execute method.
    public void execute(Database.BatchableContext bc, List<ULiT_Negoptim__Contract__c> scope) {
        List<ULiT_Negoptim__Contract_Discount__c> discountsToUpdate = new List<ULiT_Negoptim__Contract_Discount__c>();
        for (ULiT_Negoptim__Contract__c contract : scope) {
            List<ULiT_Negoptim__Contract_Discount__c> discounts = contract.ULiT_Negoptim__Contract_Discounts__r;
            for (Integer i = 0; i < discounts.size(); i++) {
                Integer j = i+1;
                ULiT_Negoptim__Contract_Discount__c item = discounts.get(i);
                if (item.ULiT_Negoptim__Display_Order__c != j) {
                    item.ULiT_Negoptim__Display_Order__c = j;
                    discountsToUpdate.add(item); 
                }
            }
        }
        UPDATE discountsToUpdate;
    }
    
    public void finish(Database.BatchableContext bc) {
        //
    }
}