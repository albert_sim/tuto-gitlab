/**
 * @author ULiT
 * @date 4/10/2021
 * @description a class that contains invocable to call the future method that assigns user to a group
 **/

public class CommunityUserGroupInsert {
    @InvocableMethod(label='Insert Group Member' description='Insert Group Member')
    public static void insertGroupMember(List<InputWrapper> inputFields) {
        futureGroupMemberAttribution(inputFields.get(0).userId, inputFields.get(0).groupId);
    }
    
    //future method to assign user to a group
    @future
    public static void futureGroupMemberAttribution(String userId, string groupId) {   
        GroupMember groupmember = new GroupMember();
        groupmember.GroupId = groupId;
        groupmember.UserOrGroupId = userId;
        insert groupmember;
        system.debug('>>> ' + JSON.serialize(groupmember));
    }
   
    //wrapper for the inputs variable from the flow
    public class InputWrapper {
        @InvocableVariable(label='UserId' description='UserId')
        public String userId;
        
        @InvocableVariable(label='GroupId' description='GroupId')
        public String groupId;
        
        public InputWrapper(String userId, String groupId) {
            this.userId = userId;
            this.groupId = groupId;
        }
        public InputWrapper() {
        }
    }
    
}