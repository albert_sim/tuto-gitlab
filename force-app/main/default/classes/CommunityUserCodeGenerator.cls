public class CommunityUserCodeGenerator {
    
    @InvocableMethod(label='Generate Unique Code' description='Generate Unique Code')
    public static List<ResultWrapper> generateUniqueCode() {
        Blob key = Crypto.generateAesKey(128);
        String uniqueString = EncodingUtil.convertToHex(key);
        String hex = uniqueString.substring(0, 8);
        return new List<ResultWrapper> {
        	new ResultWrapper(hex)
        };
    }
    public class ResultWrapper {
        public ResultWrapper(String hex) {
            nickName = hex;
        }
        @InvocableVariable(label='result' description='result')
        public String nickName;
    }
}