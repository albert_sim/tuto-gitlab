public class DeleteAllContractDiscountBatch implements Database.Batchable<ULiT_Negoptim__Contract_Discount__c>, Database.Stateful {
    
    public DeleteAllContractDiscountBatch() {
        
    }
    
    public Iterable<ULiT_Negoptim__Contract_Discount__c > start(Database.BatchableContext bc) {
        return [SELECT Id FROM ULiT_Negoptim__Contract_Discount__c LIMIT 50000];
    }
    
    public void execute(Database.BatchableContext bc, List<ULiT_Negoptim__Contract_Discount__c > scope) {
        delete scope;
    }
    
    public void finish(Database.BatchableContext bc) {
        
    }
}