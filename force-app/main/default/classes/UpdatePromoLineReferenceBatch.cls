/**
 * @author ULiT
 * @date 08-09-2021
 * @description Batch to Generate promo line at the Product level when Promo line is created with HeigherLever Packaging product
******************************************************************************************************************************************* */
public class UpdatePromoLineReferenceBatch implements Database.Batchable<sObject> {
    private ULiT_Negoptim.NegoptimBatch nb;
    public String query;
    private List<String> promoDetailsFields;
    
    public UpdatePromoLineReferenceBatch(String startedFrom) {
        this.nb = new ULit_Negoptim.NegoptimBatch(UpdatePromoLineReferenceBatch.class.getName(), ULit_Negoptim.NegoptimBatch.BatchType.stateful, startedFrom);
        this.promoDetailsFields = new List<String>();
        Map<String, Schema.SObjectField> promoDetailsFieldsMap = Schema.SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.getMap();
        for(String fieldName : promoDetailsFieldsMap.keySet()) {
            if(promoDetailsFieldsMap.get(fieldName).getDescribe().isUpdateable()) {
                this.promoDetailsFields.add(fieldName);
            }
        }
        String q = 'SELECT Id, ULiT_Negoptim__Commercial_Event__c, ULiT_Negoptim__Product__r.ULiT_Negoptim__Consumer_Trade_Item__c, ' + String.join(this.promoDetailsFields, ',') + '';
        q += ' FROM ULiT_Negoptim__Commercial_Plan_Detail__c';
        q += ' WHERE ULiT_Negoptim__Product__r.ULiT_Negoptim__Consumer_Trade_Item__c <> NULL';
        q += ' AND ULiT_Negoptim__Product__r.RecordType.DeveloperName = \'Higher_Level_Packaging\'';
        q += ' AND ULiT_Negoptim__Consumer_Trade_Item_Promo_Detail__c = NULL AND ULiT_Negoptim__Supplier__c <> NULL';
        ////q += ' AND ULiT_Negoptim__Prenego_envelop__c <> NULL';
        this.query = q;
        this.nb.logParameter('query', q);
    }
    
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }
    
    public void execute(Database.BatchableContext BC, List<ULiT_Negoptim__Commercial_Plan_Detail__c> scope) {
        System.SavePoint sp = Database.setSavepoint();
        try {
            Set<Id> commercialPlanIdSet = new Set<Id>();
            Set<Id> productIdSet = new Set<Id>();
            Set<Id> supplierIdSet = new Set<Id>();
            Set<Id> productConsumerTradeItemIdSet = new Set<Id>();
            List<ULiT_Negoptim__Commercial_Plan_Detail__c> updatedPromoDetails = new List<ULiT_Negoptim__Commercial_Plan_Detail__c>();
            Map<String, ULiT_Negoptim__Commercial_Plan_Detail__c> newParentPromoDetailsMap = new Map<String, ULiT_Negoptim__Commercial_Plan_Detail__c>();
            for (ULiT_Negoptim__Commercial_Plan_Detail__c item : scope) {
                commercialPlanIdSet.add(item.ULiT_Negoptim__Commercial_Event__c);
                productIdSet.add(item.ULiT_Negoptim__Product__c);
                supplierIdSet.add(item.ULiT_Negoptim__Supplier__c);
                productConsumerTradeItemIdSet.add(item.ULiT_Negoptim__Product__r.ULiT_Negoptim__Consumer_Trade_Item__c);
            }
            // get parent promo details
            Map<String, ULiT_Negoptim__Commercial_Plan_Detail__c> parentPromoDetailsMap = new Map<String, ULiT_Negoptim__Commercial_Plan_Detail__c>();
            Map<String, ULiT_Negoptim__Supplier_PG_Mapping__c> pgProductMap = new Map<String, ULiT_Negoptim__Supplier_PG_Mapping__c>(); //key: product id +supplier id, value: pg
            if(!productConsumerTradeItemIdSet.isEmpty()) {
                for(ULiT_Negoptim__Commercial_Plan_Detail__c item : [SELECT Id, ULiT_Negoptim__Commercial_Event__c, ULiT_Negoptim__Prenego_envelop__c, ULiT_Negoptim__Supplier__c, ULiT_Negoptim__Product__c
                                                                     FROM ULiT_Negoptim__Commercial_Plan_Detail__c
                                                                     WHERE ULiT_Negoptim__Commercial_Event__c IN :commercialPlanIdSet
                                                                     AND ULiT_Negoptim__Supplier__c IN :supplierIdSet
                                                                     AND ULiT_Negoptim__Product__c IN :productConsumerTradeItemIdSet])
                {
                    parentPromoDetailsMap.put(item.ULiT_Negoptim__Commercial_Event__c + '' + item.ULiT_Negoptim__Supplier__c + '' + item.ULiT_Negoptim__Product__c, item);
                }
                List<ULiT_Negoptim__Supplier_PG_Mapping__c> pgList = [SELECT Id, ULiT_Negoptim__Supplier__c, ULiT_Negoptim__Product__c, ULiT_Negoptim__Last_Gross_Price__c
                                                                      FROM ULiT_Negoptim__Supplier_PG_Mapping__c
                                                                      WHERE ULiT_Negoptim__Product__c = :productConsumerTradeItemIdSet
                                                                      AND ULiT_Negoptim__Supplier__c = :supplierIdSet];
                if (pgList != null && !pgList.isEmpty()) {
                    for (ULiT_Negoptim__Supplier_PG_Mapping__c pgMapping : pgList) {
                        pgProductMap.put(pgMapping.ULiT_Negoptim__Supplier__c + '' + pgMapping.ULiT_Negoptim__Product__c, pgMapping);
                    }
                }
            }
            for (ULiT_Negoptim__Commercial_Plan_Detail__c item : scope) {
                String key = item.ULiT_Negoptim__Commercial_Event__c + '' + item.ULiT_Negoptim__Supplier__c + '' + item.ULiT_Negoptim__Product__r.ULiT_Negoptim__Consumer_Trade_Item__c;
                if(parentPromoDetailsMap.containsKey(key)) {
                    item.ULiT_Negoptim__Consumer_Trade_Item_Promo_Detail__c = parentPromoDetailsMap.get(key).Id;
                    updatedPromoDetails.add(item);
                } else if(!newParentPromoDetailsMap.containsKey(key)) {
                    // create new promo line
                    ULiT_Negoptim__Commercial_Plan_Detail__c newPromoDetail = item.clone(false);
                    Id productId = item.ULiT_Negoptim__Product__r.ULiT_Negoptim__Consumer_Trade_Item__c;
                    newPromoDetail.ULiT_Negoptim__Product__c = productId;
                    newPromoDetail.RecordTypeId = getObjectRecordTypeId(ULiT_Negoptim__Commercial_Plan_Detail__c.SObjectType, ('To_Be_Validated'));
                    newPromoDetail.ULiT_Negoptim__Supplier_Product__c = null;
                    newPromoDetail.ULiT_Negoptim__Regular_Purchase_Price__c = null;
                    newPromoDetail.ULiT_Negoptim__Regular_Lowered_Price_BeforePromoDisc__c = null;
                    newPromoDetail.ULiT_Negoptim__Discount_Average_Promo_Rate_Nego__c = null;
                    newPromoDetail.ULiT_Negoptim__Volume_Regular_Period_Reference__c = null;
                    newPromoDetail.ULiT_Negoptim__Volume_Period_Reference__c = null;
                    newPromoDetail.ULiT_Negoptim__Volume_Targeted__c = null;
                    ULiT_Negoptim__Supplier_PG_Mapping__c pgMapping = pgProductMap.containsKey(item.ULiT_Negoptim__Supplier__c + '' + productId) ? pgProductMap.get(item.ULiT_Negoptim__Supplier__c + '' + productId) : null;
                    if (pgMapping != null) {
                        newPromoDetail.ULiT_Negoptim__Supplier_Product__c = pgMapping.Id;
                        newPromoDetail.ULiT_Negoptim__Regular_Purchase_Price__c = pgMapping.ULiT_Negoptim__Last_Gross_Price__c;
                    }
                    newParentPromoDetailsMap.put(key, newPromoDetail);
                }
            }
            // check security on insert ULiT_Negoptim__Commercial_Plan_Detail__c fields
            List<String> promoDetailsInsertFields = new List<String> {
                SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.ULiT_Negoptim__Commercial_Event__c.Name
            };
            promoDetailsInsertFields.addAll(this.promoDetailsFields);
            if(!newParentPromoDetailsMap.isEmpty()) {
                if (ULiT_Negoptim.NegoptimHelper.checkCreatibility(ULiT_Negoptim__Commercial_Plan_Detail__c.SObjectType, promoDetailsInsertFields)) {
                    List<Database.SaveResult> results = Database.insert(newParentPromoDetailsMap.values(), false);
                    for (Integer i = 0; i < results.size(); i++) {
                        Database.SaveResult result = results.get(i);
                        ULiT_Negoptim__Commercial_Plan_Detail__c promoDetail = newParentPromoDetailsMap.values().get(i);
                        if (!result.isSuccess()) {
                            String errorMessage = promoDetail + '\n';
                            Database.Error[] errors = result.getErrors();
                            for (Database.Error err : errors) {
                                errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                            }
                            this.nb.logError(errorMessage);
                        }
                    }
                    for (ULiT_Negoptim__Commercial_Plan_Detail__c item : scope) {
                        String key = item.ULiT_Negoptim__Commercial_Event__c + '' + item.ULiT_Negoptim__Supplier__c + '' + item.ULiT_Negoptim__Product__r.ULiT_Negoptim__Consumer_Trade_Item__c;
                        if(newParentPromoDetailsMap.containsKey(key)) {
                            item.ULiT_Negoptim__Consumer_Trade_Item_Promo_Detail__c = newParentPromoDetailsMap.get(key).Id;
                            updatedPromoDetails.add(item);
                        }
                    }
                }
            }
            if(!updatedPromoDetails.isEmpty()) {
                if (ULiT_Negoptim.NegoptimHelper.checkUpdatibility(ULiT_Negoptim__Commercial_Plan_Detail__c.SObjectType, promoDetailsFields)) {
                    List<Database.SaveResult> results = Database.update(updatedPromoDetails, false);
                    for (Integer i = 0; i < results.size(); i++) {
                        Database.SaveResult result = results.get(i);
                        ULiT_Negoptim__Commercial_Plan_Detail__c promoDetail = updatedPromoDetails.get(i);
                        if (!result.isSuccess()) {
                            String errorMessage = promoDetail + '\n';
                            Database.Error[] errors = result.getErrors();
                            for (Database.Error err : errors) {
                                errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                            }
                            this.nb.logError(errorMessage);
                        }
                    }
                }
            }
        } catch(DMLException e) {
            Database.rollback(sp);
            nb.logError('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
        } catch(Exception e) {
            system.debug('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
            Database.rollback(sp);
            nb.logError(e);
        }
        // save log
        nb.saveLog(bc);
    }
    
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName) {
        // If sObjectType is wrong, then an Exception is thrown.
        Id rt;
        Map<Id, Schema.RecordTypeInfo> sObjectTypeRecordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosById();
        for (Id RTId : sObjectTypeRecordTypeInfo.keySet()) {
            if (sObjectTypeRecordTypeInfo.get(RTId).getDeveloperName() == recordTypeName) {
                rt = RTId;
            }
        }
        //Return the record type id
        return rt;
    }
    
    public void finish(Database.BatchableContext BC) {
        this.nb.sendEmail(BC, null, UpdatePromoLineReferenceBatch.class.getName());
    }
}