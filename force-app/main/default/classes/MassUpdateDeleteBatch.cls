/**
 * @description Batchable class used to Update/Delete many records
 * saves logs via NegoptimBatch
 **/
public with sharing  class MassUpdateDeleteBatch implements Database.Batchable<SObject>, Database.Stateful {
    private String query;
    public String errors;
    //private NegoptimBatch nb;
    public String objName;
    public String filterString;
    public String mode;
    public String fieldsToUpdate;
    public Integer batchSize;
    private MassUpdateDeleteLog massUpdateDeleteLog;

/**
 * @description Constructor
 * @params String startedFrom (class Name), String filter (Filter records), String objName, String type (Operation type: Delete/Update), String fieldsToUpdate (the updated values)
 **/
    public MassUpdateDeleteBatch(String startedFrom, String filter, String objName, String m, String fieldsToUpdate) {
        this.mode = m;
        this.errors = '';
        System.debug('started from ' + startedFrom);
        this.massUpdateDeleteLog = new MassUpdateDeleteLog(MassUpdateDeleteBatch.class.getName(), startedFrom + '_' + this.mode);
        this.massUpdateDeleteLog.logParameter('Operation', this.mode);
        this.query = 'SELECT Id FROM ' + objName;
        if (filter != '') {
            this.query += ' WHERE ' + filter; // the filter is the where clause passed as string from js
        }
        this.fieldsToUpdate = fieldsToUpdate;
        this.massUpdateDeleteLog.logParameter('Filter', filter);
        if (this.mode == 'Update') {
            String updates = '';
            List<FieldWrapper> updateRows = (List<FieldWrapper>) JSON.deserialize(fieldsToUpdate, List<FieldWrapper>.class);
            for (FieldWrapper updateRow : updateRows) {
                updates += 'Change: ' + updateRow.fieldName + ' To: ' + updateRow.value + '\n';
            }
            this.massUpdateDeleteLog.logParameter('Update', updates);
        }
        //nb.logParameter('query', this.query);
    }

    // Start method
    public Database.QueryLocator start(Database.BatchableContext bc) {
        return Database.getQueryLocator(query); 
    }
    
    // Execute method
    public void execute(Database.BatchableContext bc, List<SObject> scope) {
        Savepoint sp = Database.setSavepoint();
        try {
            if (this.mode == 'Delete') {
                List<Database.DeleteResult> result = Database.delete(scope);
                for (Database.DeleteResult sr : result) {
                    if (!sr.isSuccess()) {
                        // If Operation failed, so write all errors into log              
                        for(Database.Error err : sr.getErrors()) {
                            massUpdateDeleteLog.logError('Error: ' + err.getStatusCode() + ' - ' + err.getMessage());
                        }
                    }
                }
            }
            if (this.mode == 'Update') {
                List<FieldWrapper> fwList = (List<FieldWrapper>) JSON.deserialize(fieldsToUpdate, List<FieldWrapper>.class);
                System.debug(JSON.serialize(fwList));
                List<SObject> toUpdateObjects = new List<SObject>();
                for (SObject objToUpdate : scope) {
                    for (FieldWrapper fw : fwList) {
                        if (fw.value == 'NULL' || fw.value == 'null') {
                            objToUpdate.put(fw.fieldName, null);
                        } else {
                            if (fw.fieldType == 'CHECKBOX' || fw.fieldType == 'BOOLEAN') {
                                objToUpdate.put(fw.fieldName, Boolean.valueOf(fw.value));
                            } else if (fw.fieldType == 'DATE') {
                                objToUpdate.put(fw.fieldName, date.valueOf(fw.value));
                            } else if (fw.fieldType == 'DATETIME') {
                                objToUpdate.put(fw.fieldName, DateTime.valueOf(fw.value));
                            } else if (fw.fieldType == 'NUMBER') {
                                objToUpdate.put(fw.fieldName, Decimal.valueOf(fw.value));
                            } else {
                                objToUpdate.put(fw.fieldName, fw.value);
                            }
                        }
                    }
                    toUpdateObjects.add(objToUpdate);
                }
                system.debug('to update ::: '+ JSON.serialize(toUpdateObjects));

                List<Database.SaveResult> result = Database.update(toUpdateObjects);
                for (Database.SaveResult sr : result) {
                    if (!sr.isSuccess()) {
                        // If Operation failed, so write all errors into log              
                        for(Database.Error err : sr.getErrors()) {
                            System.debug('Exception: ' + err.getMessage());
                            massUpdateDeleteLog.logError('Error: ' + err.getStatusCode() + ' - ' + err.getMessage());
                        }
                    }
                }
            }
        } catch (DmlException ex) {
            Database.rollback(sp);
            system.debug('Exception: ' + ex.getMessage() + ' - ' + ex.getLineNumber());
            massUpdateDeleteLog.logError('Exception: ' + ex.getMessage() + ' - ' + ex.getLineNumber());
        } catch (Exception e) {
            Database.rollback(sp);
            system.debug('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
            massUpdateDeleteLog.logError('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
        }
        //nb.incrementBatchIndex();
    }

    // Finish method
    public void finish(Database.BatchableContext bc) {
        massUpdateDeleteLog.saveLog(bc);
      // massUpdateDeleteLog.sendEmail(bc, null, null);
    }
}