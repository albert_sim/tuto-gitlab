public with sharing class CommercialPlanDetailRFPController {
    
     /** COMMERCIAL PLAN / CONTRACT */
    public SObject record {get; set;}
    public String recordId {get; set;}
     public Boolean isFromRFX { get {
        return this.MASTER_OBJECT_NAME != null && this.MASTER_OBJECT_NAME == SObjectType.ULiT_Negoptim__Request_for_x__c.Name;
    } set; }
    public Boolean isFromCommercialPlanDetail { get {
        return this.MASTER_OBJECT_NAME != null && this.MASTER_OBJECT_NAME == SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.Name;
    } set; }
    private final String MASTER_OBJECT_NAME;
    // ApexPages.StandardSetController must be instantiated
    // for standard list controllers
    Public Integer size {get; set;} 
    Public Integer noOfRecords {get; set;} 
    public List<SelectOption> paginationSizeOptions {get; set;}
    
    //variables for filters
    public List<SelectOption> recordTypeList {get; set;}
    public String selectedType {get; set;}
    public List<SelectOption> statusList {get; set;}
    public String selectedStatus {get; set;}
    public List<SelectOption> riskLevelList {get; set;}
    public String selectedRiskLevel {get; set;}
    public Boolean ownLabelSupplierTrue {get; set;}
    public Boolean ownLabelSupplierFalse {get; set;}
    public Boolean isPreferredTrue {get; set;}
    public Boolean isPreferredFalse {get; set;}
    public Boolean myNegoSupp {get; set;}
    public List<SelectOption> countryOriginOptionList {get; set;}
    public List<SelectOption> selectedCountryOriginOptionList {get; set;}
    public String searchTermCountry {get; set;}
    public String selectedCountries { get; set; }
    public String searchTerm {get; set;}
    public ApexPages.StandardSetController setCtrlr {get; set;}
    public String commercialPlanDetailId;
    public String requestForX;
    public ULiT_Negoptim__Commercial_Plan_Detail__c cpd {get; set;}
    public ULiT_Negoptim__Request_for_x__c rfx {get; set;}
    public Map<Id, ULiT_Negoptim__Commercial_Plan_Detail_RFP__c> negoscopeRFPMap; // map<supplier Id, related rfp>
    Set<Id> initialLoadNegoscopesIds = new Set<Id>();
    Map<Id,ULiT_Negoptim__Sup_sup_NegoScope__c> nsMap = new Map<Id,ULiT_Negoptim__Sup_sup_NegoScope__c>(); 
    Set<NegoScopeWrapper> allNegoscopesSet = new Set<NegoScopeWrapper>();
    List<ULiT_Negoptim__Sup_sup_NegoScope__c> selectedNegoscopes = new List<ULiT_Negoptim__Sup_sup_NegoScope__c>();
    
    public CommercialPlanDetailRFPController() {
        this.recordId = ApexPages.currentPage().getParameters().get('id');
        if (this.recordId != null && !String.isBlank(this.recordId)) {
            try { 
                this.selectedType = '';
                size = 10;
                this.selectedCountryOriginOptionList = new List<SelectOption>();
                this.selectedCountries = ApexPages.CurrentPage().getParameters().get('selectedCountries');
                if (this.selectedCountries != null && String.isNotBlank(this.selectedCountries)) {
                    this.selectedCountries = fromHex(this.selectedCountries).unescapeHtml4();
                    setSelectedCountryOptionsList();
                }
        this.negoscopeRFPMap = new Map<Id, ULiT_Negoptim__Commercial_Plan_Detail_RFP__c>();
        this.selectedCountries = '';
        this.recordTypeList = new List<SelectOption>();
        this.recordTypeList.add(new SelectOption('', 'All'));
        List<RecordType> recordtypes = [SELECT Id, Name FROM RecordType WHERE sObjectType = :SObjectType.ULiT_Negoptim__Sup_Supplier__c.Name];
        for (RecordType rt : recordtypes) {
            this.recordTypeList.add(new SelectOption(rt.Id, rt.Name));
        }
        this.statusList = new List<SelectOption>();
        this.statusList.add(new SelectOption('', 'All'));
        Schema.DescribeFieldResult fieldResult = ULiT_Negoptim__Sup_Supplier__c.ULiT_Negoptim__Status__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : ple) {
            this.statusList.add(new SelectOption(pickListVal.getValue(), pickListVal.getLabel()));
        }
        this.riskLevelList = new List<SelectOption>();
        this.riskLevelList.add(new SelectOption('', 'All'));
        Schema.DescribeFieldResult fieldRiskResult = ULiT_Negoptim__Sup_Supplier__c.ULiT_Negoptim__Risk_Supplier_del__c.getDescribe();
        List<Schema.PicklistEntry> riskple = fieldRiskResult.getPicklistValues();
        for (Schema.PicklistEntry pickListVal : riskple) {
            this.riskLevelList.add(new SelectOption(pickListVal.getValue(), pickListVal.getLabel()));
        }
        
        this.ownLabelSupplierTrue = true;
        this.ownLabelSupplierFalse = true;
        this.isPreferredTrue = true;
        this.isPreferredFalse = true;
        this.myNegoSupp = false;
		
        paginationSizeOptions = new List<SelectOption>();
        paginationSizeOptions.add(new SelectOption('5','5'));
        paginationSizeOptions.add(new SelectOption('10','10'));
        paginationSizeOptions.add(new SelectOption('20','20'));
        paginationSizeOptions.add(new SelectOption('50','50'));
        paginationSizeOptions.add(new SelectOption('100','100'));
        
            // check security on Commercial_Plan_Detail__c fields
            String[] commercialPlanDetailFields = new String[] {
                SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.Id.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.Name.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.ULiT_Negoptim__Event_Name__c.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.ULiT_Negoptim__Event_Code__c.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.ULiT_Negoptim__Event_Start_Date__c.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.ULiT_Negoptim__Event_End_Date__c.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail__c.fields.ULiT_Negoptim__Unit_Need__c.Name
                    };
                        
           this.MASTER_OBJECT_NAME = Id.valueOf(this.recordId).getSObjectType().getDescribe().getName();
           if (this.isFromRFX) {
               rfx = [SELECT Id, Name, SPITM_Promo_detail__c, SPITM_Promo_detail__r.ULiT_Negoptim__Unit_Need__c, ULiT_Negoptim__Submission_Deadline__c
                      FROM ULiT_Negoptim__Request_for_x__c
                      WHERE Id = :recordId];
               cpd = [SELECT Id, Name, ULiT_Negoptim__Event_Name__c, ULiT_Negoptim__Event_Code__c, ULiT_Negoptim__Event_Start_Date__c, ULiT_Negoptim__Event_End_Date__c,
                      ULiT_Negoptim__Unit_Need__c, ULiT_Negoptim__Commercial_Event__c, ULiT_Negoptim__Volume_Targeted__c
                      FROM ULiT_Negoptim__Commercial_Plan_Detail__c
                      WHERE Id = :rfx.SPITM_Promo_detail__c];
           } else if(this.isFromCommercialPlanDetail){
               cpd = [SELECT Id, Name, ULiT_Negoptim__Event_Name__c, ULiT_Negoptim__Event_Code__c, ULiT_Negoptim__Event_Start_Date__c, ULiT_Negoptim__Event_End_Date__c,
                      ULiT_Negoptim__Unit_Need__c, ULiT_Negoptim__Commercial_Event__c, ULiT_Negoptim__Volume_Targeted__c
                      FROM ULiT_Negoptim__Commercial_Plan_Detail__c
                      WHERE Id = :recordId];
               List<ULiT_Negoptim__Request_for_x__c> rfxList = [SELECT Id, Name, SPITM_Promo_detail__c, SPITM_Promo_detail__r.ULiT_Negoptim__Unit_Need__c, ULiT_Negoptim__Submission_Deadline__c
                                                                FROM ULiT_Negoptim__Request_for_x__c
                                                                WHERE SPITM_Promo_detail__c = :recordId limit 1];
               if(rfxList != null && !rfxList.isEmpty()) {
                   rfx = rfxList[0];
               }
           } else {
                  //  throw new AuraHandeledException('Label.MSG_Template_No_Master_Detail_Object');
                }
                if (rfx != null) {
                    List<ULiT_Negoptim__Commercial_Plan_Detail_RFP__c> rfpList = [SELECT Id, Name, ULiT_Negoptim__Comment__c, ULiT_Negoptim__Supplier__c, ULiT_Negoptim__Nego_Scope__c 
                                                                                  FROM ULiT_Negoptim__Commercial_Plan_Detail_RFP__c
                                                                                  WHERE ULiT_Negoptim__Request_for_x__c = :rfx.Id];
                    if (!rfpList.isEmpty()) {
                        for(ULiT_Negoptim__Commercial_Plan_Detail_RFP__c rfp : rfpList) {
                            if(!negoscopeRFPMap.keySet().contains(rfp.ULiT_Negoptim__Nego_Scope__c )) {
                                negoscopeRFPMap.put(rfp.ULiT_Negoptim__Nego_Scope__c, rfp);
                            }
                        }
                    }
            }
            this.search();
            // load existing selection
            load();
        } catch(Exception e) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.FATAL, 'Invalid Id'));
        }
    }
    }
    public String fromHex(String input) {
        Blob blobItem = EncodingUtil.convertFromHex(input);
        String hex = EncodingUtil.convertToHex(blobItem);
        return blobItem.toString();
    }
    //Changes the size of pagination
    public PageReference refreshPageSize() {
        setCtrlr.setPageSize(size);
        return null;
    }
    
    // Initialize setCtrlr and return a list of records
    public List<NegoScopeWrapper> getSuppliers() {
        List<NegoScopeWrapper> suppliersList = new List<NegoScopeWrapper>();
        if(setCtrlr != null && !setCtrlr.getRecords().isEmpty()){
            for(ULiT_Negoptim__Sup_sup_NegoScope__c item : (List<ULiT_Negoptim__Sup_sup_NegoScope__c>) setCtrlr.getRecords()) {
                Boolean isSelected = nsMap.keySet().contains(item.Id);
                ULiT_Negoptim__Commercial_Plan_Detail_RFP__c suppRFP = new ULiT_Negoptim__Commercial_Plan_Detail_RFP__c(ULiT_Negoptim__Comment__c = '');
                if(negoscopeRFPMap.keyset().contains(item.id)){
                    suppRFP = negoscopeRFPMap.get(item.Id);
                }
                suppliersList.add(new NegoScopeWrapper(item, suppRFP, isSelected));
            }
            allNegoscopesSet.addAll(suppliersList);
        }
        
        return suppliersList;
    }
    
    public PageReference processSelection() {
        selectedNegoscopes.clear();
        //suppliersIds.clear();
        for(NegoScopeWrapper item : allNegoscopesSet) {
            if(item.selected == true) {
                selectedNegoscopes.add(item.negoscope);
                if(!nsMap.keySet().contains(item.negoscope.Id)) {
                    nsMap.put(item.negoscope.Id, item.negoscope);
                   // suppliersIds.add(item.negoscope.Id);
                }
            } else if(nsMap.keySet().contains(item.negoscope.Id)) {
                nsMap.remove(item.negoscope.Id);
               // suppliersIds.remove(item.negoscope.Id);
            }
        }
        return null;
    }
    
    public List<ULiT_Negoptim__Sup_sup_NegoScope__c> getSelectedSuppliers() {
        if(selectedNegoscopes.size() > 0)
            return selectedNegoscopes;
        else
            return null;
    }
    
    public void load() {
        // check security on Commercial_Plan_Detail_RFP__c fields
        /*String[] commercialPlanDetailRFPFields = new String[] {
            SObjectType.Commercial_Plan_Detail_RFP__c.fields.Supplier__c.Name,
            SObjectType.Commercial_Plan_Detail_RFP__c.fields.Commercial_Event_Line__c.Name
            };
            if (checkAccessibility(Commercial_Plan_Detail_RFP__c.SObjectType, commercialPlanDetailRFPFields)) {*/
        for(ULiT_Negoptim__Commercial_Plan_Detail_RFP__c item : [SELECT ULiT_Negoptim__Nego_Scope__c, ULiT_Negoptim__Nego_Scope__r.ULiT_Negoptim__Supplier__c FROM ULiT_Negoptim__Commercial_Plan_Detail_RFP__c
                                                                 WHERE ULiT_Negoptim__Commercial_Event_Line__c = : cpd.Id])
        {
            initialLoadNegoscopesIds.add(item.ULiT_Negoptim__Nego_Scope__c);
            nsMap.put(item.ULiT_Negoptim__Nego_Scope__c, item.ULiT_Negoptim__Nego_Scope__r);
        }
        //}
    }
    // Button Save Action
    public PageReference save() {
        this.processSelection();
        String rfpRecordType = 'Initialization';
        if(rfx == null){
            rfx = new ULiT_Negoptim__Request_for_x__c(recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Initialization' AND SobjectType = :SObjectType.ULiT_Negoptim__Request_for_x__c.Name LIMIT 1]?.Id, SPITM_Commercial_Plan__c = cpd.ULiT_Negoptim__Commercial_Event__c,
                                                     SPITM_Promo_detail__c = cpd.Id);
            rfpRecordType = 'Published';
            Database.insert(rfx);
            rfx = [SELECT Id, Name, SPITM_Promo_detail__c, SPITM_Promo_detail__r.ULiT_Negoptim__Unit_Need__c, ULiT_Negoptim__Submission_Deadline__c
                   FROM ULiT_Negoptim__Request_for_x__c
                   WHERE Id = :rfx.Id];
        }
        List<ULiT_Negoptim__Commercial_Plan_Detail_RFP__c> RFPList = new List<ULiT_Negoptim__Commercial_Plan_Detail_RFP__c>();
        for(Id item : nsMap.keySet()) {
            if(!initialLoadNegoscopesIds.contains(item)) {
                ULiT_Negoptim__Commercial_Plan_Detail_RFP__c rfp = new ULiT_Negoptim__Commercial_Plan_Detail_RFP__c(ULiT_Negoptim__Commercial_Event_Line__c = cpd.Id, ULiT_Negoptim__Need__c = cpd.ULiT_Negoptim__Unit_Need__c,
                                                                                                                    ULiT_Negoptim__Supplier__c = nsMap.get(item).ULiT_Negoptim__Supplier__c, recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = :rfpRecordType AND SobjectType = :SObjectType.ULiT_Negoptim__Commercial_Plan_Detail_RFP__c.Name LIMIT 1]?.Id,
                                                                                                                    ULiT_Negoptim__Request_for_x__c = rfx.Id, ULiT_Negoptim__Comment__c = '', ULiT_Negoptim__Volume_Proposed_Init__c = cpd.ULiT_Negoptim__Volume_Targeted__c, ULiT_Negoptim__Nego_Scope__c = item,
                                                                                                                    SPITM_Commercial_Plan__c = cpd.ULiT_Negoptim__Commercial_Event__c, ULiT_Negoptim__Answer_Limit__c = rfx.ULiT_Negoptim__Submission_Deadline__c);
                RFPList.add(rfp);
                initialLoadNegoscopesIds.add(item);
            }
        }
        if(RFPList.size() > 0) {
            /*String[] fields = new String[] {
                SObjectType.Commercial_Plan_Detail_RFP__c.fields.Commercial_Event_Line__c.Name,
                SObjectType.Commercial_Plan_Detail_RFP__c.fields.Need__c.Name,
                SObjectType.Commercial_Plan_Detail_RFP__c.fields.Supplier__c.Name
                };
            if (checkCreatibility(Commercial_Plan_Detail_RFP__c.SObjectType, fields)) {*/
            Database.insert(RFPList, false);
            //}
        }
        // Delete dettached relations
        Set<Id> relationsToremove = new Set<Id>();
        for(Id item : initialLoadNegoscopesIds) {
            if(!nsMap.keySet().contains(item)) {
                relationsToremove.add(item);
            }
        }
        if(relationsToremove.size() > 0) {
            String[] commercialPlanDetailRFPFields = new String[] {
                SObjectType.ULiT_Negoptim__Commercial_Plan_Detail_RFP__c.fields.Id.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail_RFP__c.fields.ULiT_Negoptim__Commercial_Event_Line__c.Name,
                    SObjectType.ULiT_Negoptim__Commercial_Plan_Detail_RFP__c.fields.ULiT_Negoptim__Supplier__c.Name
                    };
                        // if (checkAccessibility(Commercial_Plan_Detail_RFP__c.SObjectType, commercialPlanDetailRFPFields) && checkDeletibility(Commercial_Plan_Detail_RFP__c.SObjectType)) {
                        Delete [SELECT Id FROM ULiT_Negoptim__Commercial_Plan_Detail_RFP__c
                                WHERE ULiT_Negoptim__Commercial_Event_Line__c = :cpd.Id
                                AND ULiT_Negoptim__Supplier__c IN :relationsToremove];
            //}
        }
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.CONFIRM, 'Label.MSG_Successfully_Saved'));
        // TODO: Send Email notification to supplier
        return null;
    }
    /**
    * @description search for the new data with the new filter
    */
    public void search() {
        String query = this.buildQuery();
        this.setCtrlr = new ApexPages.StandardSetController(Database.getQueryLocator(query));
        try {
            /*String[] supplierFields = new String[] {
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.Id.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.Name.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.ULiT_Negoptim__Code__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.ULiT_Negoptim__Country_origin__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.ULiT_Negoptim__Status__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.SPITM_Own_Label_Supplier__c	.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.SPITM_Is_Preferred__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.ULiT_Negoptim__Risk_Supplier_del__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.SPITM_Internal_Comments__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.SPITM_Number_of_current_RFx__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.SPITM_Number_of_current_year_RFx__c.Name,
                SObjectType.ULiT_Negoptim__Sup_Supplier__c.fields.Number_of_previous_year_RFx__c.Name
                };*/
            //this.processSelections();
            //if (NegoptimHelper.checkAccessibility(Sup_Supplier__c.SObjectType, supplierFields)) {
            this.setCtrlr.setPageSize(this.size);
            this.noOfRecords = this.setCtrlr.getResultSize();
            //}
        } catch (Exception ex) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage() + ' - ' + ex.getLineNumber()));
        }
    }
    
    
    
    public void resetFilter() {
        searchTermCountry = '';
        selectedCountries = '';
        searchTerm = '';
        selectedRiskLevel = '';
        selectedStatus = '';
        selectedType = '';
        ownLabelSupplierTrue = true;
        ownLabelSupplierFalse = true;
        isPreferredTrue = true;
        isPreferredFalse = true;
        myNegoSupp = false;
        this.search();
    }
    /**
* @description builds a query based on the filter criteria specified on page
* fetches contract sell in records
*/
    private String buildQuery() {
        this.setCtrlr = null;
        List<String> supplierConditions = new List<String>();
        List<String> nsConditions = new List<String>();
        supplierConditions.add('ULiT_Negoptim__Status__c = \'Active\'');
        if (this.selectedType != null && this.selectedType != ''){
            supplierConditions.add(' AND RecordTypeId = \'' + this.selectedType + '\'');
        }
        if (this.selectedStatus != null && this.selectedType != '') {
            supplierConditions.add(' AND ULiT_Negoptim__Status__c = \'' + this.selectedStatus + '\'');
        }
        if (this.selectedRiskLevel != null && this.selectedRiskLevel != '') {
            supplierConditions.add(' AND ULiT_Negoptim__Risk_Supplier_del__c = \'' + this.selectedRiskLevel + '\'');
        }
        if ((!this.isPreferredTrue || !this.isPreferredFalse) && this.isPreferredTrue != this.isPreferredFalse) {
            supplierConditions.add(' AND SPITM_Is_Preferred__c	= ' + this.isPreferredTrue );
        }
        if ((!this.ownLabelSupplierTrue || !this.ownLabelSupplierFalse) && this.ownLabelSupplierTrue != this.ownLabelSupplierFalse) {
            supplierConditions.add(' AND SPITM_Own_Label_Supplier__c = ' + this.ownLabelSupplierTrue);
        }
        if(myNegoSupp) {
            nsConditions.add(' AND OwnerId = \'' + UserInfo.getUserId() + '\'');
        }
        if (this.searchTerm != null && this.searchTerm != '') {
            nsConditions.add(' AND (Name LIKE \'%' + this.searchTerm + '%\' OR ULiT_Negoptim__Supplier__r.Name LIKE \'%' + this.searchTerm + '%\' OR ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Code__c LIKE \'%' + this.searchTerm + '%\')');
        }
        List<String> selectedCountriesList = new List<String>();
        System.debug('selectedCountries '+this.selectedCountries);
        for (String item : String.escapeSingleQuotes(this.selectedCountries).split(',')) {
            if(item != '') selectedCountriesList.add(Id.valueOf(item.trim()));
        }
        if (selectedCountriesList != null && !selectedCountriesList.isEmpty()) {
            supplierConditions.add( ' AND ULiT_Negoptim__Country_origin__c IN (\'' + String.join(selectedCountriesList, '\', \'') + '\')');
        }
        
        // add Order By
        String query = 'SELECT Id, Name, ULiT_Negoptim__Supplier__c, ULiT_Negoptim__Supplier__r.Name, ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Code__c, ULiT_Negoptim__Supplier__r.RecordType.DeveloperName, ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Country_origin__c, ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Status__c,';
        query += ' ULiT_Negoptim__Supplier__r.SPITM_Own_Label_Supplier__c, ULiT_Negoptim__Supplier__r.SPITM_Is_Preferred__c, ULiT_Negoptim__Supplier__r.ULiT_Negoptim__Risk_Supplier_del__c, ULiT_Negoptim__Supplier__r.SPITM_Internal_Comments__c, ULiT_Negoptim__Supplier__r.SPITM_Portal_Comments__c,';
        query += ' ULiT_Negoptim__Supplier__r.SPITM_Number_of_current_RFx__c, ULiT_Negoptim__Supplier__r.SPITM_Number_of_current_year_RFx__c, ULiT_Negoptim__Supplier__r.Number_of_previous_year_RFx__c FROM ULiT_Negoptim__Sup_sup_NegoScope__c ';
        if(!supplierConditions.isEmpty()) {
            query += ' WHERE ULiT_Negoptim__Supplier__c IN (SELECT Id FROM ULiT_Negoptim__Sup_Supplier__c WHERE ' + String.join(supplierConditions, '') + ')';
        }
        if(!nsConditions.isEmpty()) {
           query += String.join(nsConditions, '') + ' Order by Name ASC Limit 10000';
        }
        System.debug(query);
        return query;
    }
    public void searchCountryOrigin() {
        this.countryOriginOptionList = new List<SelectOption>();
        for (ULiT_Negoptim__Orga_BU__c item : searchCountryOptions()) {
            this.countryOriginOptionList.add(new SelectOption(item.Id, item.Name));
        }
        System.debug('countryOriginOptionList >>> ' + this.countryOriginOptionList);
    }
    
    public List<ULiT_Negoptim__Orga_BU__c> searchCountryOptions(){
        List<ULiT_Negoptim__Orga_BU__c> countryList = new List<ULiT_Negoptim__Orga_BU__c>();
        System.debug('searchTermSupplier >>> (' + searchTermCountry + ')');
        String query = 'SELECT Id, Name FROM ULiT_Negoptim__Orga_BU__c ' + (searchTermCountry != null && searchTermCountry != '' ? ' WHERE Name LIKE \'%' + String.escapeSingleQuotes(searchTermCountry)+'%\'' : '')+' LIMIT 10';
        System.debug('query >>> ' + query);
        countryList = Database.query(query);
        return countryList;
    }
    private Set<String> setSelectedCountryOptionsList() {
        Set<String> countryIds = new Set<String>();
        for (String item : this.selectedCountries.split(',')) {
            if (countryIds.size() <= 5) {
                countryIds.add(item);
            }
        }
        this.selectedCountryOriginOptionList = new List<SelectOption>();
        //if (NegoptimHelper.checkAccessibility(Sup_Supplier__c.SObjectType, new List<String>{SObjectType.Sup_Supplier__c.fields.Id.Name, SObjectType.Sup_Supplier__c.fields.Name.Name})) {
        List<ULiT_Negoptim__Orga_BU__c> countryList = [SELECT Id, Name
                                                       FROM ULiT_Negoptim__Orga_BU__c
                                                       WHERE Id IN :countryIds
                                                       ORDER BY Name];
        for (ULiT_Negoptim__Orga_BU__c item : countryList) {
            this.selectedCountryOriginOptionList.add(new SelectOption(item.Id, item.Name));
        }
        //}
        return countryIds;
    }
    public void handleCountryChange() {
        setSelectedCountryOptionsList();
    }
    
    
    /* Supplier Wrapper Class */
    public class NegoScopeWrapper {
        //public ULiT_Negoptim__Sup_Supplier__c supplier {get; set;}
        public ULiT_Negoptim__Sup_sup_NegoScope__c negoscope {get; set;}
        public ULiT_Negoptim__Commercial_Plan_Detail_RFP__c rfpDetail {get; set;}
        public Boolean selected {get; set;}
        
        public NegoScopeWrapper(ULiT_Negoptim__Sup_sup_NegoScope__c ns, ULiT_Negoptim__Commercial_Plan_Detail_RFP__c rfp, Boolean sel) {
            negoscope = ns;
            selected = sel;
            rfpDetail = rfp;
        }
    }
    
}