/**
* @author ULiT
* @description unit test class for MoveContractConditionsBatch
* @date 04/10/2021
*/
@isTest
private class MoveContractConditionsBatchTest {
    private static Integer randomNumber;
    private static Integer year;
    private static ULit_Negoptim__Orga_BU__c region;
    private static ULit_Negoptim__Orga_BU__c country;
    private static ULit_Negoptim__Sup_Supplier__c supplier;
    private static ULit_Negoptim__Sup_sup_NegoScope__c NS;
    private static ULiT_Negoptim__Contract__c sourceContract;
    private static ULiT_Negoptim__Contract__c targetContract;
    private static ULit_Negoptim__Pol_Com_Condition__c sourceTariffCondition;
    private static ULit_Negoptim__Pol_Com_Condition__c targetTariffCondition;
    private static ULit_Negoptim__Contract_Discount__c sourceDiscount;
    private static ULit_Negoptim__Contract_Discount__c targetDiscount;
    private static final Map<String, Id> contractRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Contract__c.SObjectType);
    private static final Map<String, Id> buRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Orga_BU__c.SObjectType);
        
    static void init() {
        Id rootId = getRootId();
        year = System.Today().year();
        // Add Country
        ULit_Negoptim__Country_List__c myCountry = createCountry(true, 'FRANCE', 'FR');
        // Create Region
        region = createBURegion(true, 'EU');
        // Create Country
        country = createBUCountry(true, myCountry, region.Id);
        // Create Supplier
        supplier = createSupplier(false, country, true);
        insert supplier;
        // Get the default NegoScope created on supplier creation
        NS = getNSs(supplier.Id)[0];
        // Create Contract
        sourceContract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        sourceContract.ULit_Negoptim__TO1__c = 1000;
        sourceContract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Nego_Base_Init'));
        targetContract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        targetContract.ULit_Negoptim__TO1__c = 1000;
        targetContract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('Nego_Base'));
        insert new List<ULiT_Negoptim__Contract__c> {sourceContract, targetContract};
        // Create Policy - Tariff Conditions
        sourceTariffCondition = createTariffCondition(false, 0, 1);
        sourceTariffCondition.ULit_Negoptim__Condition_status__c = 'Open';
        sourceTariffCondition.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        sourceTariffCondition.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        sourceTariffCondition.ULit_Negoptim__Status_BDate__c = date.newInstance(year - 2 , 1, 1);
        sourceTariffCondition.ULit_Negoptim__Proof_counterparty_BU__c = country.Id;
        targetTariffCondition = createTariffCondition(false, 0, 1);
        targetTariffCondition.ULit_Negoptim__Condition_status__c = 'Open';
        targetTariffCondition.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        targetTariffCondition.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        targetTariffCondition.ULit_Negoptim__Status_BDate__c = date.newInstance(year - 2 , 1, 1);
        targetTariffCondition.ULit_Negoptim__Proof_counterparty_BU__c = country.Id;
        insert new List<ULit_Negoptim__Pol_Com_Condition__c> {sourceTariffCondition, targetTariffCondition};
        ULit_Negoptim__Pol_Com_Condition__c childrenTariffCondition1 = createTariffCondition(false, 0, 1);
        childrenTariffCondition1.ULiT_Negoptim__Parent_Condition__c = sourceTariffCondition.Id;
        childrenTariffCondition1.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        childrenTariffCondition1.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        childrenTariffCondition1.ULit_Negoptim__Proof_counterparty_BU__c = country.Id;
        childrenTariffCondition1.ULit_Negoptim__Is_Default_SubCondition__c = true;
        ULit_Negoptim__Pol_Com_Condition__c childrenTariffCondition2 = createTariffCondition(false, 0, 1);
        childrenTariffCondition2.SPITM_Groupe_nego__c = '10_REMISE_FACTURE';
        childrenTariffCondition2.ULit_Negoptim__Nego_Discount_type__c = 'Price Discount';
        childrenTariffCondition2.ULiT_Negoptim__Parent_Condition__c = targetTariffCondition.Id;
        childrenTariffCondition2.ULit_Negoptim__Proof_counterparty_BU__c = country.Id;
        childrenTariffCondition2.ULit_Negoptim__Is_Default_SubCondition__c = true;
        insert new List<ULit_Negoptim__Pol_Com_Condition__c> {childrenTariffCondition1, childrenTariffCondition2};
    }
    
    testmethod static void case1_move_full_not_unicity_condition() {
        Test.startTest();
        init();
        // Create Conditions
        sourceDiscount = createCondition(false, sourceTariffCondition, sourceContract, sourceContract.ULit_Negoptim__Contract_BDate__c, sourceContract.ULit_Negoptim__Contract_EDate__c);
        sourceDiscount.ULit_Negoptim__Is_TargetToMove__c = true;
        sourceDiscount.ULit_Negoptim__Rank__c = 1;
        sourceDiscount.ULit_Negoptim__TargetToMove_Condition__c = targetTariffCondition.Id;
        sourceDiscount.ULit_Negoptim__TargetToMove_per__c = 10;
        sourceDiscount.ULit_Negoptim__TargetToMove_amt__c = 1000;
        sourceDiscount.ULit_Negoptim__TargetToMove_U__c = 500;
        targetDiscount = createCondition(false, sourceTariffCondition, targetContract, targetContract.ULit_Negoptim__Contract_BDate__c, targetContract.ULit_Negoptim__Contract_EDate__c);
        targetDiscount.ULit_Negoptim__Rank__c = 1;
        targetDiscount.ULit_Negoptim__Value_per__c = 10;
        targetDiscount.ULit_Negoptim__Value_amt__c = 1000;
        targetDiscount.ULit_Negoptim__unit_amount__c = 500;
        insert new List<ULit_Negoptim__Contract_Discount__c> {sourceDiscount, targetDiscount};
        Database.executebatch(new MoveContractConditionsBatch('Test', new List<Id>{sourceContract.Id}, new List<Id>{targetContract.Id}), 10);
        Test.stopTest();
        targetDiscount = [SELECT ULit_Negoptim__Condition_Type__c, ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Value_per__c, ULit_Negoptim__Value_amt__c, ULit_Negoptim__unit_amount__c FROM ULit_Negoptim__Contract_Discount__c WHERE Id = :targetDiscount.Id];
        System.assertEquals(targetTariffCondition.Id, targetDiscount.ULit_Negoptim__Condition_Type__c);
        System.assertEquals(targetTariffCondition.ULit_Negoptim__Nego_Discount_Type__c, targetDiscount.ULit_Negoptim__Nego_Discount_Type__c);
        System.assertEquals(sourceDiscount.ULit_Negoptim__TargetToMove_per__c, targetDiscount.ULit_Negoptim__Value_per__c);
        System.assertEquals(sourceDiscount.ULit_Negoptim__TargetToMove_amt__c, targetDiscount.ULit_Negoptim__Value_amt__c);
        System.assertEquals(sourceDiscount.ULit_Negoptim__TargetToMove_U__c, targetDiscount.ULit_Negoptim__unit_amount__c);
    }
    
    testmethod static void case2_Move_Full_with_Unicity_Condition_But_Not_In_Target() {
        Test.startTest();
        init();
        targetTariffCondition.ULit_Negoptim__Is_Contract_Unicity_Requiered__c = true;
        update targetTariffCondition;
        // Create Conditions
        sourceDiscount = createCondition(false, sourceTariffCondition, sourceContract, sourceContract.ULit_Negoptim__Contract_BDate__c, sourceContract.ULit_Negoptim__Contract_EDate__c);
        sourceDiscount.ULit_Negoptim__Is_TargetToMove__c = true;
        sourceDiscount.ULit_Negoptim__Rank__c = 1;
        sourceDiscount.ULit_Negoptim__TargetToMove_Condition__c = targetTariffCondition.Id;
        sourceDiscount.ULit_Negoptim__TargetToMove_per__c = 10;
        sourceDiscount.ULit_Negoptim__TargetToMove_amt__c = 1000;
        sourceDiscount.ULit_Negoptim__TargetToMove_U__c = 500;
        targetDiscount = createCondition(false, sourceTariffCondition, targetContract, targetContract.ULit_Negoptim__Contract_BDate__c, targetContract.ULit_Negoptim__Contract_EDate__c);
        targetDiscount.ULit_Negoptim__Rank__c = 1;
        targetDiscount.ULit_Negoptim__Value_per__c = 10;
        targetDiscount.ULit_Negoptim__Value_amt__c = 1000;
        targetDiscount.ULit_Negoptim__unit_amount__c = 500;
        insert new List<ULit_Negoptim__Contract_Discount__c> {sourceDiscount, targetDiscount};
        Database.executebatch(new MoveContractConditionsBatch('Test', new List<Id>{sourceContract.Id}, new List<Id>{targetContract.Id}), 10);
        Test.stopTest();
        targetDiscount = [SELECT ULit_Negoptim__Condition_Type__c, ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Value_per__c,
                                                                           ULit_Negoptim__Value_amt__c, ULit_Negoptim__unit_amount__c 
                                                                           FROM ULit_Negoptim__Contract_Discount__c 
                                                                           WHERE Id = :targetDiscount.Id];
        System.assertEquals(targetTariffCondition.Id, targetDiscount.ULit_Negoptim__Condition_Type__c);
        System.assertEquals(targetTariffCondition.ULit_Negoptim__Nego_Discount_Type__c, targetDiscount.ULit_Negoptim__Nego_Discount_Type__c);
        System.assertEquals(10, targetDiscount.ULit_Negoptim__Value_per__c);
        System.assertEquals(1000, targetDiscount.ULit_Negoptim__Value_amt__c);
        System.assertEquals(500, targetDiscount.ULit_Negoptim__unit_amount__c);
    }
    
    testmethod static void case3_Move_Full_with_Unicity_Condition_Exist_In_Target() {
        Test.startTest();
        init();
        targetTariffCondition.ULit_Negoptim__Is_Contract_Unicity_Requiered__c = true;
        update targetTariffCondition;
        // Create Conditions
        sourceDiscount = createCondition(false, sourceTariffCondition, sourceContract, sourceContract.ULit_Negoptim__Contract_BDate__c, sourceContract.ULit_Negoptim__Contract_EDate__c);
        sourceDiscount.ULit_Negoptim__Is_TargetToMove__c = true;
        sourceDiscount.ULit_Negoptim__Rank__c = 1;
        sourceDiscount.ULit_Negoptim__TargetToMove_Condition__c = targetTariffCondition.Id;
        sourceDiscount.ULit_Negoptim__TargetToMove_per__c = 10;
        sourceDiscount.ULit_Negoptim__TargetToMove_amt__c = 1000;
        sourceDiscount.ULit_Negoptim__TargetToMove_U__c = 500;
        targetDiscount = createCondition(false, sourceTariffCondition, targetContract, targetContract.ULit_Negoptim__Contract_BDate__c, targetContract.ULit_Negoptim__Contract_EDate__c);
        targetDiscount.ULit_Negoptim__Rank__c = 1;
        targetDiscount.ULit_Negoptim__Value_per__c = 10;
        targetDiscount.ULit_Negoptim__Value_amt__c = 1000;
        targetDiscount.ULit_Negoptim__unit_amount__c = 500;
        ULit_Negoptim__Contract_Discount__c uniqueTargetDiscount = createCondition(false, targetTariffCondition, targetContract, targetContract.ULit_Negoptim__Contract_BDate__c, targetContract.ULit_Negoptim__Contract_EDate__c);
        uniqueTargetDiscount.ULit_Negoptim__Rank__c = 1;
        uniqueTargetDiscount.ULit_Negoptim__Value_per__c = 10;
        uniqueTargetDiscount.ULit_Negoptim__Value_amt__c = 1000;
        uniqueTargetDiscount.ULit_Negoptim__unit_amount__c = 500;
        insert new List<ULit_Negoptim__Contract_Discount__c> {sourceDiscount, targetDiscount, uniqueTargetDiscount};
        Database.executebatch(new MoveContractConditionsBatch('Test', new List<Id>{sourceContract.Id}, new List<Id>{targetContract.Id}), 10);
        Test.stopTest();
        uniqueTargetDiscount = [SELECT ULit_Negoptim__Condition_Type__c, ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Value_per__c,
                                ULit_Negoptim__Value_amt__c, ULit_Negoptim__unit_amount__c 
                                FROM ULit_Negoptim__Contract_Discount__c 
                                WHERE ULit_Negoptim__Contract__c = :targetContract.Id
                                AND ULit_Negoptim__Condition_Type__c = :targetTariffCondition.Id];
        ULit_Negoptim__Contract_Discount__c targetDiscountDeleted = [SELECT ULit_Negoptim__Condition_Type__c, ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Value_per__c,
                                                                           ULit_Negoptim__Value_amt__c, ULit_Negoptim__unit_amount__c, ULit_Negoptim__Status__c
                                                                           FROM ULit_Negoptim__Contract_Discount__c 
                                                                           WHERE Id = :targetDiscount.Id];
        System.assertEquals(targetTariffCondition.Id, uniqueTargetDiscount.ULit_Negoptim__Condition_Type__c);
        System.assertEquals(targetTariffCondition.ULit_Negoptim__Nego_Discount_Type__c, uniqueTargetDiscount.ULit_Negoptim__Nego_Discount_Type__c);
        System.assertEquals(20, uniqueTargetDiscount.ULit_Negoptim__Value_per__c);
        System.assertEquals(2000, uniqueTargetDiscount.ULit_Negoptim__Value_amt__c);
        System.assertEquals(1000, uniqueTargetDiscount.ULit_Negoptim__unit_amount__c);
        System.assertEquals('Deleted', targetDiscountDeleted.ULit_Negoptim__Status__c);
    }
    
    testmethod static void case4_move_partial_not_unicity_condition() {
        Test.startTest();
        init();
        // Create Conditions
        sourceDiscount = createCondition(false, sourceTariffCondition, sourceContract, sourceContract.ULit_Negoptim__Contract_BDate__c, sourceContract.ULit_Negoptim__Contract_EDate__c);
        sourceDiscount.ULit_Negoptim__Is_TargetToMove__c = true;
        sourceDiscount.ULit_Negoptim__Rank__c = 1;
        sourceDiscount.ULit_Negoptim__TargetToMove_Condition__c = targetTariffCondition.Id;
        sourceDiscount.ULit_Negoptim__TargetToMove_per__c = 10;
        sourceDiscount.ULit_Negoptim__TargetToMove_amt__c = 1000;
        sourceDiscount.ULit_Negoptim__TargetToMove_U__c = 500;
        targetDiscount = createCondition(false, sourceTariffCondition, targetContract, targetContract.ULit_Negoptim__Contract_BDate__c, targetContract.ULit_Negoptim__Contract_EDate__c);
        targetDiscount.ULit_Negoptim__Rank__c = 1;
        targetDiscount.ULit_Negoptim__Value_per__c = 20;
        targetDiscount.ULit_Negoptim__Value_amt__c = 1500;
        targetDiscount.ULit_Negoptim__unit_amount__c = 750;
        insert new List<ULit_Negoptim__Contract_Discount__c> {sourceDiscount, targetDiscount};
        Database.executebatch(new MoveContractConditionsBatch('Test', new List<Id>{sourceContract.Id}, new List<Id>{targetContract.Id}), 10);
        Test.stopTest();
        ULit_Negoptim__Contract_Discount__c newCondition = [SELECT ULit_Negoptim__Value_per__c, ULit_Negoptim__Value_amt__c, ULiT_Negoptim__unit_amount__c FROM ULit_Negoptim__Contract_Discount__c WHERE ULit_Negoptim__Condition_Type__c = :targetTariffCondition.Id];
        ULit_Negoptim__Contract_Discount__c oldCondition = [SELECT ULit_Negoptim__Value_per__c, ULit_Negoptim__Value_amt__c, ULiT_Negoptim__unit_amount__c FROM ULit_Negoptim__Contract_Discount__c WHERE Id = :targetDiscount.Id];
        System.assertEquals(10, newCondition.ULit_Negoptim__Value_per__c);
        System.assertEquals(1000, newCondition.ULit_Negoptim__Value_amt__c);
        System.assertEquals(500, newCondition.ULit_Negoptim__unit_amount__c);
        System.assertEquals(10, oldCondition.ULit_Negoptim__Value_per__c);
        System.assertEquals(500, oldCondition.ULit_Negoptim__Value_amt__c);
        System.assertEquals(250, oldCondition.ULit_Negoptim__unit_amount__c);
    }
    
    testmethod static void case5_move_partial_with_unicity_condition() {
        Test.startTest();
        init();
        targetTariffCondition.ULit_Negoptim__Is_Contract_Unicity_Requiered__c = true;
        update targetTariffCondition;
        // Create Conditions
        sourceDiscount = createCondition(false, sourceTariffCondition, sourceContract, sourceContract.ULit_Negoptim__Contract_BDate__c, sourceContract.ULit_Negoptim__Contract_EDate__c);
        sourceDiscount.ULit_Negoptim__Is_TargetToMove__c = true;
        sourceDiscount.ULit_Negoptim__Rank__c = 1;
        sourceDiscount.ULit_Negoptim__TargetToMove_Condition__c = targetTariffCondition.Id;
        sourceDiscount.ULit_Negoptim__TargetToMove_per__c = 10;
        sourceDiscount.ULit_Negoptim__TargetToMove_amt__c = 400;
        sourceDiscount.ULit_Negoptim__TargetToMove_U__c = 200;
        targetDiscount = createCondition(false, sourceTariffCondition, targetContract, targetContract.ULit_Negoptim__Contract_BDate__c, targetContract.ULit_Negoptim__Contract_EDate__c);
        targetDiscount.ULit_Negoptim__Rank__c = 1;
        targetDiscount.ULit_Negoptim__Value_per__c = 20;
        targetDiscount.ULit_Negoptim__Value_amt__c = 1000;
        targetDiscount.ULit_Negoptim__unit_amount__c = 500;
        insert new List<ULit_Negoptim__Contract_Discount__c> {sourceDiscount, targetDiscount};
        Database.executebatch(new MoveContractConditionsBatch('Test', new List<Id>{sourceContract.Id}, new List<Id>{targetContract.Id}), 10);
        Test.stopTest();
        ULit_Negoptim__Contract_Discount__c newCondition = [SELECT ULit_Negoptim__Value_per__c, ULit_Negoptim__Value_amt__c FROM ULit_Negoptim__Contract_Discount__c WHERE ULit_Negoptim__Condition_Type__c = :targetTariffCondition.Id];
        system.assertEquals(sourceDiscount.ULit_Negoptim__TargetToMove_per__c, newCondition.ULit_Negoptim__Value_per__c);
        system.assertEquals(sourceDiscount.ULit_Negoptim__TargetToMove_amt__c, newCondition.ULit_Negoptim__Value_amt__c);
        targetDiscount = [SELECT ULit_Negoptim__Value_per__c, ULit_Negoptim__Value_amt__c, ULit_Negoptim__unit_amount__c FROM ULit_Negoptim__Contract_Discount__c WHERE Id = :targetDiscount.Id];
        ULit_Negoptim__Contract_Discount__c uniqueDiscount = [SELECT ULit_Negoptim__Value_per__c, ULit_Negoptim__Value_amt__c, ULit_Negoptim__unit_amount__c FROM ULit_Negoptim__Contract_Discount__c WHERE ULit_Negoptim__Contract__c = :targetContract.Id AND ULit_Negoptim__Condition_Type__c = :targetTariffCondition.Id];
        // targetDiscount.ULit_Negoptim__Value_per__c = targetDiscount.ULit_Negoptim__Value_per__c - sourceDiscount.ULit_Negoptim__TargetToMove_per__c
        system.assertEquals(20 - 10, targetDiscount.ULit_Negoptim__Value_per__c);
        system.assertEquals(1000 - 400, targetDiscount.ULit_Negoptim__Value_amt__c);
        system.assertEquals(500 - 200, targetDiscount.ULit_Negoptim__unit_amount__c);
        system.assertEquals(10, uniqueDiscount.ULit_Negoptim__Value_per__c);
        system.assertEquals(400, uniqueDiscount.ULit_Negoptim__Value_amt__c);
        system.assertEquals(200, uniqueDiscount.ULit_Negoptim__unit_amount__c);
    }
    
    /**
     * Get Orga_HE__c root element Id
     * */
    public static Id getRootId() {
        Id rootid;
        List<Ulit_Negoptim__Orga_HE__c> elements = [SELECT Id FROM Ulit_Negoptim__Orga_HE__c WHERE Ulit_Negoptim__Parent_Element__c = null];
        if(elements != NULL && elements.size() > 0) {
            rootid = elements.get(0).Id;
        } else {
            Ulit_Negoptim__Orga_HE__c root = new Ulit_Negoptim__Orga_HE__c(Name = 'ROOT', Ulit_Negoptim__Elt_Code__c = 'ROOT', Ulit_Negoptim__Parent_Element__c = null, Ulit_Negoptim__Level__c = 0, Ulit_Negoptim__Status_BDate__c = date.newInstance(System.today().year(), 1, 1));
            insert root;
            rootid = root.Id;
        }
        return rootid;
    }
    
    /* BUSINESS UNIT */
    // Country List creation.
    private static ULit_Negoptim__Country_List__c createCountry(Boolean doInsert, String name, String code) {
        ULit_Negoptim__Country_List__c myCountry = new ULit_Negoptim__Country_List__c(Name = name, ULit_Negoptim__Country_Code__c = code);
        if (doInsert) insert myCountry;
        System.assert(true);
        return myCountry;
    }

    // Region creation.
    private static ULit_Negoptim__Orga_BU__c createBURegion(Boolean doInsert, String name) {
        ULit_Negoptim__Orga_BU__c region = new ULit_Negoptim__Orga_BU__c(Name = name, ULit_Negoptim__BU_Code__c = name, ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Open',
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                           ULit_Negoptim__Set_BU_List_Entity__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                           ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                           ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Payment__c = true,
                                           ULit_Negoptim__Set_Invoice__c = true,
                                           ULit_Negoptim__Set_Product_Listing__c = true,
                                           ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                           ULit_Negoptim__Set_SF_Planning__c = true,
                                           ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                           ULit_Negoptim__Set_Budget_Maker__c = true,
                                           ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                           ULit_Negoptim__Set_PriceList_Statement__c = true);
        region.RecordTypeId = buRTIds.get('Region');
        if (doInsert) insert region;
        System.assert(true);
        return region;
    }
    // Country creation.
    private static ULit_Negoptim__Orga_BU__c createBUCountry(Boolean doInsert, ULit_Negoptim__Country_List__c c, Id regionId) {
        ULit_Negoptim__Orga_BU__c country = new ULit_Negoptim__Orga_BU__c(Name = c.Name, ULit_Negoptim__BU_Code__c = c.ULit_Negoptim__Country_Code__c + getRandomNumber(), ULit_Negoptim__ISO_Country_Code__c = c.ULit_Negoptim__Country_Code__c,
                                            ULit_Negoptim__Country__c = c.Name, ULit_Negoptim__Country_Zone_origin__c = regionId, ULit_Negoptim__Status__c = 'Open',
                                            ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                            ULit_Negoptim__Set_BU_List_Entity__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                            ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                            ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Payment__c = true,
                                            ULit_Negoptim__Set_Invoice__c = true,
                                            ULit_Negoptim__Set_Product_Listing__c = true,
                                            ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                            ULit_Negoptim__Set_SF_Planning__c = true,
                                            ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                            ULit_Negoptim__Set_Budget_Maker__c = true,
                                            ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                            ULit_Negoptim__Set_PriceList_Statement__c = true);
        country.RecordTypeId = buRTIds.get('Country');
        if (doInsert) insert country;
        System.assert(true);
        return country;
    }

    /* SUPPLIER + NEGOSCOPE */
    // Supplier creation.
    private static ULit_Negoptim__Sup_Supplier__c createSupplier(Boolean doInsert, ULit_Negoptim__Orga_BU__c country, Boolean withNS) {
    	String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Sup_Supplier__c supplier = new ULit_Negoptim__Sup_Supplier__c(Name = 'SUP ' + rdm, ULit_Negoptim__Code__c = rdm, ULit_Negoptim__Country_origin__c = country.Id, ULit_Negoptim__Acc_Country__c = country.Name,
        								ULit_Negoptim__Is_Default_NegoScope__c = withNS, ULit_Negoptim__Acc_Address_External_Synchro__c = false, ULit_Negoptim__Admin_Address_External_Synchro__c = false,
        								ULit_Negoptim__Status__c = 'Active');
        if (doInsert) insert supplier;
        System.assert(true);
        return supplier;
    }

    // Get the list of NS related to the supplier passed by parameter.
	private static List<ULit_Negoptim__Sup_sup_NegoScope__c> getNSs(Id SupplierId) {
		System.assert(true);
        return [SELECT Id, Name, ULit_Negoptim__Supplier__c, ULit_Negoptim__Supplier__r.Name, ULit_Negoptim__Supplier__r.ULit_Negoptim__Code__c, ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                OwnerId, ULit_Negoptim__NS_Code__c, ULit_Negoptim__NS_Code_Long__c
                FROM ULit_Negoptim__Sup_sup_NegoScope__c
                WHERE ULit_Negoptim__Supplier__c = :supplierId];

 	}

    /* CONTRACT + CONDITION */
    // Contract creation.
    private static ULit_Negoptim__Contract__c createContract(Boolean doInsert, ULit_Negoptim__Sup_sup_NegoScope__c NS, Date beginDate, Date endDate) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Contract__c contract = new ULit_Negoptim__Contract__c(Name = NS.ULit_Negoptim__Supplier__r.Name + ' - Contract ' + year, ULit_Negoptim__Supplier__c = NS.ULit_Negoptim__Supplier__c,
                                               ULit_Negoptim__Supplier_Nego_Scope__c = NS.Id, ULit_Negoptim__Contract_BU__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                                               ULit_Negoptim__BU_Source__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c, ULit_Negoptim__Contract_BDate__c = beginDate,
                                               ULit_Negoptim__Contract_EDate__c = endDate, ULit_Negoptim__Duration__c = beginDate.monthsBetween(endDate) + 1,
                                               ULit_Negoptim__Contract_Commercial_BDate__c = beginDate, ULit_Negoptim__Contract_Commercial_EDate__c = endDate,
                                               ULit_Negoptim__Contract_Type__c = 'Contract', ULit_Negoptim__Status__c = 'Signed', ULit_Negoptim__Ext_id__c = rdm,
                                               ULit_Negoptim__Duration_type__c = 'Month', ULit_Negoptim__Contract_Numbder__c = 'c' + rdm,
                                               RecordTypeId = contractRTIds.get('To_Be_Signed'));
        if (doInsert) Database.insert(contract);
		System.assert(true);
        return contract;
    }

    // Method to increment randomNumber
    private static Integer getRandomNumber() {
        if (randomNumber == null)
            randomNumber = (Integer)(Math.random()*9999);
        return randomNumber++;
    }
	/**
     * Get Object RecordType Map of Name/Id
     * @param sObjectType
     * */
    public static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();
        // Check Accessibility.
        // if(!checkAccessibilityFields(Schema.SObjectType.RecordType.fields.getMap(), new String [] {'Id', 'DeveloperName'})) {
        // 	return rtMap;
        // }
        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }
    /**
     * Get Object RecordType Id by Name
     * @param sObjectType
     * @param recordTypeName
     * @example: Id recordTypeId = NegoptimHelper.getObjectRecordTypeId(ULit_Negoptim__Orga_BU__c.SObjectType, 'Country');
     * */
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName) {
        // If sObjectType is wrong, then an Exception is thrown.
        Id rt;
        Map<Id, Schema.RecordTypeInfo> sObjectTypeRecordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosById();
        for (Id RTId : sObjectTypeRecordTypeInfo.keySet()) {
            if (sObjectTypeRecordTypeInfo.get(RTId).getDeveloperName() == recordTypeName) {
                rt = RTId;
            }
        }
        if (rt == null) {
            // If recordTypeName is wrong, then an Exception is thrown.
            throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.');
        }
        //Return the record type id
        return rt;
    }
	// Tarrif Condition creation.
    public static ULit_Negoptim__Pol_Com_Condition__c createTariffCondition(Boolean doInsert, Integer pickListValue, Integer indexValue) {
    	String rdm = String.valueOf(getRandomNumber());
    	// Get field describe.
    	Schema.DescribeFieldResult fieldResult = ULit_Negoptim__Pol_Com_Condition__c.ULit_Negoptim__Nego_Discount_Type__c.getDescribe();
        // Get the picklist value.
        String plv = fieldResult.getPicklistValues().get(pickListValue).getValue();
        // Create an instance of Tarrif Condition.
        ULit_Negoptim__Pol_Com_Condition__c tariffCondition = new ULit_Negoptim__Pol_Com_Condition__c(Name = plv, ULit_Negoptim__Nego_Discount_Type__c = plv, ULit_Negoptim__Index__c = 'Index' + indexValue,
        								ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Condition_Code__c = rdm,
        								ULit_Negoptim__Abrev__c = 'TC', ULit_Negoptim__Gen_Name_com__c = 'TC', ULit_Negoptim__Name_Com__c = 'TC', ULit_Negoptim__Name_Fi__c = 'TC', ULit_Negoptim__Name_Leg__c = 'TC',
        								ULit_Negoptim__Gen_Name_Fi__c = 'TC', ULit_Negoptim__Gen_Name_Leg__c = 'TC', ULit_Negoptim__VAT_Type__c = 'Rebate', ULit_Negoptim__Acc_Document_Type__c = 'Credit Note Request');
        // Do insertion.
        if (doInsert) insert tariffCondition;
        System.assert(true);
        return tariffCondition;
    }
    // Condition creation.
    public static ULit_Negoptim__Contract_Discount__c createCondition(Boolean doInsert, ULit_Negoptim__Pol_Com_Condition__c tariffCondition, ULit_Negoptim__Contract__c contract, Date beginDate, Date endDate) {
        ULit_Negoptim__Contract_Discount__c condition = new ULit_Negoptim__Contract_Discount__c(ULit_Negoptim__Nego_Discount_Type__c = tariffCondition.ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Condition_Type__c = tariffCondition.Id,
                                                                                                ULit_Negoptim__Contract__c = contract.Id, ULit_Negoptim__Product_Scope__c = contract.ULit_Negoptim__Supplier_Nego_Scope__c, ULit_Negoptim__BU_Scope__c = contract.ULit_Negoptim__BU_Source__c,
                                                                                                ULit_Negoptim__Disc_BDate__c = beginDate, ULit_Negoptim__Disc_EDate__c = endDate);
        if (doInsert) Database.insert(condition);
        System.assert(true);
        return condition;
    }
    public class RecordTypeException extends Exception {}
}