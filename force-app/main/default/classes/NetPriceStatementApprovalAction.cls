/**
 * @author ULiT
 * @date 01 June 2021
 * @description class containing invocable action to respond to an approval process of a NetPrice Statement from the assortment record page via a flow
*/
global class NetPriceStatementApprovalAction {
    @InvocableMethod(label = 'Respond to approval Process')
    global static List<ResultWrapper> executeResponseAction(List<ResponseWrapper> responses) {
        System.SavePoint sp = Database.setSavepoint();
        try {
            Map<Id, ResponseWrapper> responseWrapperMap = new Map<Id, ResponseWrapper>();
            for (ResponseWrapper rw : responses) {
                responseWrapperMap.put(rw.assortmentBUId, rw);
            }
            Map<Id, ULiT_Negoptim__NetPrice_Statement__c> netPriceStatementsMap = new Map<Id, ULiT_Negoptim__NetPrice_Statement__c> ([SELECT Id, ULiT_Negoptim__Assortment_BU__c
                                                                                                                                      FROM ULiT_Negoptim__NetPrice_Statement__c
                                                                                                                                      WHERE ULiT_Negoptim__Assortment_BU__c IN :responseWrapperMap.keySet()]);
            
            List<ResultWrapper> results = new List<ResultWrapper>();
            for (ProcessInstanceWorkitem item : [SELECT Id, ProcessInstance.ProcessDefinitionId, ProcessInstance.TargetObjectId, ProcessInstance.Status
                                                 FROM ProcessInstanceWorkitem
                                                 WHERE ProcessInstance.Status = 'Pending'
                                                 AND ProcessInstance.TargetObjectId IN :netPriceStatementsMap.keySet()])
            {
                ResponseWrapper rw = responseWrapperMap?.get(netPriceStatementsMap?.get(item.ProcessInstance.TargetObjectId)?.ULiT_Negoptim__Assortment_BU__c);
                String action = rw.isApproved ? 'Approve' : 'Reject';
                
                Approval.ProcessWorkitemRequest req = new Approval.ProcessWorkitemRequest();
                req.setComments(rw.comment);
                
                //Approve or Reject Record
                req.setAction(action);
                req.setWorkitemId(item.Id);
                // Submit the request for approval
                Approval.ProcessResult result = Approval.process(req);
                
                ResultWrapper resultWrapper = new ResultWrapper();
                resultWrapper.assortmentBUId = netPriceStatementsMap?.get(result.getEntityId())?.ULiT_Negoptim__Assortment_BU__c;
                resultWrapper.isSuccess  = result.isSuccess();
                String errors = '';
                if (!result.isSuccess()) {
                    for (Database.Error error : result.getErrors()) {
                        errors += error.getMessage();
                    }
                }
                resultWrapper.errors  = errors;
                results.add(resultWrapper);
            }
            return results;
        } catch (Exception ex) {
            Database.rollback(sp);
            throw new HandledException(ex.getMessage());
        }
    }
    
    global class ResponseWrapper {
        @InvocableVariable(required=true)
        global Id assortmentBUId;
        @InvocableVariable(required=true)
        global Boolean isApproved;
        @InvocableVariable
        global String comment;
    }
    
    global class ResultWrapper {
        @InvocableVariable(required=true)
        global Id assortmentBUId;
        @InvocableVariable
        global Boolean isSuccess;
        @InvocableVariable
        global String errors;
        
    }
}