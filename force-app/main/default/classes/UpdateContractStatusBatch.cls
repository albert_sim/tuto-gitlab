/**
 * @author ULiT
 * @date 23-08-2021
 * @description Batch to Update Contracts PE Status to SPITM_Sans_Signature
******************************************************************************************************************************************* */
public class UpdateContractStatusBatch implements Database.Batchable<sObject> {

    private ULiT_Negoptim.NegoptimBatch nb;
    public String query;

    public UpdateContractStatusBatch(String startedFrom) {
        this.nb = new ULit_Negoptim.NegoptimBatch(UpdateContractStatusBatch.class.getName(), ULit_Negoptim.NegoptimBatch.BatchType.stateful, startedFrom);
        Integer year = Date.today().year();
        String q = 'SELECT Id FROM ULiT_Negoptim__Contract__c ';
        q += ' WHERE RecordType.DeveloperName = \'Period_Extension\'';
        q += ' AND ULiT_Negoptim__Reference_Year__c = ' + year;
        q += ' AND ULiT_Negoptim__Status__c IN (\'In preparation\',\'Invalide\',\'Refusé Frs\',\'Refusé ITMAI\',\'Validated\')';
        this.query = q;
        System.debug('q = '+ q);
        this.nb.logParameter('query', q);
    }
    public Database.QueryLocator start(Database.BatchableContext BC) {
        return Database.getQueryLocator(query);
    }

    public void execute(Database.BatchableContext BC, List<ULiT_Negoptim__Contract__c> scope) {
        System.SavePoint sp = Database.setSavepoint();
        try {
            for (ULiT_Negoptim__Contract__c contract : scope) {
                contract.ULiT_Negoptim__Status__c = 'SPITM_Sans_Signature';
            }
            // check security on ULiT_Negoptim__Contract__c fields
            List<String> contractUpdateFields = new List<String> {
                SObjectType.ULiT_Negoptim__Contract__c.fields.ULiT_Negoptim__Status__c.Name
            };
            if (ULiT_Negoptim.NegoptimHelper.checkUpdatibility(ULiT_Negoptim__Contract__c.SObjectType, contractUpdateFields)) {
                List<Database.SaveResult> results = Database.update(scope, false);
                for (Integer i = 0; i < results.size(); i++) {
                    Database.SaveResult result = results.get(i);
                    ULiT_Negoptim__Contract__c contract = scope.get(i);
                    if (!result.isSuccess()) {
                        String errorMessage = contract + '\n';
                        Database.Error[] errors = result.getErrors();
                        for (Database.Error err : errors) {
                            errorMessage += err.getFields() + ' - ' + err.getStatusCode() + ' - ' + err.getMessage() + '\n';
                        }
                        this.nb.logError(errorMessage);
                    }
                }
            }
        } catch(DMLException e) {
            Database.rollback(sp);
            nb.logError('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
        } catch(Exception e) {
            system.debug('Exception: ' + e.getMessage() + ' - ' + e.getLineNumber());
            Database.rollback(sp);
            nb.logError(e);
        }
        // save log
        nb.saveLog(bc);
    }

    public void finish(Database.BatchableContext BC) {
        this.nb.sendEmail(BC, null, UpdateContractStatusBatch.class.getName());
    }
}