/**
 * @author ULiT
 * @date 08-09-2021
 * @description Unit test class for UpdatePromoLineReferenceBatch
******************************************************************************************************************************************* */
@isTest
private class UpdatePromoLineReferenceBatchTest {
    private static Integer randomNumber;
    private static Integer year;
    private static ULit_Negoptim__Orga_BU__c region;
    private static ULit_Negoptim__Orga_BU__c country;
    private static ULit_Negoptim__Sup_Supplier__c supplier;
    private static ULit_Negoptim__Sup_sup_NegoScope__c NS;
    private static ULit_Negoptim__Contract__c contract;
    ////private static ULit_Negoptim__Pol_Com_Condition__c tariffCondition;
    ////private static ULit_Negoptim__Contract_Discount__c condition;
    private static ULit_Negoptim__Orga_HE__c department;
    private static ULit_Negoptim__Orga_HE__c section;
    private static List<Id> contractIdSet = new List<Id>();
    public static final Map<String, Id> contractRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Contract__c.SObjectType);
    public static final Map<String, Id> buRTIds = getObjectRecordTypeMapIds(ULit_Negoptim__Orga_BU__c.SObjectType);

    static void init() {
        try {
            Id rootId = getRootId();
            year = System.Today().year();
             // Add Country
            ULit_Negoptim__Country_List__c myCountry = createCountry(true, 'FRANCE', 'FR');
            // Create Region
            region = createBURegion(true, 'EU');
            // Create Country
            country = createBUCountry(true, myCountry, region.Id);
            // Create Supplier
            supplier = createSupplier(false, country, true);
            insert supplier;
            // Get the default NegoScope created on supplier creation
            NS = getNSs(supplier.Id)[0];
            // Create Contract
            contract = createContract(false, NS, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
            contract.Name = supplier.Name + ' - Contract ' + year;
            contract.ULit_Negoptim__TO1__c = 1000;
            contract.ULit_Negoptim__Contract_Numbder__c = 'C001';
            contract.ULit_Negoptim__Contract_Type__c = 'Contract';
            contract.ULit_Negoptim__D_N__c = 'N';
            contract.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Contract__c.SObjectType, ('To_Be_Signed'));
            insert contract;
            // Create Policy - Tariff Conditions
            /*tariffCondition = createTariffCondition(false, 0, 1);
            tariffCondition.ULit_Negoptim__Condition_status__c = 'Open';
            tariffCondition.ULit_Negoptim__Status_BDate__c = date.newInstance(year - 2 , 1, 1);
            insert tariffCondition;*/
            // Create Condition
            /*condition = createCondition(false, tariffCondition, contract, contract.ULit_Negoptim__Contract_BDate__c, contract.ULit_Negoptim__Contract_EDate__c);
            insert condition;*/
            // Create Department
            department = createDepartment(true, rootId);
            // Create Section
            section = createSection(true, department.Id);
        } catch (Exception ex) {
            System.debug('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>');
            System.debug('ERROR >>>>>>>>>>>>>>>>>>>>>>>>' + ex.getMessage() + ' - ' + ex.getLineNumber());
            System.debug(ex.getStackTraceString());
            System.debug('<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<');
        }
    }

    static testmethod void test_case1() {
        init();
        // insert product
        Product2 p1 = createProduct(false, section.Id, supplier.Id);
        p1.IsActive = true;
        p1.RecordTypeId = getObjectRecordTypeId(Product2.SObjectType, ('Product'));
        Product2 p2 = createProduct(false, section.Id, supplier.Id);
        p2.IsActive = true;
        p2.RecordTypeId = getObjectRecordTypeId(Product2.SObjectType, ('Product'));
        insert new List<Product2>{p1, p2};
        // insert product HLP
        Product2 productHLP1 = createProduct(false, section.Id, supplier.Id);
        productHLP1.IsActive = true;
        productHLP1.ULiT_Negoptim__Consumer_Trade_Item__c = p1.Id;
        productHLP1.RecordTypeId = getObjectRecordTypeId(Product2.SObjectType, ('Higher_Level_Packaging'));
        Product2 productHLP2 = createProduct(false, section.Id, supplier.Id);
        productHLP2.IsActive = true;
        productHLP2.ULiT_Negoptim__Consumer_Trade_Item__c = p2.Id;
        productHLP2.RecordTypeId = getObjectRecordTypeId(Product2.SObjectType, ('Higher_Level_Packaging'));
        insert new List<Product2>{productHLP1, productHLP2};
        // insert commercial plan
        ULit_Negoptim__Commercial_Plan__c commercialPlan = createCommercialPlan(false, date.newInstance(year, 1, 1), date.newInstance(year, 12, 31));
        commercialPlan.ULit_Negoptim__BU_Target__c = country.Id;
        insert commercialPlan;
        // insert promo details
        ULit_Negoptim__Commercial_Plan_Detail__c commercialPlanDetailP1 = createCommercialPlanDetail(false, commercialPlan.Id, NS);
        commercialPlanDetailP1.ULit_Negoptim__Contract__c = contract.Id;
        ////commercialPlanDetailP1.ULit_Negoptim__Prenego_envelop__c = condition.Id;
        commercialPlanDetailP1.ULit_Negoptim__Nego_Scope__c = NS.Id;
        commercialPlanDetailP1.ULit_Negoptim__Product__c = p1.Id;
        commercialPlanDetailP1.ULit_Negoptim__BU_Target__c = country.Id;
        ULit_Negoptim__Commercial_Plan_Detail__c commercialPlanDetailHLP1 = createCommercialPlanDetail(false, commercialPlan.Id, NS);
        commercialPlanDetailHLP1.ULit_Negoptim__Contract__c = contract.Id;
        ////commercialPlanDetailHLP1.ULit_Negoptim__Prenego_envelop__c = condition.Id;
        commercialPlanDetailHLP1.ULit_Negoptim__Nego_Scope__c = NS.Id;
        commercialPlanDetailHLP1.ULit_Negoptim__Product__c = productHLP1.Id;
        commercialPlanDetailHLP1.ULit_Negoptim__BU_Target__c = country.Id;
        ULit_Negoptim__Commercial_Plan_Detail__c commercialPlanDetailHLP2 = createCommercialPlanDetail(false, commercialPlan.Id, NS);
        commercialPlanDetailHLP2.ULit_Negoptim__Contract__c = contract.Id;
        ////commercialPlanDetailHLP2.ULit_Negoptim__Prenego_envelop__c = condition.Id;
        commercialPlanDetailHLP2.ULit_Negoptim__Nego_Scope__c = NS.Id;
        commercialPlanDetailHLP2.ULit_Negoptim__Product__c = productHLP2.Id;
        commercialPlanDetailHLP2.ULit_Negoptim__BU_Target__c = country.Id;
        insert new List<ULit_Negoptim__Commercial_Plan_Detail__c>{commercialPlanDetailP1, commercialPlanDetailHLP1, commercialPlanDetailHLP2};
        Test.startTest();
        Database.executebatch(new UpdatePromoLineReferenceBatch('Test'), 10);
        Test.stopTest();
        commercialPlanDetailHLP1 = [SELECT Id, ULiT_Negoptim__Consumer_Trade_Item_Promo_Detail__c FROM ULit_Negoptim__Commercial_Plan_Detail__c WHERE Id = :commercialPlanDetailHLP1.Id];
        commercialPlanDetailHLP2 = [SELECT Id, ULiT_Negoptim__Consumer_Trade_Item_Promo_Detail__c FROM ULit_Negoptim__Commercial_Plan_Detail__c WHERE Id = :commercialPlanDetailHLP2.Id];
        System.assertEquals(commercialPlanDetailP1.Id, commercialPlanDetailHLP1.ULiT_Negoptim__Consumer_Trade_Item_Promo_Detail__c);
        System.assertNotEquals(null, commercialPlanDetailHLP2.ULiT_Negoptim__Consumer_Trade_Item_Promo_Detail__c);
    }

    /**
     * Get Orga_HE__c root element Id
     * */
    public static Id getRootId() {
        Id rootid;
        List<Ulit_Negoptim__Orga_HE__c> elements = [SELECT Id FROM Ulit_Negoptim__Orga_HE__c WHERE Ulit_Negoptim__Parent_Element__c = null];
        if(elements != NULL && elements.size() > 0) {
            rootid = elements.get(0).Id;
        } else {
            Ulit_Negoptim__Orga_HE__c root = new Ulit_Negoptim__Orga_HE__c(Name = 'ROOT', Ulit_Negoptim__Elt_Code__c = 'ROOT', Ulit_Negoptim__Parent_Element__c = null, Ulit_Negoptim__Level__c = 0, Ulit_Negoptim__Status_BDate__c = date.newInstance(System.today().year(), 1, 1));
            insert root;
            rootid = root.Id;
        }
        return rootid;
    }
    
    /* BUSINESS UNIT */
    // Country List creation.
    private static ULit_Negoptim__Country_List__c createCountry(Boolean doInsert, String name, String code) {
        ULit_Negoptim__Country_List__c myCountry = new ULit_Negoptim__Country_List__c(Name = name, ULit_Negoptim__Country_Code__c = code);
        if (doInsert) insert myCountry;
        System.assert(true);
        return myCountry;
    }

    // Region creation.
    private static ULit_Negoptim__Orga_BU__c createBURegion(Boolean doInsert, String name) {
        ULit_Negoptim__Orga_BU__c region = new ULit_Negoptim__Orga_BU__c(Name = name, ULit_Negoptim__BU_Code__c = name, ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Open',
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                           ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                           ULit_Negoptim__Set_BU_List_Entity__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                           ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                           ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                           ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                           ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                           ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Detail__c = true,
                                           ULit_Negoptim__Set_Invoice_Payment__c = true,
                                           ULit_Negoptim__Set_Invoice__c = true,
                                           ULit_Negoptim__Set_Product_Listing__c = true,
                                           ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                           ULit_Negoptim__Set_SF_Planning__c = true,
                                           ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                           ULit_Negoptim__Set_Budget_Maker__c = true,
                                           ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                           ULit_Negoptim__Set_PriceList_Statement__c = true);
        region.RecordTypeId = buRTIds.get('Region');
        if (doInsert) insert region;
        System.assert(true);
        return region;
    }
    // Country creation.
    private static ULit_Negoptim__Orga_BU__c createBUCountry(Boolean doInsert, ULit_Negoptim__Country_List__c c, Id regionId) {
        ULit_Negoptim__Orga_BU__c country = new ULit_Negoptim__Orga_BU__c(Name = c.Name, ULit_Negoptim__BU_Code__c = c.ULit_Negoptim__Country_Code__c + getRandomNumber(), ULit_Negoptim__ISO_Country_Code__c = c.ULit_Negoptim__Country_Code__c,
                                            ULit_Negoptim__Country__c = c.Name, ULit_Negoptim__Country_Zone_origin__c = regionId, ULit_Negoptim__Status__c = 'Open',
                                            ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1),
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                            ULit_Negoptim__Set_BU_List_Entity__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                            ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                            ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Payment__c = true,
                                            ULit_Negoptim__Set_Invoice__c = true,
                                            ULit_Negoptim__Set_Product_Listing__c = true,
                                            ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                            ULit_Negoptim__Set_SF_Planning__c = true,
                                            ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                            ULit_Negoptim__Set_Budget_Maker__c = true,
                                            ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                            ULit_Negoptim__Set_PriceList_Statement__c = true);
        country.RecordTypeId = buRTIds.get('Country');
        if (doInsert) insert country;
        System.assert(true);
        return country;
    }

    // Group creation.
    private static ULit_Negoptim__Orga_BU__c createGroupBU(Boolean doInsert, String name) {
   		String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Orga_BU__c groupBU = new ULit_Negoptim__Orga_BU__c(Name = 'Group ' + rdm, ULit_Negoptim__BU_Code__c = 'G' + rdm, ULit_Negoptim__ISO_Country_Code__c = 'LBN',
                                            ULit_Negoptim__BU_ODate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Open',
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Budget_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Nego_BU_Target__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Source__c = true,
                                            ULit_Negoptim__Set_Assortment_Target_BU_Target__c = true,
                                            ULit_Negoptim__Set_BU_List_Entity__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Counterparty__c = true,
                                            ULit_Negoptim__Set_Com_Condition_Master_BU__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Source__c = true,
                                            ULit_Negoptim__Set_Contract_BU_Target__c = true,
                                            ULit_Negoptim__Set_Contract_Condition_BU_Scope__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Main_Term__c = true,
                                            ULit_Negoptim__Set_Contract_Discount_Sub_Term__c = true,
                                            ULit_Negoptim__Set_Invoice_BUDispatch_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Detail__c = true,
                                            ULit_Negoptim__Set_Invoice_Payment__c = true,
                                            ULit_Negoptim__Set_Invoice__c = true,
                                            ULit_Negoptim__Set_Product_Listing__c = true,
                                            ULit_Negoptim__Set_SF_Data_Collection__c = true,
                                            ULit_Negoptim__Set_SF_Planning__c = true,
                                            ULit_Negoptim__Set_Budget_Maker_Detail__c = true,
                                            ULit_Negoptim__Set_Budget_Maker__c = true,
                                            ULit_Negoptim__Set_NetPrice_Statement__c = true,
                                            ULit_Negoptim__Set_PriceList_Statement__c = true);
        groupBU.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Orga_BU__c.SObjectType, 'Group');
       	if (doInsert) insert groupBU;
       	System.assert(true);
       	return groupBU;
   	}

    /* SUPPLIER + NEGOSCOPE */
    // Supplier creation.
    private static ULit_Negoptim__Sup_Supplier__c createSupplier(Boolean doInsert, ULit_Negoptim__Orga_BU__c country, Boolean withNS) {
    	String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Sup_Supplier__c supplier = new ULit_Negoptim__Sup_Supplier__c(Name = 'SUP ' + rdm, ULit_Negoptim__Code__c = rdm, ULit_Negoptim__Country_origin__c = country.Id, ULit_Negoptim__Acc_Country__c = country.Name,
        								ULit_Negoptim__Is_Default_NegoScope__c = withNS, ULit_Negoptim__Acc_Address_External_Synchro__c = false, ULit_Negoptim__Admin_Address_External_Synchro__c = false,
        								ULit_Negoptim__Status__c = 'Active');
        if (doInsert) insert supplier;
        System.assert(true);
        return supplier;
    }

    // Get the list of NS related to the supplier passed by parameter.
	private static List<ULit_Negoptim__Sup_sup_NegoScope__c> getNSs(Id SupplierId) {
		System.assert(true);
        return [SELECT Id, Name, ULit_Negoptim__Supplier__c, ULit_Negoptim__Supplier__r.Name, ULit_Negoptim__Supplier__r.ULit_Negoptim__Code__c, ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                OwnerId, ULit_Negoptim__NS_Code__c, ULit_Negoptim__NS_Code_Long__c
                FROM ULit_Negoptim__Sup_sup_NegoScope__c
                WHERE ULit_Negoptim__Supplier__c = :supplierId];

 	}

    /* CONTRACT + CONDITION */
    // Contract creation.
    private static ULit_Negoptim__Contract__c createContract(Boolean doInsert, ULit_Negoptim__Sup_sup_NegoScope__c NS, Date beginDate, Date endDate) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Contract__c contract = new ULit_Negoptim__Contract__c(Name = NS.ULit_Negoptim__Supplier__r.Name + ' - Contract ' + year, ULit_Negoptim__Supplier__c = NS.ULit_Negoptim__Supplier__c,
                                               ULit_Negoptim__Supplier_Nego_Scope__c = NS.Id, ULit_Negoptim__Contract_BU__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c,
                                               ULit_Negoptim__BU_Source__c = NS.ULit_Negoptim__Supplier__r.ULit_Negoptim__Country_origin__c, ULit_Negoptim__Contract_BDate__c = beginDate,
                                               ULit_Negoptim__Contract_EDate__c = endDate, ULit_Negoptim__Duration__c = beginDate.monthsBetween(endDate) + 1,
                                               ULit_Negoptim__Contract_Commercial_BDate__c = beginDate, ULit_Negoptim__Contract_Commercial_EDate__c = endDate,
                                               ULit_Negoptim__Contract_Type__c = 'Contract', ULit_Negoptim__Status__c = 'Signed', ULit_Negoptim__Ext_id__c = rdm, ULit_Negoptim__D_N__c = 'N',
                                               ULit_Negoptim__Duration_type__c = 'Month', ULit_Negoptim__Contract_Numbder__c = 'c' + rdm,
                                               RecordTypeId = contractRTIds.get('To_Be_Signed'));
        if (doInsert) Database.insert(contract);
		System.assert(true);
        return contract;
    }

    // Method to increment randomNumber
    private static Integer getRandomNumber() {
        if (randomNumber == null)
            randomNumber = (Integer)(Math.random()*9999);
        return randomNumber++;
    }
	/**
     * Get Object RecordType Map of Name/Id
     * @param sObjectType
     * */
    public static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();
        // Check Accessibility.
        // if(!checkAccessibilityFields(Schema.SObjectType.RecordType.fields.getMap(), new String [] {'Id', 'DeveloperName'})) {
        // 	return rtMap;
        // }
        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }
    /**
     * Get Object RecordType Id by Name
     * @param sObjectType
     * @param recordTypeName
     * @example: Id recordTypeId = NegoptimHelper.getObjectRecordTypeId(ULit_Negoptim__Orga_BU__c.SObjectType, 'Country');
     * */
    public static String getObjectRecordTypeId(SObjectType sObjectType, String recordTypeName) {
        // If sObjectType is wrong, then an Exception is thrown.
        Id rt;
        Map<Id, Schema.RecordTypeInfo> sObjectTypeRecordTypeInfo = sObjectType.getDescribe().getRecordTypeInfosById();
        for (Id RTId : sObjectTypeRecordTypeInfo.keySet()) {
            if (sObjectTypeRecordTypeInfo.get(RTId).getDeveloperName() == recordTypeName) {
                rt = RTId;
            }
        }
        if (rt == null) {
            // If recordTypeName is wrong, then an Exception is thrown.
            throw new RecordTypeException('Record type "'+ recordTypeName +'" does not exist.');
        }
        //Return the record type id
        return rt;
    }
	// Tarrif Condition creation.
    public static ULit_Negoptim__Pol_Com_Condition__c createTariffCondition(Boolean doInsert, Integer pickListValue, Integer indexValue) {
    	String rdm = String.valueOf(getRandomNumber());
    	// Get field describe.
    	Schema.DescribeFieldResult fieldResult = ULit_Negoptim__Pol_Com_Condition__c.ULit_Negoptim__Nego_Discount_Type__c.getDescribe();
        // Get the picklist value.
        String plv = fieldResult.getPicklistValues().get(pickListValue).getValue();
        // Create an instance of Tarrif Condition.
        ULit_Negoptim__Pol_Com_Condition__c tariffCondition = new ULit_Negoptim__Pol_Com_Condition__c(Name = plv, ULit_Negoptim__Nego_Discount_Type__c = plv, ULit_Negoptim__Index__c = 'Index' + indexValue,
        								ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Condition_Code__c = rdm,
        								ULit_Negoptim__Abrev__c = 'TC', ULit_Negoptim__Gen_Name_com__c = 'TC', ULit_Negoptim__Name_Com__c = 'TC', ULit_Negoptim__Name_Fi__c = 'TC', ULit_Negoptim__Name_Leg__c = 'TC',
        								ULit_Negoptim__Gen_Name_Fi__c = 'TC', ULit_Negoptim__Gen_Name_Leg__c = 'TC', ULit_Negoptim__VAT_Type__c = 'Rebate', ULit_Negoptim__Acc_Document_Type__c = 'Credit Note Request');
        // Do insertion.
        if (doInsert) insert tariffCondition;
        System.assert(true);
        return tariffCondition;
    }
    // Condition creation.
    public static ULit_Negoptim__Contract_Discount__c createCondition(Boolean doInsert, ULit_Negoptim__Pol_Com_Condition__c tariffCondition, ULit_Negoptim__Contract__c contract, Date beginDate, Date endDate) {
        ULit_Negoptim__Contract_Discount__c condition = new ULit_Negoptim__Contract_Discount__c(ULit_Negoptim__Nego_Discount_Type__c = tariffCondition.ULit_Negoptim__Nego_Discount_Type__c, ULit_Negoptim__Condition_Type__c = tariffCondition.Id,
                                                                                                ULit_Negoptim__Contract__c = contract.Id, ULit_Negoptim__Product_Scope__c = contract.ULit_Negoptim__Supplier_Nego_Scope__c, ULit_Negoptim__BU_Scope__c = contract.ULit_Negoptim__BU_Source__c,
                                                                                                ULit_Negoptim__Disc_BDate__c = beginDate, ULit_Negoptim__Disc_EDate__c = endDate);
        if (doInsert) Database.insert(condition);
        System.assert(true);
        return condition;
    }
    
    /* HIERARCHY ELEMENT + BRAND + PRODUCT + SELL IN */
    // Department creation.
    public static ULit_Negoptim__Orga_HE__c createDepartment(Boolean doInsert, Id parentId) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Orga_HE__c department = new ULit_Negoptim__Orga_HE__c(Name = 'DPT ' + rdm, ULit_Negoptim__Elt_Code__c = 'D' + rdm, ULit_Negoptim__Parent_Element__c = parentId, ULit_Negoptim__Level__c = 1,
                                               ULit_Negoptim__Dispatch_Inv_Hierarchy_Starting_Point__c = false, ULit_Negoptim__Purchases_DB_Upper_Starting_Point__c = true,
                                               ULit_Negoptim__Set_Assortment_nego_level__c	 = true, ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Active');
        if (doInsert) insert department;
        System.assert(true);
        return department;
    }
    // Section creation.
    public static ULit_Negoptim__Orga_HE__c createSection(Boolean doInsert, Id parentId) {
        String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Orga_HE__c section = new ULit_Negoptim__Orga_HE__c(Name = 'SEC ' + rdm, ULit_Negoptim__Elt_Code__c = 'S' + rdm, ULit_Negoptim__Parent_Element__c = parentId, ULit_Negoptim__Level__c = 2,
                                            ULit_Negoptim__Dispatch_Inv_Hierarchy_Starting_Point__c = true, ULit_Negoptim__Purchases_DB_Upper_Starting_Point__c = false,
                                            ULit_Negoptim__Status_BDate__c = date.newInstance(year, 1, 1), ULit_Negoptim__Status__c = 'Active', ULit_Negoptim__Set_Product_link_level__c = true);
        if (doInsert) insert section;
        System.assert(true);
        return section;
    }
    
    // Product creation.
    public static Product2 createProduct(Boolean doInsert, Id sectionId, Id supplierId) {
        String rdm = String.valueOf(getRandomNumber());
        Product2 product = new Product2(Name = 'PRD ' + rdm, ProductCode = 'P' + rdm, ULit_Negoptim__Product_EAN__c = '10000' + rdm,
                                        ULit_Negoptim__Category__c = sectionId, ULit_Negoptim__Product_MasterSupplier__c = supplierId, IsActive = true,
                                        ULit_Negoptim__Ext_Id__c = '10000' + rdm + '_' + 'P' + rdm);
        if (doInsert) insert product;
        System.assert(true);
        return product;
    }
    
    /* COMMERCIAL PLAN */
    // Commercial Plan creation.
    public static ULit_Negoptim__Commercial_Plan__c createCommercialPlan(Boolean doInsert, Date beginDate, Date endDate) {
    	String rdm = String.valueOf(getRandomNumber());
        ULit_Negoptim__Commercial_Plan__c commercialPlan = new ULit_Negoptim__Commercial_Plan__c(Name = 'Comm' + rdm, ULit_Negoptim__Start_Date__c = beginDate, ULit_Negoptim__End_Date__c = endDate,
                                                                   ULit_Negoptim__Event_Type__c = 'Collection', ULit_Negoptim__Event_Support__c = 'catalogue', ULit_Negoptim__Status__c = 'In Preparation');
        commercialPlan.RecordTypeId = getObjectRecordTypeId(ULit_Negoptim__Commercial_Plan__c.SObjectType, ('Media_Catalog'));
        if (doInsert) Database.insert(commercialPlan);
        System.assert(true);
        return commercialPlan;
    }
    // Commercial Plan Detail creation.
    public static ULit_Negoptim__Commercial_Plan_Detail__c createCommercialPlanDetail(Boolean doInsert, Id commercialPlanId, ULit_Negoptim__Sup_sup_NegoScope__c NS) {
        ULit_Negoptim__Commercial_Plan_Detail__c commercialPlanDetail = new ULit_Negoptim__Commercial_Plan_Detail__c(ULit_Negoptim__Commercial_Event__c = commercialPlanId, ULit_Negoptim__Nego_Scope__c = NS.Id,
                                                                                       ULit_Negoptim__Supplier__c = NS.ULit_Negoptim__Supplier__c, ULit_Negoptim__Contribution_Unit_Quantity__c = 1, ULit_Negoptim__Contribution_Unit_Amount__c = 1,
                                                                                       ULit_Negoptim__Statut__c = 'In process of validation', ULit_Negoptim__Temp_Contract_Reference__c = 'Temp Ref Contract...');
   		if (doInsert) Database.insert(commercialPlanDetail);
   		System.assert(true);
        return commercialPlanDetail;
    }
    public class RecordTypeException extends Exception {}
}