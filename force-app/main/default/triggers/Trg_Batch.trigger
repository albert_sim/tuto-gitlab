trigger Trg_Batch on ULiT_Negoptim__Batch__c (after insert) {
    if (Trigger.isAfter) {
        if (Trigger.isInsert) {
            Set<Id> batchIdForProductIntegrationSet = new Set<Id>();
            Id batchIdForContractIntegration;
            Id productIntegrationRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Product_Integration' AND SobjectType = 'ULiT_Negoptim__Batch__c' LIMIT 1]?.Id;
            Id contractIntegrationRT = [SELECT Id FROM RecordType WHERE DeveloperName = 'Contract_Integration' AND SobjectType = 'ULiT_Negoptim__Batch__c' LIMIT 1]?.Id;
            for (ULiT_Negoptim__Batch__c batchObj : trigger.new) {
                if (productIntegrationRT != null && batchObj.RecordTypeId == productIntegrationRT && batchObj.ULiT_Negoptim__Status__c == 'In Progress') {
                    batchIdForProductIntegrationSet.add(batchObj.Id);
                }
                if (contractIntegrationRT != null && batchObj.RecordTypeId == contractIntegrationRT && batchObj.ULiT_Negoptim__Status__c == 'In Progress') {
                    batchIdForContractIntegration = batchObj.Id;
                }
            }
            if (!batchIdForProductIntegrationSet.isEmpty()) {
                ULiT_Negoptim.NegoptimBatch.executeBatch(new InitSPMwithLegacyFieldsBatch('Trg_Batch', null, batchIdForProductIntegrationSet), 100);
            }
            if (batchIdForContractIntegration != null) {
                ULiT_Negoptim.NegoptimBatch.executeBatch(new CalculateContractIndexesBatch('Trg_Batch', null, batchIdForContractIntegration), 10);
            }
        }
    }
}