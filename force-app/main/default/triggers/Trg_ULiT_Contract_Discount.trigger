/**
 * @author ULiT
 * @description TMP code to update SPITM_Is_Include_NIP__c, SPITM_Is_Include_Promo_Condition__c when a new condition is created or deleted
 * Update SPITM_SRP_Correction__c on contract and contract discount after change on SPITM_SRP_Correction__c
 * @date 21 September 2021
*/
trigger Trg_ULiT_Contract_Discount on ULiT_Negoptim__Contract_Discount__c (before insert, before update, after insert, after delete) {

    // Before Insert / Before Update
    // Update SPITM_SRP_Correction__c on contract and contract discount after change on SPITM_SRP_Correction__c
    if (trigger.isBefore) {
        if (trigger.isInsert || trigger.isUpdate) {
            List<ULiT_Negoptim__Contract_Discount__c> nDiscounts = trigger.new;
            Map<Id, ULiT_Negoptim__Contract_Discount__c> oDiscounts = trigger.OldMap;
            List<ULiT_Negoptim__Contract_Discount__c> updatedDiscountsList = new List<ULiT_Negoptim__Contract_Discount__c>();
            Map<Id, ULiT_Negoptim__Contract__c> contractsToUpdate = new Map<Id, ULiT_Negoptim__Contract__c>();
            
            Set<Id> contractIdSet = new Set<Id>();
            for (ULiT_Negoptim__Contract_Discount__c item : nDiscounts) {
                if (item.Id == null || item.SPITM_SRP_Correction__c != oDiscounts.get(item.Id).SPITM_SRP_Correction__c) {
                    contractIdSet.add(item.ULiT_Negoptim__Contract__c);
                    updatedDiscountsList.add(item);
                }
            }
            if (!contractIdSet.isEmpty()) {
                Map<Id, ULiT_Negoptim__Contract__c> contractsMap = new Map<Id, ULiT_Negoptim__Contract__c>([SELECT Id, SPITM_SRP_Correction__c
                                                                                                            FROM ULiT_Negoptim__Contract__c
                                                                                                            WHERE Id IN :contractIdSet
                                                                                                            AND ULiT_Negoptim__Is_Managed_By_ExternalSource__c = FALSE]);
                if (!contractsMap.isEmpty()) {
                    for (ULiT_Negoptim__Contract_Discount__c item : updatedDiscountsList) {
                        if (contractsMap.containsKey(item.ULiT_Negoptim__Contract__c)) {
                            // update N % Négo. on discount
                            item.ULiT_Negoptim__X2015_Nego_p__c = item.SPITM_SRP_Correction__c;
                            // update +/- SRP on contract
                            ULiT_Negoptim__Contract__c contract = contractsMap.get(item.ULiT_Negoptim__Contract__c);
                            Decimal contractSRPCorrection = contract.SPITM_SRP_Correction__c != null ? contract.SPITM_SRP_Correction__c : 0;
                            Decimal newDiscountSRPCorrection = item.SPITM_SRP_Correction__c != null ? item.SPITM_SRP_Correction__c : 0;
                            Decimal oldDiscountSRPCorrection = oDiscounts != null && oDiscounts.containsKey(item.Id) && oDiscounts.get(item.Id).SPITM_SRP_Correction__c != null ? oDiscounts.get(item.Id).SPITM_SRP_Correction__c : 0;
                            contract.SPITM_SRP_Correction__c = contractSRPCorrection + (newDiscountSRPCorrection - oldDiscountSRPCorrection);
                            contractsToUpdate.put(contract.Id, contract);
                        }
                    }
                    if (!contractsToUpdate.isEmpty()) {
                        update contractsToUpdate.values();
                    }
                }
            }
        }
    }
    if (Trigger.isAfter) {
        Set<Id> nipIdSet = new Set<Id>();
        Set<Id> promoIdSet = new Set<Id>();

        for (ULiT_Negoptim__Pol_Com_Condition__c item : [SELECT Id, Name
                                                         FROM ULiT_Negoptim__Pol_Com_Condition__c
                                                         WHERE Name IN ('NIP', 'RP0', 'EN0')])
        {
            if (item.Name == 'NIP') {
                nipIdSet.add(item.Id);
            } else {
                promoIdSet.add(item.Id);
            }
        }
        // case after insert
        if (Trigger.isInsert) {
            Set<Id> containsNipConditionContracts = new Set<Id>();
            Set<Id> containsPromoConditionContracts = new Set<Id>();
            // go over all new contract discounts and seperate their contacts into 2 sets depending on the tarrif condition
            for (ULiT_Negoptim__Contract_Discount__c item : Trigger.new) {
                if (item.ULiT_Negoptim__Condition_Type__c != null && (item.SPITM_Contract_Record_Type__c == 'Signed' || item.SPITM_Contract_Record_Type__c == 'To_Be_Signed')) {
                    if (nipIdSet.contains(item.ULiT_Negoptim__Condition_Type__c)) {
                        containsNipConditionContracts.add(item.ULiT_Negoptim__Contract__c);
                    }
                    if (promoIdSet.contains(item.ULiT_Negoptim__Condition_Type__c)) {
                        containsPromoConditionContracts.add(item.ULiT_Negoptim__Contract__c);
                    }
                }
            }
            // query contracts
            if (!containsNipConditionContracts.isEmpty() || !containsPromoConditionContracts.isEmpty()) {
                List<ULiT_Negoptim__Contract__c> contracts = [SELECT Id, SPITM_Is_Include_NIP__c, SPITM_Is_Include_Promo_Condition__c
                                                              FROM ULiT_Negoptim__Contract__c
                                                              WHERE Id IN :containsNipConditionContracts
                                                              OR Id IN :containsPromoConditionContracts];
                for (ULiT_Negoptim__Contract__c item : contracts) {
                    // if the contract id is in the set of contract that contains a new NIP condtion, then set SPITM_Is_Include_NIP__c to true
                    if (containsNipConditionContracts.contains(item.Id)) {
                        item.SPITM_Is_Include_NIP__c = true;
                    }
                    // if the contract id is in the set of contract that contains a new RP0 or EN0 condtion, then set SPITM_Is_Include_Promo_Condition__c to true
                    if (containsPromoConditionContracts.contains(item.Id)) {
                        item.SPITM_Is_Include_Promo_Condition__c = true;
                    }
                }
                // update the contracts
                update contracts;
            }
        }
        // case after delete
        if (Trigger.isDelete) {
            Set<Id> deletedNipConditionContracts = new Set<Id>();
            Set<Id> deletedPromoConditionContracts = new Set<Id>();
            // loop over the deleted conditions and seperate their contracts into 2 sets depending on the deleted conditions' type
            for (ULiT_Negoptim__Contract_Discount__c item : Trigger.old) {
                if (item.ULiT_Negoptim__Condition_Type__c != null && (item.SPITM_Contract_Record_Type__c == 'Signed' || item.SPITM_Contract_Record_Type__c == 'To_Be_Signed')) {
                    if (nipIdSet.contains(item.ULiT_Negoptim__Condition_Type__c)) {
                        deletedNipConditionContracts.add(item.ULiT_Negoptim__Contract__c);
                    }
                    if (promoIdSet.contains(item.ULiT_Negoptim__Condition_Type__c)) {
                        deletedPromoConditionContracts.add(item.ULiT_Negoptim__Contract__c);
                    }
                }
            }

            // aggregate the number of NIP, RP0, EN0 conditions remaining for the contracts
            if (!deletedNipConditionContracts.isEmpty() || !deletedPromoConditionContracts.isEmpty()) {
                List<AggregateResult> results = [SELECT COUNT(Id), ULiT_Negoptim__Contract__c, ULiT_Negoptim__Condition_Type__c
                                                 FROM ULiT_Negoptim__Contract_Discount__c
                                                 WHERE (ULiT_Negoptim__Condition_Type__c IN :nipIdSet OR ULiT_Negoptim__Condition_Type__c IN :promoIdSet)
                                                 AND RecordType.DeveloperName IN ('Signed', 'To_Be_Signed')
                                                 AND Id NOT IN :Trigger.old
                                                 AND (ULiT_Negoptim__Contract__c IN :deletedNipConditionContracts
                                                      OR ULiT_Negoptim__Contract__c IN :deletedPromoConditionContracts)
                                                 GROUP BY ULiT_Negoptim__Contract__c, ULiT_Negoptim__Condition_Type__c];

                for (AggregateResult result : results) {
                    Id contractId = Id.valueOf(String.valueOf(result.get('ULiT_Negoptim__Contract__c')));
                    Id tarrifCondition = Id.valueOf(String.valueOf(result.get('ULiT_Negoptim__Condition_Type__c')));
                    // if the contract has a deleted NIP, but still contains other NIP in db, then remove
                    // from deletedNipConditionContracts because there's no need for update
                    if (deletedNipConditionContracts.contains(contractId) && nipIdSet.contains(tarrifCondition)) {
                        deletedNipConditionContracts.remove(contractId);
                    }
                    // if the contract has a deleted RP0 or EN0, but still contains other promo conditions in db, then remove
                    // from deletedPromoConditionContracts because there's no need for update
                    if (deletedPromoConditionContracts.contains(contractId) && promoIdSet.contains(tarrifCondition)) {
                        deletedPromoConditionContracts.remove(contractId);
                    }
                }
            }

            if (!deletedNipConditionContracts.isEmpty() || !deletedPromoConditionContracts.isEmpty()) {
                List<ULiT_Negoptim__Contract__c> contracts = [SELECT Id, SPITM_Is_Include_NIP__c, SPITM_Is_Include_Promo_Condition__c
                                                              FROM ULiT_Negoptim__Contract__c
                                                              WHERE Id IN :deletedNipConditionContracts
                                                              OR Id IN :deletedPromoConditionContracts];

                for (ULiT_Negoptim__Contract__c item : contracts) {
                    if (deletedNipConditionContracts.contains(item.Id)) {
                        item.SPITM_Is_Include_NIP__c = false;
                    }
                    if (deletedPromoConditionContracts.contains(item.Id)) {
                        item.SPITM_Is_Include_Promo_Condition__c = false;
                    }
                }
                // update contracts
                update contracts;
            }
        }
    }
}