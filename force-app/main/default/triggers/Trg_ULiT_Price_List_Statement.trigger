trigger Trg_ULiT_Price_List_Statement on ULiT_Negoptim__Price_List_Statement__c (After Update) {
    if (Trigger.isAfter && Trigger.isUpdate) {
        Trg_ULiT_Price_List_Statement_Handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}