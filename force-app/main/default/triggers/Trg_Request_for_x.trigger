trigger Trg_Request_for_x on ULiT_Negoptim__Request_For_x__c (before insert, before update) {
    /*if(Trigger.isAfter) {
        if(Trigger.isInsert || Trigger.isUpdate) {
            Set<Id> commercialPlanIds = new Set<Id>();
            for(ULiT_Negoptim__Request_For_x__c request : Trigger.new) {
                if ((Trigger.isUpdate && Trigger.OldMap.containsKey(request.Id) && request.SPITM_Commercial_Plan__c != Trigger.OldMap.get(request.Id).SPITM_Commercial_Plan__c)
                    || request.SPITM_Commercial_Plan__c != null) {
                        commercialPlanIds.add(request.SPITM_Commercial_Plan__c);
                    }
            }
            if (!commercialPlanIds.isEmpty()) {
                List<ULiT_Negoptim__Commercial_Plan_Detail__c> allCommercialPlanDetails = [SELECT Id, ULiT_Negoptim__Supplier__c, ULiT_Negoptim__Unit_Need__c, ULiT_Negoptim__Volume_Targeted__c,
                                                                                           ULiT_Negoptim__Plan_Parent__c, ULiT_Negoptim__Commercial_Event__c
                                                                                           FROM ULiT_Negoptim__Commercial_Plan_Detail__c
                                                                                           WHERE ULiT_Negoptim__Commercial_Event__c IN :commercialPlanIds];
                Map<Id, List<ULiT_Negoptim__Commercial_Plan_Detail__c>> commercialPlansMap = new Map<Id, List<ULiT_Negoptim__Commercial_Plan_Detail__c>>();
                for (ULiT_Negoptim__Commercial_Plan_Detail__c commercialPlanDetail : allCommercialPlanDetails) {
                    if (!commercialPlansMap.containsKey(commercialPlanDetail.ULiT_Negoptim__Commercial_Event__c))
                        commercialPlansMap.put(commercialPlanDetail.ULiT_Negoptim__Commercial_Event__c, new List<ULiT_Negoptim__Commercial_Plan_Detail__c>());
                    commercialPlansMap.get(commercialPlanDetail.ULiT_Negoptim__Commercial_Event__c).add(commercialPlanDetail);
                }
                Id recordTypeId = [SELECT Id FROM RecordType WHERE DeveloperName = 'Initialization' LIMIT 1]?.Id;
                List<ULiT_Negoptim__Commercial_Plan_Detail_RFP__c> commercialPlanDetailRFPList = new List<ULiT_Negoptim__Commercial_Plan_Detail_RFP__c>();
                for(ULiT_Negoptim__Request_For_x__c request : trigger.new) {
                    if (commercialPlansMap.containsKey(request.SPITM_Commercial_Plan__c)) {
                        List<ULiT_Negoptim__Commercial_Plan_Detail__c> commercialPlanDetails = commercialPlansMap.get(request.SPITM_Commercial_Plan__c);
                        for (ULiT_Negoptim__Commercial_Plan_Detail__c planDetail : commercialPlanDetails) {
                            String name = 'RFP - ' + planDetail.ULiT_Negoptim__Plan_Parent__r.Name + ' - ' + planDetail.ULiT_Negoptim__Unit_Need_ShortDesc__c;
                            ULiT_Negoptim__Commercial_Plan_Detail_RFP__c commercialPlanDetailRFP = new ULiT_Negoptim__Commercial_Plan_Detail_RFP__c(RecordTypeId = recordTypeId,
                                                                                                                                                    ULiT_Negoptim__Request_For_x__c = request.Id,
                                                                                                                                                    ULiT_Negoptim__Supplier__c = planDetail.ULiT_Negoptim__Supplier__c,
                                                                                                                                                    ULiT_Negoptim__Commercial_Event_Line__c = planDetail.Id,
                                                                                                                                                    ULiT_Negoptim__Need__c = planDetail.ULiT_Negoptim__Unit_Need__c,
                                                                                                                                                    ULiT_Negoptim__Volume_Proposed_Init__c = planDetail.ULiT_Negoptim__Volume_Targeted__c,
                                                                                                                                                    SPITM_Commercial_Plan__c = planDetail.ULiT_Negoptim__Plan_Parent__c,
                                                                                                                                                    ULiT_Negoptim__Answer_Limit__c = request.ULiT_Negoptim__Submission_Deadline__c);
                            commercialPlanDetailRFPList.add(commercialPlanDetailRFP);
                        }
                    }
                }
                insert commercialPlanDetailRFPList;
            }
        }
    }*/
    if(Trigger.isBefore) {
        if(Trigger.isInsert) {
            Set<Id> promoDetailIds = new Set<Id>();
            for (ULiT_Negoptim__Request_For_x__c request : Trigger.new) {
                if (Trigger.isInsert && request.SPITM_Promo_detail__c != null) {
                    promoDetailIds.add(request.SPITM_Promo_detail__c);
                }
            }
            Map<Id, ULiT_Negoptim__Commercial_Plan_Detail__c> commercialPlanDetails = new Map<Id, ULiT_Negoptim__Commercial_Plan_Detail__c>([SELECT Id, ULiT_Negoptim__Commercial_Event__r.Name, ULiT_Negoptim__Unit_Need_ShortDesc__c
                                                                                                                                             FROM ULiT_Negoptim__Commercial_Plan_Detail__c
                                                                                                                                             WHERE Id IN :promoDetailIds]);
            for (ULiT_Negoptim__Request_For_x__c request : Trigger.new) {
                if (commercialPlanDetails.containsKey(request.SPITM_Promo_detail__c)) {
                    request.Name = 'RFX - ' + commercialPlanDetails.get(request.SPITM_Promo_detail__c).ULiT_Negoptim__Commercial_Event__r.Name + ' - ' + commercialPlanDetails.get(request.SPITM_Promo_detail__c).ULiT_Negoptim__Unit_Need_ShortDesc__c;  
                }
            }
        }
    }
}