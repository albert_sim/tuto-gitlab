trigger Trg_ULiT_NetPrice_Statement on ULiT_Negoptim__NetPrice_Statement__c (after update) {
    if (Trigger.isAfter && Trigger.isUpdate) {
        trg_ULiT_NetPrice_Statement_Handler.onAfterUpdate(Trigger.new, Trigger.oldMap);
    }
}