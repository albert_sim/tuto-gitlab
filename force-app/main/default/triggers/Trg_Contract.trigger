/**
 * @author ULiT
 * @date 22-09-2021
 * @description Trigger Trg_Contract
 * Update contract when conso/clone/duplicate
 * Auto PE status management when To_Be_Signed status change to Signed
 * Update fields on update of top contract
 * Populate signatory contact
******************************************************************************************************************************************* */
trigger Trg_Contract on ULiT_Negoptim__Contract__c (before insert, before update, after update) {

    Map<String, Id> contractRTIds = getObjectRecordTypeMapIds(ULiT_Negoptim__Contract__c.SObjectType);
    Id toBeSignedRTId = contractRTIds.get('To_Be_Signed');
    Id signedRTId = contractRTIds.get('Signed');
    Id negoBaseInitRTId = contractRTIds.get('Nego_Base_Init');
    Id simulationRTId = contractRTIds.get('Simulation');
    Id signedCopyRTId = contractRTIds.get('Signed_Copy');
    Id periodExtensionRTId = contractRTIds.get('Period_Extension');
    Set<Id> isOnTopRecordTypes = new Set<Id>{simulationRTId, toBeSignedRTId, signedRTId, signedCopyRTId};

    // Trigger Logic
    // Before Insert or Update
    if (Trigger.isBefore) {
        if (Trigger.isInsert || Trigger.isUpdate) {
            // Update contract when conso/clone/duplicate
            List<ULiT_Negoptim__Contract__c> nContracts = Trigger.new;
            Map<Id, ULiT_Negoptim__Contract__c> oContractMap = Trigger.OldMap;
            Map<Id, ULiT_Negoptim__Contract__c> contractsToUpdate = new Map<Id, ULiT_Negoptim__Contract__c>();
            Set<Id> parentContractsIds = new Set<Id>();
            Set<Id> nsIdSet = new Set<Id>();
            Set<Id> buIdSet = new Set<Id>();
            Set<Integer> yearSet = new Set<Integer>();
            // Populate signatory contact
            Set<Id> supplierIds = new Set<Id>();
            Set<Id> isSynchroContactRecordTypesId = new Set<Id> {signedRTId, toBeSignedRTId, periodExtensionRTId, simulationRTId};
            Set<String> excludedStatusesSet = new Set<String> {'Signé Frs', 'Signed', 'Balanced', 'Closed', 'Deleted',  'Ready to publish', 'Published', 'Document Printed'};
            for (ULiT_Negoptim__Contract__c item : nContracts) {
                if (item.ULiT_Negoptim__Is_Managed_By_ExternalSource__c == true) continue;
                // Before Insert of contract with ULiT_Negoptim__Parent_Contract__c
                if (item.Id == null && item.ULiT_Negoptim__Parent_Contract__c != null) {
                    contractsToUpdate.put(item.Id, item);
                    parentContractsIds.add(item.ULiT_Negoptim__Parent_Contract__c);
                }
                // Before Insert or Update of ULiT_Negoptim__Is_OnTop_Simulation__c to TRUE
                if (item.ULiT_Negoptim__Is_OnTop_Simulation__c && (item.Id == null || (oContractMap != null && oContractMap.containsKey(item.Id)
                                                                                       && isOnTopRecordTypes.contains(item.RecordTypeId)
                                                                                       && item.ULiT_Negoptim__Is_OnTop_Simulation__c != oContractMap.get(item.Id).ULiT_Negoptim__Is_OnTop_Simulation__c)))
                {
                    buIdSet.add(item.ULiT_Negoptim__Contract_BU__c);
                    nsIdSet.add(item.ULiT_Negoptim__Supplier_Nego_Scope__c);
                    yearSet.add(item.ULiT_Negoptim__Contract_EDate__c.year());
                    String key = item.ULiT_Negoptim__Contract_BU__c + '' + item.ULiT_Negoptim__Supplier_Nego_Scope__c + '' + item.ULiT_Negoptim__Contract_EDate__c.year();
                    contractsToUpdate.put(item.Id, item);
                }
                // Before Insert - Populate signatory contact
                if (item.Id == null && isSynchroContactRecordTypesId.contains(item.RecordTypeId) && !excludedStatusesSet.contains(item.ULiT_Negoptim__Status__c)) {
                    if (item.ULiT_Negoptim__Supplier_Nego_Scope__c != null && item.ULiT_Negoptim__Supplier__c != null) {
                        nsIdSet.add(item.ULiT_Negoptim__Supplier_Nego_Scope__c);
                        supplierIds.add(item.ULiT_Negoptim__Supplier__c);
                        contractsToUpdate.put(item.Id, item);
                    }
                }
            }
            if (!contractsToUpdate.isEmpty()) {
                Map<String, ULiT_Negoptim__Contract__c> existingContractsMap = new Map<String, ULiT_Negoptim__Contract__c>();
                if((!nsIdSet.isEmpty() && !buIdSet.isEmpty() && !yearSet.isEmpty()) || !parentContractsIds.isEmpty()) {
                    for (ULiT_Negoptim__Contract__c item : [SELECT Id, RecordTypeId, RecordType.DeveloperName, ULiT_Negoptim__Contract_BU__c, ULiT_Negoptim__Supplier_Nego_Scope__c,
                                                            ULiT_Negoptim__Contract_EDate__c, ULiT_Negoptim__Status__c, ULiT_Negoptim__Is_OnTop_Simulation__c,
                                                            SPITM_AftSales_Service_LastResponse_Date__c, SPITM_AftSales_Service_Last_Request_Date__c, SPITM_Is_Annex3_Sent_By_Partner__c,
                                                            SPITM_Is_3Net_Contract__c, SPITM_Is_Excluded_To_Nego_Summary__c, SPITM_Accord_Non_Ristournble__c
                                                            FROM ULiT_Negoptim__Contract__c
                                                            WHERE ULiT_Negoptim__Status__c <> 'Deleted'
                                                            AND (Id IN :parentContractsIds
                                                                 OR (ULiT_Negoptim__Supplier_Nego_Scope__c IN :nsIdSet
                                                                     AND ULiT_Negoptim__Contract_BU__c IN :buIdSet
                                                                     AND ULiT_Negoptim__Reference_Year__c IN :yearSet
                                                                     AND RecordType.DeveloperName IN ('Simulation', 'To_Be_Signed')
                                                                     AND ULiT_Negoptim__Is_OnTop_Simulation__c = TRUE))])
                    {
                        String key = '';
                        if (parentContractsIds.contains(item.Id)) {
                            key += item.Id;
                        } else {
                            key = item.ULiT_Negoptim__Contract_BU__c + '' + item.ULiT_Negoptim__Supplier_Nego_Scope__c + '' + item.ULiT_Negoptim__Contract_EDate__c.year();
                        }
                        existingContractsMap.put(key, item);
                    }
                }
                Map<String, ULiT_Negoptim__Contact_Operational_Directory__c> contactOperationalDirectoryMap = new Map<String, ULiT_Negoptim__Contact_Operational_Directory__c>();
                if (!supplierIds.isEmpty() && !nsIdSet.isEmpty()) {
                    for(ULiT_Negoptim__Contact_Operational_Directory__c item : [SELECT Id, ULiT_Negoptim__Nego_Scope__c, ULiT_Negoptim__Supplier__c,
                                                                                ULiT_Negoptim__Contact__c, ULiT_Negoptim__Contact__r.Title
                                                                                FROM ULiT_Negoptim__Contact_Operational_Directory__c
                                                                                WHERE ULiT_Negoptim__IsActive__c = TRUE
                                                                                AND ULiT_Negoptim__IsSignatory__c = TRUE
                                                                                AND ULiT_Negoptim__Supplier__c IN :supplierIds
                                                                                AND ULiT_Negoptim__Nego_Scope__c IN :nsIdSet
                                                                                AND ULiT_Negoptim__Contact__c <> NULL])
                    {
                        contactOperationalDirectoryMap.put(String.valueOf(item.ULiT_Negoptim__Nego_Scope__c) + String.valueOf(item.ULiT_Negoptim__Supplier__c), item);
                    }
                }
                for (ULiT_Negoptim__Contract__c item : contractsToUpdate.values()) {
                    if (item.ULiT_Negoptim__Parent_Contract__c != null && existingContractsMap.containsKey(item.ULiT_Negoptim__Parent_Contract__c)) {
                        ULiT_Negoptim__Contract__c parentContract = existingContractsMap.get(item.ULiT_Negoptim__Parent_Contract__c);
                        item.SPITM_Is_3Net_Contract__c = parentContract.SPITM_Is_3Net_Contract__c;
                        item.SPITM_Is_Excluded_To_Nego_Summary__c = parentContract.SPITM_Is_Excluded_To_Nego_Summary__c;
                        item.SPITM_Accord_Non_Ristournble__c = parentContract.SPITM_Accord_Non_Ristournble__c;
                    }
                    // update fields on new top contract
                    String key = item.ULiT_Negoptim__Contract_BU__c + '' + item.ULiT_Negoptim__Supplier_Nego_Scope__c + '' + item.ULiT_Negoptim__Contract_EDate__c.year();
                    if (item.ULiT_Negoptim__Is_OnTop_Simulation__c == true && existingContractsMap.containsKey(key)) {
                        ULiT_Negoptim__Contract__c oldTopContract = existingContractsMap.get(key);
                        item.SPITM_AftSales_Service_LastResponse_Date__c = oldTopContract.SPITM_AftSales_Service_LastResponse_Date__c;
                        item.SPITM_AftSales_Service_Last_Request_Date__c = oldTopContract.SPITM_AftSales_Service_Last_Request_Date__c;
                        item.SPITM_Is_Annex3_Sent_By_Partner__c = oldTopContract.SPITM_Is_Annex3_Sent_By_Partner__c;
                    }
                    // Populate signatory contact
                    key = String.valueOf(item.ULiT_Negoptim__Supplier_Nego_Scope__c) + String.valueOf(item.ULiT_Negoptim__Supplier__c);
                    if(!contactOperationalDirectoryMap.isEmpty() && contactOperationalDirectoryMap.containsKey(key)) {
                        ULiT_Negoptim__Contact_Operational_Directory__c contactOperationalDirectory = contactOperationalDirectoryMap.get(key);
                        item.ULiT_Negoptim__PartnerSigned_By__c = contactOperationalDirectory.ULiT_Negoptim__Contact__c;
                        item.ULiT_Negoptim__PartnerSigned_Title__c = contactOperationalDirectory.ULiT_Negoptim__Contact__r.Title;
                    }
                }
            }
        }
    }
    // After Insert or Update
    if (Trigger.isAfter) {
        if (Trigger.isUpdate) {
            // Auto PE status management when ToBeSigned status change to Signed
            List<ULiT_Negoptim__Contract__c> nContracts = Trigger.new;
            Map<Id, ULiT_Negoptim__Contract__c> oContractMap = Trigger.OldMap;
            Set<Id> nsIdSet = new Set<Id>();
            Set<Id> buIdSet = new Set<Id>();
            Set<Integer> yearSet = new Set<Integer>();
            Map<String, ULiT_Negoptim__Contract__c> negoBaseInitMap = new Map<String, ULiT_Negoptim__Contract__c>();
            for (ULiT_Negoptim__Contract__c item : nContracts) {
                if(item.ULiT_Negoptim__Is_Managed_By_ExternalSource__c == true) continue;
                //After Update of ULiT_Negoptim__Status__c to Signed on To_Be_Signed/Signed contract
                if (oContractMap != null && oContractMap.containsKey(item.Id)
                    && (item.RecordTypeId == toBeSignedRTId || item.RecordTypeId == signedRTId)
                    && item.ULiT_Negoptim__Status__c != oContractMap.get(item.Id).ULiT_Negoptim__Status__c
                    && item.ULiT_Negoptim__Status__c == 'Signed')
                {
                    nsIdSet.add(item.ULiT_Negoptim__Supplier_Nego_Scope__c);
                    buIdSet.add(item.ULiT_Negoptim__Contract_BU__c);
                    yearSet.add(Integer.valueOf(item.ULiT_Negoptim__Reference_Year__c));
                }
                //After Update of ULiT_Negoptim__Status__c to Validated on Nego_Base_Init contract
                if (oContractMap != null && oContractMap.containsKey(item.Id)
                    && item.RecordTypeId == negoBaseInitRTId
                    && item.ULiT_Negoptim__Status__c != oContractMap.get(item.Id).ULiT_Negoptim__Status__c
                    && item.ULiT_Negoptim__Status__c == 'Validated')
                {
                    nsIdSet.add(item.ULiT_Negoptim__Supplier_Nego_Scope__c);
                    buIdSet.add(item.ULiT_Negoptim__Contract_BU__c);
                    yearSet.add(Integer.valueOf(item.ULiT_Negoptim__Reference_Year__c));
                    String key = item.ULiT_Negoptim__Contract_BU__c + '' + item.ULiT_Negoptim__Supplier_Nego_Scope__c + '' + item.ULiT_Negoptim__Contract_EDate__c.year();
                    negoBaseInitMap.put(key, item);
                }
            }
            if (!nsIdSet.isEmpty() && !buIdSet.isEmpty() && !yearSet.isEmpty()) {
                //For contract with RT = Period_Extension AND (SPITM_Accord_Non_Ristournble__c = TRUE or SPITM_Is_3Net_Contract__c = TRUE or SPITM_Is_Excluded_To_Nego_Summary__c)
                // + (same NS + same BU + Same contract year)
                //IF Status = In preparation or Validated
                //update automatically to 'Rien à Signer'
                List<ULiT_Negoptim__Contract__c> contractsToUpdate = new List<ULiT_Negoptim__Contract__c>();
                Set<Id> negoBaseSet = new Set<Id>();
                Set<Id> negoBaseInitSet = new Set<Id>();
                for (ULiT_Negoptim__Contract__c item : [SELECT Id, RecordType.DeveloperName, ULiT_Negoptim__Contract_BU__c, ULiT_Negoptim__Supplier_Nego_Scope__c,ULiT_Negoptim__Contract_EDate__c
                                                        FROM ULiT_Negoptim__Contract__c
                                                        WHERE ULiT_Negoptim__Supplier_Nego_Scope__c IN :nsIdSet
                                                        AND ULiT_Negoptim__Contract_BU__c IN :buIdSet
                                                        AND ULiT_Negoptim__Reference_Year__c IN :yearSet
                                                        AND ULiT_Negoptim__Status__c <> 'Deleted'
                                                        AND ((RecordType.DeveloperName = 'Period_Extension'
                                                            AND (SPITM_Accord_Non_Ristournble__c = TRUE OR SPITM_Is_3Net_Contract__c = TRUE OR SPITM_Is_Excluded_To_Nego_Summary__c = TRUE)
                                                            AND ULiT_Negoptim__Status__c IN ('In preparation', 'Validated'))
                                                            OR (RecordType.DeveloperName = 'Nego_Base'))])
                {
                    String key = item.ULiT_Negoptim__Contract_BU__c + '' + item.ULiT_Negoptim__Supplier_Nego_Scope__c + '' + item.ULiT_Negoptim__Contract_EDate__c.year();
                    // case 1: update status PE
                    if (item.RecordType.DeveloperName == 'Period_Extension') {
                        item.ULiT_Negoptim__Status__c = 'SPITM_Rien_A_Signer';
                        contractsToUpdate.add(item);
                    }
                    // case 2: execute move condition batch
                    if (item.RecordType.DeveloperName == 'Nego_Base' && negoBaseInitMap.containsKey(key)) {
                        negoBaseSet.add(item.Id);
                        negoBaseInitSet.add(negoBaseInitMap.get(key).Id);
                    }
                }
                if (!negoBaseSet.isEmpty() && !negoBaseInitSet.isEmpty()) {
                    Database.executeBatch(new MoveContractConditionsBatch('Trg_Contract', new List<Id>(negoBaseInitSet), new List<Id>(negoBaseSet)), 10);
                }
                if (!contractsToUpdate.isEmpty()) {
                    update contractsToUpdate;
                }
            }
        }
    }
	/**
     * Get Object RecordType Map of Name/Id
     * @param sObjectType
     * */
    public static Map<String, Id> getObjectRecordTypeMapIds(SObjectType sObjectType) {
        Map<String, Id> rtMap = new Map<String, Id>();
        // If sObjectType is wrong, then an Exception is thrown.
        String sObjectName = sObjectType.getDescribe().getName();
        // Check Accessibility.
        if(!checkAccessibilityFields(Schema.SObjectType.RecordType.fields.getMap(), new String [] {'Id', 'SobjectType', 'DeveloperName', 'IsActive'})) {
        	return rtMap;
        }
        List<RecordType> rtList = [SELECT Id, DeveloperName FROM RecordType WHERE SobjectType = :sObjectName AND IsActive = true];
        for(RecordType item : rtList) {
            rtMap.put(item.DeveloperName, item.Id);
        }
        //Return the record type id
        return rtMap;
    }

    /**
     * Check Field Level Security for reading
     * @param fieldsMap
     * */
    public static Boolean checkAccessibilityFields(Map<String, SObjectField> fieldsMap, String[] fields) {
        Boolean result = true;
        for (String field : fields) {
            // Check if the user has read access on each field
            if (fieldsMap.get(field) == null || !fieldsMap.get(field).getDescribe().isAccessible()) {
                result = false;
                throw new NoAccessException('Insufficient access to read ' + field);
            }
        }
        return result;
    }
    public class NoAccessException extends Exception {}
}