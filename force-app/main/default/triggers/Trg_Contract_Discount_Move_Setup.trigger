/**
 * @author ULiT
 * @date 26-08-2021
 * @description Trigger on ULiT_Negoptim__Contract_Discount_Move_Setup__c to manage execute Mass Move preparation of conditions
******************************************************************************************************************************************* */
trigger Trg_Contract_Discount_Move_Setup on ULiT_Negoptim__Contract_Discount_Move_Setup__c (after update) {

    if (trigger.isAfter) {
        if (trigger.isUpdate) {
            Trg_Contract_Discount_Move_Setup_Handler.OnAfterUpdate(trigger.new, trigger.OldMap);
        }
    }
}