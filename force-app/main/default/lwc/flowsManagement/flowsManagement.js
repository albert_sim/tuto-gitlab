import { LightningElement, track, wire } from 'lwc';

import getFlows from '@salesforce/apex/FlowsControlPanelController.getFlows';
import disableFlow from '@salesforce/apex/FlowsControlPanelController.disableFlow';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
export default class FlowsManagement extends LightningElement {
    @track loading = true;
    @track flowData = [];
    @track objectNameOptions = [];
    @track selectedObject = '';
    AllData = new Map();
    AllFlows = new Map();
    connectedCallback() {
    }

    @wire(getFlows) getFlows({ error, data }) {
        if (data) {
            this.objectNameOptions = [];
            let objectNames = [];
            let dataParse = JSON.parse(data);
            for (let flow of dataParse) {
                if (!this.AllData.get(flow.objectName)) {
                    this.AllData.set(flow.objectName, []);
                }
                flow.isActive = flow.Status == 'Active';
                this.AllData.get(flow.objectName).push(flow);
                if (!this.AllFlows.get(flow.DefinitionId)) {
                    this.AllFlows.set(flow.DefinitionId, []);
                }
                this.AllFlows.get(flow.DefinitionId).push(flow);
                if (!objectNames.includes(flow.objectName)) objectNames.push(flow.objectName);
            }
            objectNames.sort();
            let options = [];
            for (let objectName of objectNames) {
                let row = {};
                if (objectName != null) {
                    row = { value: objectName, label: objectName };
                    options.push(row);
                }
            }
            if (objectNames.includes(null)) {
                this.objectNameOptions = [{ label: '--None--', value: null }].concat(options);
            } else {
                this.objectNameOptions = options;
            }

            this.selectedObject = this.objectNameOptions[0].value;
            this.flowData = [];
            this.flowData = this.AllData.get(this.selectedObject);
            this.loading = false;
        } else if (error) {
            this.handleToastMessage('Error', error.body.message, 'error');
        }
    }

    handleChangeObject(event) {
        this.selectedObject = event.target.value;
        this.flowData = [];
        this.flowData = this.AllData.get(this.selectedObject);
    }

    disableFlow(event) {
        let form = event.target.dataset.form;
        let flow = this.flowData.filter((flow) => flow.Id == form)[0];
        let indexFlow = this.flowData.indexOf(flow);
        let copyFlowData = [...this.flowData];
        let copyFlow = copyFlowData[indexFlow];
        this.loading = true;
        var r = confirm(copyFlow.isActive ? 'Do you want to deactivate the Flow?!' : 'Do you want to activate the Flow?!');
        if (r == true) {
            disableFlow({ definitionId: copyFlow.DefinitionId, activeVersion: copyFlow.isActive ? 0 : parseInt(copyFlow.VersionNumber) }).then(result => {
                if (result == '') {
                    copyFlow.isActive = !copyFlow.isActive;
                    copyFlow.Status = 'Obsolete';
                    copyFlowData[indexFlow] = copyFlow;
                    this.flowData = copyFlowData;
                    this.AllData[this.selectedObject] = copyFlowData;
                    if (this.AllFlows.get(copyFlow.DefinitionId).length > 1 && copyFlow.isActive) {
                        for (let versionFlow of this.AllFlows.get(copyFlow.DefinitionId)) {
                            if (versionFlow.isActive && versionFlow.Id != form) {
                                versionFlow.isActive = false;
                                versionFlow.Status = 'Obsolete';
                                const element = this.template.querySelector("[data-id=\'" + versionFlow.Id + "\']");
                                element.checked = versionFlow.isActive;
                            }
                        }
                    }
                    this.loading = false;
                } else {
                    let resultParse = JSON.parse(result);
                    if (resultParse && resultParse[0].hasOwnProperty('message')) {
                        this.handleToastMessage('Error', resultParse[0].message, 'error');
                        const element = this.template.querySelector("[data-id=\'" + form + "\']");
                        element.checked = flow.isActive;
                        this.loading = false;
                    }
                }
            });
        } else {
            const element = this.template.querySelector("[data-id=\'" + form + "\']");
            element.checked = flow.isActive;
            this.loading = false;
        }
    }

    handleToastMessage(title, message, variant) {
        if (typeof this.sforce === 'undefined') {
            this.dispatchEvent(new ShowToastEvent({
                title: title,
                message: message,
                variant: variant
            }));
        } else {
            confirm(message);
        }
    }
}