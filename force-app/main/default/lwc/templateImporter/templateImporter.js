import { LightningElement, track } from 'lwc';
import importTemplates from '@salesforce/apex/templateImporterCmpController.importTemplates';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

export default class TemplateImporter extends LightningElement {
    @track files;

    handleFilesSelected(event) {
        let fr = new FileReader();
        fr.onload = function(){
            importTemplates({
                data : fr.result
            }).then( result => {
                console.log('Done')
            }).catch(e => {
                console.log('Failed');
            })
        }
        fr.readAsText(event.target.files[0]);
    }
}