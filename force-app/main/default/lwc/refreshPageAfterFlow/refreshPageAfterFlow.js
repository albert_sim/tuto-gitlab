/**
 * ULIT DEV
 * This component is used in community flows in order to refresh the current page they've been launched from
 * 
 */

import { LightningElement, api } from 'lwc';

export default class RefreshPageAfterFlow extends LightningElement {
	@api recordId;
    connectedCallback() {
        location.reload(); 
    }
}