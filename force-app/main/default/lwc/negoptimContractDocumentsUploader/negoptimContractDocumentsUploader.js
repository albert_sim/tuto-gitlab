/**
* @author ULiT
* @description LWC to upload file to contract and create contract documents from these files
**************************************************************************************************************************** */
import { LightningElement, track, api, wire } from 'lwc';
import createContractDocument from '@salesforce/apex/ContractDocumentUploaderController.createContractDocument';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';

//labels
import LBL_Contract_Document_Uploader_Title from '@salesforce/label/c.LBL_Contract_Document_Uploader_Title';
import MSG_Contract_Documents_Uploaded from '@salesforce/label/c.MSG_Contract_Documents_Uploaded';
export default class NegoptimContractDocumentsUploader extends LightningElement {
    /**
     * Variables
    */
    @api recordId;
    @track globalLoading = false;
    // All Labels
    labels = {
        LBL_Contract_Document_Uploader_Title,
        MSG_Contract_Documents_Uploaded
    }
    /**
     * Getters
    */
    /**
     * Handlers
    */
    handleUploadFinished(event) {
        this.globalLoading = true;
        console.log(JSON.stringify(event.detail.files));
        createContractDocument({
            uploadedFiles : JSON.stringify(event.detail.files),
            contractId : this.recordId
        }).then(result => {
            this.globalLoading = false;
            this.dispatchEvent(new ShowToastEvent({
                title: "",
                message: this.labels.MSG_Contract_Documents_Uploaded,
                variant: 'success'
            }));
            eval("$A.get('e.force:refreshView').fire();");
        }).catch(error => {
            this.globalLoading = false;
            this.dispatchEvent(new ShowToastEvent({
                title: "Error",
                message: error.body.message,
                variant: 'error'
            }));
        });
    }
}