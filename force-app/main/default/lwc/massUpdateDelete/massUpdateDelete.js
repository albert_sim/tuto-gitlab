/**
 * @author ULIT
 * @date 18/12/2020
 * @description JS for negOptim mass update delete LWC to allow deletion or update of a set of object records at once
 * */
import { LightningElement, wire, track } from "lwc";
import { NavigationMixin } from 'lightning/navigation';
import { ShowToastEvent } from "lightning/platformShowToastEvent";
import getAllObjectNames from "@salesforce/apex/MassUpdateDeleteCmpController.getAllObjectNames";
import getObjectFields from "@salesforce/apex/MassUpdateDeleteCmpController.getObjectFields";
import getBatchDetails from "@salesforce/apex/MassUpdateDeleteCmpController.getBatchDetails";
import executeUpdateDelete from "@salesforce/apex/MassUpdateDeleteCmpController.executeUpdateDelete";
import getStatusOfBatch from "@salesforce/apex/MassUpdateDeleteCmpController.getStatusOfBatch";
import getLogsDetails from "@salesforce/apex/MassUpdateDeleteCmpController.getLogsDetails";
import getFirstLogs from "@salesforce/apex/MassUpdateDeleteCmpController.getFirstLogs";
import getCountOfTargetedRecords from "@salesforce/apex/MassUpdateDeleteCmpController.getCountOfTargetedRecords";
import getObjectRecordTypes from "@salesforce/apex/MassUpdateDeleteCmpController.getObjectRecordTypes";


// object
import Log_Object from "@salesforce/schema/ULiT_Negoptim__Log__c";
// field
import ApexJobId from '@salesforce/schema/ULiT_Negoptim__Log__c.ULiT_Negoptim__Apex_Job_ID__c';
import TotalJobItems from '@salesforce/schema/ULiT_Negoptim__Log__c.ULiT_Negoptim__TotalJobItems__c';
import JobItemsProcessed from '@salesforce/schema/ULiT_Negoptim__Log__c.ULiT_Negoptim__JobItemsProcessed__c';
import Status from '@salesforce/schema/ULiT_Negoptim__Log__c.ULiT_Negoptim__Status__c';
import NumberOfErrors from '@salesforce/schema/ULiT_Negoptim__Log__c.ULiT_Negoptim__NumberOfErrors__c';
import NumberOfWarnings from '@salesforce/schema/ULiT_Negoptim__Log__c.ULiT_Negoptim__NumberOfWarnings__c';

import { getObjectInfo } from 'lightning/uiObjectInfoApi';

// custom Label
import LBL_Save from "@salesforce/label/c.LBL_Save";
import LBL_Edit from "@salesforce/label/c.LBL_Edit";
import LBL_Close from '@salesforce/label/c.LBL_Close';
import LBL_Cancel from '@salesforce/label/c.LBL_Cancel';
import LBL_Confirm from '@salesforce/label/c.LBL_Confirm';
import LBL_Object from "@salesforce/label/c.LBL_Object";
import LBL_Object_Fields from "@salesforce/label/c.LBL_Object_Fields";
import LBL_Execute from "@salesforce/label/c.LBL_Execute";
import LBL_Operator from "@salesforce/label/c.LBL_Operator";
import LBL_Value from "@salesforce/label/c.LBL_Value";
import LBL_Add from "@salesforce/label/c.LBL_Add";
import LBL_MassUpdateDelete from "@salesforce/label/c.LBL_MassUpdateDelete";
import LBL_NoFilter from "@salesforce/label/c.LBL_NoFilter";
import LBL_Progress from "@salesforce/label/c.LBL_Progress";
import LBL_Operation from "@salesforce/label/c.LBL_Operation";
import MSG_No_Filter from "@salesforce/label/c.MSG_No_Filter";

export default class MassDeleteComponent extends NavigationMixin(LightningElement) {
  @track objectList = [];
  @track fieldsList = [];
  @track updatableFieldsList = [];
  @track criteriaList = [];
  @track editMode = false;
  @track updateSection = false;
  @track fieldsToUpdate = [];
  @track CustomFilter = false;
  @track massOption = "Delete";
  @track Operators = [];
  @track loading = true;
  @track noFilterWarning = false;
  @track continue = true;
  @track openSettings = false;
  @track batchSize = 50;
  @track log = "";
  @track runningBatches = [];
  @track tableStyleHeight = '';
  @track options = [{ label: "Mass delete", value: "Delete" }, { label: "Mass update", value: "Update" }];
  @track operatorsInner = [{ value: "=", label: "=" }, { value: "<>", label: "<>" }, { value: ">", label: ">" }, { value: "<", label: "<" }];
  @track object;
  @track fieldSelected;
  @track fieldToUpdateSelected;
  @track massRecordsLength = '0';
  @track confirmationRequest = false;
  @track loadingLogs = false;
  @track recordTypeList = [];

  filter = "";
  filterType = "AND";
  apexJobId = "";
  numberOperators = [{ value: "=", label: "=" }, { value: "<>", label: "<>" }, { value: ">", label: ">" }, { value: "<", label: "<" }];
  stringOperators = [{ value: "=", label: "=" }, { value: "<>", label: "<>" }, { value: "Contains", label: "Contains" }, { value: "Not Contains", label: "Not Contains" }];
  stringFieldTypes = ["STRING", "REFERENCE", "PICKLIST", "TEXT", "TEXTAREA", "CHECKBOX", "PHONE", "URL", "ID"];
  booleanOperators = [{ value: "=", label: "=" }, { value: "<>", label: "<>" }]
  // All Labels
  Label = {
    LBL_Save,
    LBL_Edit,
    LBL_Close,
    LBL_Cancel,
    LBL_Confirm,
    LBL_Object,
    LBL_Object_Fields,
    LBL_Execute,
    LBL_Add,
    LBL_MassUpdateDelete,
    LBL_Operator,
    LBL_Value,
    LBL_NoFilter,
    LogLabels: {},
    LBL_Progress,
    LBL_Operation,
    MSG_No_Filter
  };
  @wire(getObjectInfo, { objectApiName: Log_Object })
  wiredOI({ error, data }) {
    var logFields = {};
    if (data) {
      logFields = {
        "ApexJobId": data.fields[ApexJobId.fieldApiName].label,
        "TotalJobItems": data.fields[TotalJobItems.fieldApiName].label,
        "JobItemsProcessed": data.fields[JobItemsProcessed.fieldApiName].label,
        "Status": data.fields[Status.fieldApiName].label,
        "NumberOfErrors": data.fields[NumberOfErrors.fieldApiName].label,
        "NumberOfWarnings": data.fields[NumberOfWarnings.fieldApiName].label
      }
    }
    this.Label.LogLabels = logFields;
  }
  connectedCallback() {
    getAllObjectNames({}).then(result => {
      if (result) {
        let tmp = [];
        for (let x of result) {
          let v = { label: x, value: x, isVisible: true };
          tmp.push(v);
        }
        this.objectList = tmp;
      }
      this.objectList.sort((a, b) => (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0));
      this.loading = false;
    })
    this.loadingLogs = true;
    this.getFirstLogsHandler();
    window.addEventListener('resize', this.changeHeight);
  }
  renderedCallBack(){
    this.loading = true;
    this.changeHeight();
    this.loading = false;
  }
  getFirstLogsHandler() {
    getFirstLogs({}).then(result => {
      this.runningBatches = [];
      for (var res of result) {
        res.url = '/' + res.Id;
        let op = res.ULiT_Negoptim__Started_From__c.split('_')[1];
        if (res.ULiT_Negoptim__TotalJobItems__c != 0) {
          this.runningBatches.push({
            batchId: res.ULiT_Negoptim__Apex_Job_ID__c, TotalJobItems: res.ULiT_Negoptim__TotalJobItems__c, JobItemsProcessed: res.ULiT_Negoptim__JobItemsProcessed__c,
            status: res.ULiT_Negoptim__Status__c, operation: op, progress: (res.ULiT_Negoptim__JobItemsProcessed__c / res.ULiT_Negoptim__TotalJobItems__c) * 100, log: res, operationIcon : op == "Delete" ? 'action:delete' : 'action:edit'
          });
        }
        else {
          this.runningBatches.push({
            batchId: res.ULiT_Negoptim__Apex_Job_ID__c, TotalJobItems: res.ULiT_Negoptim__TotalJobItems__c, JobItemsProcessed: res.ULiT_Negoptim__JobItemsProcessed__c,
            status: res.ULiT_Negoptim__Status__c, operation: op, progress: 100, log: res, operationIcon : op == "Delete" ? 'action:delete' : 'action:edit'
          });
        }
      }
      this.loadingLogs = false;
      this.setTableDimension();
    })
    window.addEventListener('resize', this.changeHeight);
  }
  
  setTableDimension(){
    let header = this.template.querySelector('.header').getBoundingClientRect().height;
    let tableContainer = this.template.querySelector('.tableContainer').getBoundingClientRect().height;
    let logTableHeight = window.innerHeight - tableContainer - header - 200;
    this.tableStyleHeight = 'height:' + logTableHeight + 'px';
  }
  changeHeight = () => this.setTableDimension();
 
  monitorBatches() {
    if (this.runningBatches.length > 0) {
      var incompleteBatches = false;
      for (var batch of this.runningBatches) {
        if ((batch.status != "Completed" && batch.status != 'Failed') 
        || (batch.status == "Completed" && batch.log == null)
        || (batch.status == "Failed" && batch.log == null)
        || (batch.status == "Aborted" && batch.log == null)) 
        {
          incompleteBatches = true;
        }
      }
      if (incompleteBatches) {
        let batchToGetLogs = [];
        let self = this;
        let counter = 0;
        var allCompleted = false;
        
        var interval = setInterval(() => {
          getBatchDetails({ batchIdList: JSON.stringify(this.runningBatches.map(obj => { return obj.batchId })) }).then(result => {
            for (var batch of result) {
              if (batch.Status != 'Completed' && batch.Status != 'Failed') {
                allCompleted = false;
                if (batch.TotalJobItems > 0) {
                  this.runningBatches.filter(obj => obj.batchId == batch.Id)[0].progress = (batch.JobItemsProcessed * 100) / batch.TotalJobItems;
                }
              } else {
                allCompleted = true;
                this.runningBatches.filter(obj => obj.batchId == batch.Id)[0].progress = 100;
                batchToGetLogs.push(batch.Id);
              }
              this.runningBatches.filter(obj => obj.batchId == batch.Id)[0].status = batch.Status;
              this.runningBatches.filter(obj => obj.batchId == batch.Id)[0].TotalJobItems = batch.TotalJobItems;
              this.runningBatches.filter(obj => obj.batchId == batch.Id)[0].JobItemsProcessed = batch.JobItemsProcessed;
            } 
          }).catch((error)=>{
            console.log(error);
          });
          counter++;
          if (allCompleted || counter > 15) {
            self.getLogsDetailsFunction(batchToGetLogs);
            this.loadingLogs = false;
            clearInterval(interval);
          }
        }, 2000);
      }
      this.loadingLogs = false;
    }
  }
  getLogsDetailsFunction(batchIds) {
    let self = this;
    setTimeout(function () {
      getLogsDetails({ apexJobIds: JSON.stringify(batchIds) }).then(result => {
        for (var log of result) {
          log.url = '/' + log.Id;
          self.runningBatches.filter(obj => obj.batchId == log.ULiT_Negoptim__Apex_Job_ID__c)[0].log = log;
        }
      });
    }, 800);
  }

  handleRefreshLogs() {
    this.loadingLogs = true;
    this.getFirstLogsHandler();
    //  if (this.runningBatches.length > 0) {
    //    for (var batch of this.runningBatches) {
    //      if ((batch.status == "Completed" || batch.Status == 'Failed' || batch.Status == 'Aborted') && batch.log != null) {
    //        batch.TotalJobItems = batch.log.ULiT_Negoptim__TotalJobItems__c;
    //        batch.JobItemsProcessed = batch.log.ULiT_Negoptim__JobItemsProcessed__c;
    //      }
    //    }
    //  }
  }
  //toggle between options(DELETE or UPDATE)
  handleChangeMassOption(event) {
    this.massOption = event.target.value;
    //  if (this.massOption == 'Update') this.updateSection = true;
    this.updateSection = this.massOption == 'Update' ? true : false;
  }

  //Select Object to get its fields
  handleObjectSelected(event) {
    this.massRecordsLength = 0;
    this.fieldsList = [];
    this.criteriaList = [];
    this.fieldsToUpdate = [];
    this.updatableFieldsList = [];
    this.object = event.target.value;
    this.loading = true;
    getObjectRecordTypes({ objectName: this.object }).then(result => {
      let resultMap = JSON.parse(JSON.stringify(result))
      for (var recordtype of Object.keys(resultMap)) {
        this.recordTypeList.push({ value: recordtype, label: resultMap[recordtype] });
      }
    });
    getObjectFields({ objectName: this.object }).then((result) => {
      if (result) {
        this.fieldsList = JSON.parse(result).map(item => { return { value: item.fieldName, label: item.fieldName, type: item.fieldType, isUpdateable: item.isUpdateable, inEdit: false }; });
        for (var field of this.fieldsList) {
          field.isRecordType = field.value == 'RecordTypeId';
          field.isDate = field.type == 'DATE';
          field.isDateTime = field.type == 'DATETIME';
          field.isBoolean = field.type == 'BOOLEAN';
          field.isNumber = (field.type == 'CURRENCY' || field.type == 'PERCENT' || field.type == 'NUMBER');
          field.isText = (!field.isBoolean && !field.isDateTime && !field.isDate && !field.isNumber);
        }
      }
      this.updatableFieldsList = this.fieldsList.filter(obj => obj.isUpdateable == true);
      this.updatableFieldsList.sort((a, b) => (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0));
      this.fieldsList.sort((a, b) => (a.label > b.label) ? 1 : ((b.label > a.label) ? -1 : 0));
      this.loading = false;
    }).catch(error => {
      console.log(error);
      this.loading = false;
    });
  }

  //Add criteria to filter records of objects
  handleAddCriteria() {
    this.loading = true;
    let FieldName = this.template.querySelector(".FieldName").value != undefined ? this.template.querySelector(".FieldName").value : "";
    let Operator = this.template.querySelector(".Operator").value != undefined ? this.template.querySelector(".Operator").value : "";
    let DisplayedValue = this.template.querySelector(".Value").value;
    if (FieldName != "" && Operator != "") {
      let Value = this.template.querySelector(".Value").value;
      if (!this.stringFieldTypes.includes(this.fieldSelected.type)) {
        if (this.fieldSelected.type == 'BOOLEAN' && (DisplayedValue == "" || Value.toLowerCase == "null")) {
          Value = false;
          DisplayedValue = "false";
        } else if (DisplayedValue == "" || Value.toLowerCase == "null") {
          DisplayedValue = "null";
          Value = "null";
        }
      } else if (this.stringFieldTypes.includes(this.fieldSelected.type) && Value.toLowerCase != "null") Value = "'" + Value + "'";
      this.criteriaList.push({ FieldName: FieldName, Operator: Operator, DisplayedOperator: this.Operators.filter((obj) => obj.value == Operator)[0].label, DisplayedValue: DisplayedValue, Value: Value, inEdit: false });
      this.template.querySelector(".FieldName").value = "";
      this.template.querySelector(".Operator").value = "";
      this.fieldSelected = null;
      this.template.querySelector(".Value").value = "";
      this.buildQueryFilter();
      getCountOfTargetedRecords({ objName: this.object, filter: this.filter }).then(result => {
        this.massRecordsLength = result;
      });
    }
    else this.handleToastMessage("Error", "Filter values are not filled", "error");
    this.loading = false;
  }

  //compose the querry to get recordsbased on criterias entered
  buildQueryFilter() {
    this.filter = "";
    if (this.criteriaList.length > 0) {
      if (this.criteriaList.length > 1) {
        for (var i = 0; i < this.criteriaList.length - 1; i++) {
          if (this.criteriaList[i].Operator == "Contains") {
            this.filter += this.criteriaList[i].FieldName + " LIKE \'%" + this.criteriaList[i].DisplayedValue + "%\' " + this.filterType + " ";
          }
          else if (this.criteriaList[i].Operator == "Not Contains") {
            this.filter += 'NOT ' + this.criteriaList[i].FieldName + " LIKE \'%" + this.criteriaList[i].DisplayedValue + "%\' " + this.filterType + " ";
          } else {
            this.filter += this.criteriaList[i].FieldName + " " + this.criteriaList[i].Operator + " "
              + this.criteriaList[i].Value + " " + this.filterType + " ";
          }
        }
      }
      if (this.criteriaList[this.criteriaList.length - 1].Operator == "Contains") {
        this.filter += this.criteriaList[this.criteriaList.length - 1].FieldName + " LIKE \'%" + this.criteriaList[this.criteriaList.length - 1].DisplayedValue + "%\' ";
      }
      else if (this.criteriaList[this.criteriaList.length - 1].Operator == "Not Contains") {
        this.filter += 'NOT ' + this.criteriaList[this.criteriaList.length - 1].FieldName + " LIKE \'%" + this.criteriaList[this.criteriaList.length - 1].DisplayedValue + "%\' ";
      } else {
        this.filter += this.criteriaList[this.criteriaList.length - 1].FieldName + " " + this.criteriaList[this.criteriaList.length - 1].Operator + " "
          + this.criteriaList[this.criteriaList.length - 1].Value;
      }
    }
    console.log('filter is ' + this.filter);
  }

  //Detect the selected field to display the corresponding operators
  handleFieldSelected(event) {
    let fieldName = event.target.value;
    this.fieldSelected = this.fieldsList.filter((obj) => obj.label == fieldName)[0];
    // if (this.template.querySelector(".Value") != undefined) {
    //   this.template.querySelector(".Value").value = "";
    // }
    if (this.stringFieldTypes.includes(this.fieldSelected.type)) {
      this.Operators = this.stringOperators;
    } else if (this.fieldSelected.type == 'BOOLEAN') {
      this.Operators = this.booleanOperators;
    } else {
      this.Operators = this.numberOperators;
    }
  }
  handleFieldToUpdateSelected(event) {
    try {
      let fieldName = event.target.value;
      this.fieldToUpdateSelected = this.fieldsList.filter((obj) => obj.label == fieldName)[0];
      // if (this.template.querySelector(".UpdatedValue") != undefined) {
      //   this.template.querySelector(".UpdatedValue").value = "";
      // }
      if (this.stringFieldTypes.includes(this.fieldToUpdateSelected.type)) this.Operators = this.stringOperators;
      else if (this.fieldToUpdateSelected.type == 'BOOLEAN') {
        this.Operators = this.booleanOperators;
      } else {
        this.Operators = this.numberOperators;
      }
    } catch(error){
      console.error(error);
    }
  }
  //Detect the selected field -- when entering criteria or update -- to display the corresponding operators
  handleInnerFieldSelected(event) {
    let fieldName = event.target.value;
    let fieldInnerSelected = this.fieldsList.filter((obj) => obj.label == fieldName)[0];
    if (this.stringFieldTypes.includes(fieldInnerSelected.type)) {
      this.operatorsInner = this.stringOperators;
    } else if (fieldInnerSelected.type == 'BOOLEAN') {
      this.operatorsInner = this.booleanOperators;
    } else {
      this.operatorsInner = this.numberOperators;
    }
  }

  //Delete a row from the criterias
  handleDeleteCriteria(event) {
    if (event.target.dataset.item != undefined) {
      let toDeleteCriteria = event.target.dataset.item;
      this.criteriaList.splice(parseInt(toDeleteCriteria), 1);
      this.loading = true;
      this.buildQueryFilter();
      getCountOfTargetedRecords({ objName: this.object, filter: this.filter }).then(result => {
        this.massRecordsLength = result;
        this.loading = false;
      });
    }
  }

  // to call toast message
  handleToastMessage(title, message, variant) {
    if (typeof this.sforce === "undefined") this.dispatchEvent(new ShowToastEvent({ title: title, message: message, variant: variant }));
    else confirm(message);
  }

  //to edit a row in criterias table
  handleEditClick(event) {
    if (event.target.dataset.item != undefined) {
      let toEditCriteria = parseInt(event.target.dataset.item);
      this.editMode = true;
      this.fieldSelected = this.fieldsList.filter((obj) => obj.value == this.criteriaList[toEditCriteria].FieldName)[0];
      this.criteriaList[toEditCriteria].inEdit = true;
      if (this.stringFieldTypes.includes(this.fieldSelected.type)) {
        this.operatorsInner = this.stringOperators;
      } else if (this.fieldSelected.type == 'BOOLEAN') {
        this.operatorsInner = this.booleanOperators;
      } else {
        this.operatorsInner = this.numberOperators;
      }
    }
  }

  //to cancel edition of a criteria
  handleCancelEdit(event) {
    if (event.target.dataset.item != undefined) {
      let index = parseInt(event.target.dataset.item);
      this.editMode = false;
      if (this.criteriaList[index] != undefined) this.criteriaList[index].inEdit = false;
      if (this.fieldsToUpdate[index] != undefined) this.fieldsToUpdate[index].inEdit = false;
    }
  }

  //confirm the changes after edit of criteria
  handleConfirmEdit(event) {
    if (event.target.dataset.item != undefined) {
      this.loading = true;
      this.criteriaList[event.target.dataset.item].FieldName = this.template.querySelector(".FieldNameInner").value;
      this.criteriaList[event.target.dataset.item].Operator = this.template.querySelector(".OperatorInner").value;
      this.criteriaList[event.target.dataset.item].DisplayedOperator = this.Operators.filter((obj) => obj.value == this.template.querySelector(".OperatorInner").value)[0].label;
      this.criteriaList[event.target.dataset.item].DisplayedValue = this.template.querySelector(".ValueInner").value;
      let fieldType = this.fieldsList.filter((obj) => obj.label == this.template.querySelector(".FieldNameInner").value)[0].type
      let Value = this.template.querySelector(".ValueInner").value;
      if (this.stringFieldTypes.includes(fieldType) && fieldType != 'BOOLEAN' && Value != "null" && Value != "NULL") {
        Value = "'" + Value + "'";
      }
      this.criteriaList[event.target.dataset.item].Value = Value;
      this.criteriaList[event.target.dataset.item].inEdit = false;
      this.buildQueryFilter();
      getCountOfTargetedRecords({ objName: this.object, filter: this.filter }).then(result => {
        this.massRecordsLength = result;
        this.editMode = false;
        this.loading = false;
      });
    }
  }

  //confirm the changes after edit of field update
  handleConfirmFieldUpdateEdit(event) {
    if (event.target.dataset.item != undefined) {
      this.fieldsToUpdate[event.target.dataset.item].fieldName = this.template.querySelector(".FieldNameUpdateInner").value != undefined ? this.template.querySelector(".FieldNameUpdateInner").value : "";
      this.fieldsToUpdate[event.target.dataset.item].displayedValue = this.template.querySelector(".ValueUpdateInner").value != undefined ? this.template.querySelector(".ValueUpdateInner").value : "";
      this.fieldsToUpdate[event.target.dataset.item].value = this.fieldsToUpdate[event.target.dataset.item].displayedValue
      this.fieldsToUpdate[event.target.dataset.item].inEdit = false;
      this.editMode = false;
    }
  }
  
  //Add a row of field update (field, new value)
  handleAddFieldUpdate() {
    this.loading = true;
    let displayedValue = this.template.querySelector(".UpdatedValue").value != undefined ? this.template.querySelector(".UpdatedValue").value : "";
    let value = String(displayedValue);
    if (this.fieldToUpdateSelected) {
      if (!this.fieldsToUpdate.some(obj => obj.fieldName == this.fieldToUpdateSelected.value)) {
        if (displayedValue == "") {
          displayedValue = this.fieldToUpdateSelected.type == "BOOLEAN" ? 'false' : 'null';
        }
        this.fieldsToUpdate.push({ fieldName: this.fieldToUpdateSelected.value, fieldType: this.fieldToUpdateSelected.type, displayedValue: displayedValue, value: value, inEdit: false });
        this.template.querySelector(".UpdatedFieldName").value = "";
      } else {
        this.handleToastMessage("Error", "Already added field " + this.fieldToUpdateSelected.value, "error");
      }
      this.template.querySelector(".UpdatedFieldName").value = "";
      this.fieldToUpdateSelected = null;
      //fieldType = this.stringFieldTypes.includes(this.fieldsList.filter((obj) => obj.label == this.fieldToUpdateSelected.value)[0].type) ? "String" : "Number";
      this.template.querySelector(".UpdatedValue").value = undefined;
    } else {
      this.handleToastMessage("Error", "Empty values!!", "error");
    }
    this.loading = false;
  }

  //Delete a row of field update rows
  handleDeleteFieldUpdate(event) {
    if (event.target.dataset.item != undefined) {
      this.fieldsToUpdate.splice(parseInt(event.target.dataset.item), 1);
    }
  }

  //Edit a row from the table of field update 
  handleEditFieldUpdateClick(event) {
    if (event.target.dataset.item != undefined) {
      let toEditFieldUpdate = parseInt(event.target.dataset.item);
      this.editMode = true;
      this.fieldsToUpdate[toEditFieldUpdate].inEdit = true;
    }
  }

  //Function to alert of filter before the execution
  handleExecuteUpdateDelete() {
    if (this.object != "" && this.object != undefined) {
      if (this.criteriaList.length == 0) this.noFilterWarning = true;
      else {
        this.buildQueryFilter();
        getCountOfTargetedRecords({ objName: this.object, filter: this.filter }).then(result => {
          this.massRecordsLength = result;
          this.confirmationRequest = true;
        });
        //this.executeUpdateDeleteRecords();
      }
    } else {
      this.handleToastMessage("Error", "No Object is selected", "error");
      this.loading = false;
    }
  }

  //Function of confirm execution on all records (without any filter)
  handleExecuteWithoutFilter() {
    this.noFilterWarning = false;
    this.executeUpdateDeleteRecords();
  }
  //function to close the filter warning without execution 
  handleClosePopup() {
    this.noFilterWarning = false;
    this.openSettings = false;
    this.confirmationRequest = false;
  }

  //execute the mass delete or mass update
  executeUpdateDeleteRecords() {
    this.apexJobId = "";
    this.handleClosePopup();
    //this.loading = true;
    let fieldUpdate = [];
    if (this.massOption == 'Update' && this.fieldsToUpdate.length == 0) {
      this.handleToastMessage("Warning", "Add your updated values!", "warning");
    } else {
      if (this.massOption == 'Update') {
        for (var update of this.fieldsToUpdate) {
          fieldUpdate.push({ fieldName: update.fieldName, fieldType: update.fieldType, value: update.value });
        }
      }
      executeUpdateDelete({ objName: this.object, filter: this.filter, mode: this.massOption, fieldUpdate: JSON.stringify(fieldUpdate), batchSize: this.batchSize }).then(result => {
        this.apexJobId = result;
        let self = this;
        if (this.apexJobId != undefined && this.apexJobId != null && this.apexJobId != "") {
          this.runningBatches.unshift({ batchId: this.apexJobId, TotalJobItems: null, JobItemsProcessed: null, status: "New", operation: this.massOption, progress: 0, log: null, operationIcon: this.massOption == "Delete" ? 'action:delete' : 'action:edit' });
          this.setTableDimension();
          //if (this.runningBatches.length > 5) this.runningBatches.pop();
          var interval = setInterval(() => {
            getStatusOfBatch({ apexJobId: this.apexJobId }).then(result => {
              //if (result == 'Completed' || result == 'Failed' || result == 'Aborted') {
                self.criteriaList = [];
                self.fieldsToUpdate = [];
                this.runningBatches.filter(obj => obj.batchId == this.apexJobId)[0].status = result;
                this.monitorBatches();
                this.loading = false;
                this.massRecordsLength = 0;
                clearInterval(interval);
              //}
            });
          }, 5000);
        } else {
          this.loading = false;
        }
      }).catch((error) => {
        this.handleToastMessage("Error", error.body.message, "error");
        this.loading = false;
      });
    }
  }

  //cancel mass update delete (exit the page)
  handleCancelPage() {
    if (window.history != null && window.history.length > 1) {
      window.history.back();
    }
    else {
      // Navigate to home page.
      var url = location.href;  // entire url including querystring - also: window.location.href;
      var baseURL = url.substring(0, url.indexOf('/', 14));
      this[NavigationMixin.Navigate]({
        type: 'standard__webPage',
        attributes: {
          url: baseURL + '/lightning/page/home'
        }
      },
        true // Replaces the current page in your browser history with the URL
      );
    }
  }
  handleOpenSettings() {
    this.openSettings = true;
  }
  handleChangeBatchSize() {
    this.batchSize = this.template.querySelector(".batchSize").value;
    console.log(this.batchSize);
    this.handleClosePopup();
  }
}